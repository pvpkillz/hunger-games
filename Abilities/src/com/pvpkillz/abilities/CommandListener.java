package com.pvpkillz.abilities;

import java.util.Arrays;

import main.BGMain;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import bukkitgames.API;

import utilities.BGChat;
import utilities.BGFeast;
import utilities.BGReward;
import utilities.enums.GameState;

public class CommandListener implements CommandExecutor {
	 
	private Main plugin; // pointer to your main class, unrequired if you don't need methods from the main class
 
	public CommandListener(Main plugin) {
		this.plugin = plugin;
	}
	
	public void PurchaseItem(Player p, int Price, Material item, int amt){
		int Coins = BGMain.getCoins(BGMain.getPlayerID(p.getName()));
		if (Coins > Price){//They have enough money
			send(p, "You have successfully purchased this item!");
			if (item == Material.LEATHER){
			p.getInventory().addItem(new ItemStack(Material.LEATHER_HELMET, amt));
			p.getInventory().addItem(new ItemStack(Material.LEATHER_CHESTPLATE, amt));
			p.getInventory().addItem(new ItemStack(Material.LEATHER_LEGGINGS, amt));
			p.getInventory().addItem(new ItemStack(Material.LEATHER_BOOTS, amt));
			}
			else if (item == Material.STONE){
				p.getInventory().addItem(new ItemStack(Material.CHAINMAIL_HELMET, amt));
				p.getInventory().addItem(new ItemStack(Material.CHAINMAIL_CHESTPLATE, amt));
				p.getInventory().addItem(new ItemStack(Material.CHAINMAIL_LEGGINGS, amt));
				p.getInventory().addItem(new ItemStack(Material.CHAINMAIL_BOOTS, amt));
			}
			else if(item == Material.IRON_BLOCK){
				p.getInventory().addItem(new ItemStack(Material.IRON_HELMET, amt));
				p.getInventory().addItem(new ItemStack(Material.IRON_CHESTPLATE, amt));
				p.getInventory().addItem(new ItemStack(Material.IRON_LEGGINGS, amt));
				p.getInventory().addItem(new ItemStack(Material.IRON_BOOTS, amt));
			}
			else if(item == Material.GOLD_BLOCK){
				p.getInventory().addItem(new ItemStack(Material.GOLD_HELMET, amt));
				p.getInventory().addItem(new ItemStack(Material.GOLD_CHESTPLATE, amt));
				p.getInventory().addItem(new ItemStack(Material.GOLD_LEGGINGS, amt));
				p.getInventory().addItem(new ItemStack(Material.GOLD_BOOTS, amt));
			}
			else if(item == Material.DIAMOND_BLOCK){
				p.getInventory().addItem(new ItemStack(Material.DIAMOND_HELMET, amt));
				p.getInventory().addItem(new ItemStack(Material.DIAMOND_CHESTPLATE, amt));
				p.getInventory().addItem(new ItemStack(Material.DIAMOND_LEGGINGS, amt));
				p.getInventory().addItem(new ItemStack(Material.DIAMOND_BOOTS, amt));
			}
			else{
			p.getInventory().addItem(new ItemStack(item, amt));
			}
			BGReward.takeCoins(p.getName(), Price);
		}
		else if (Coins < Price){
		send(p, "You need " + Price + " coins to purchase this item");
		}
		
	}
	public static void send(Player p, String string) {
		p.sendMessage(ChatColor.RED + "[PK] " + ChatColor.AQUA + string);
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
			Player player = (Player) sender;
		if (cmd.getName().equalsIgnoreCase("coins"))
		{
		int Coins = BGMain.getCoins(BGMain.getPlayerID(player.getName()));
		player.sendMessage(ChatColor.GREEN + "You currently have: " + ChatColor.GOLD + Coins + " coins.");
		return true;
		}
		if (cmd.getName().equalsIgnoreCase("shop"))
		{
		int second = 0;
		if (args.length == 2)
		second = Integer.parseInt(args[1]);
			
		
		if (Arrays.asList(API.getOnlineGamers()).contains(player)){
		if (API.getGameState() == GameState.GAME){
		if (args.length == 0){
		send(player,"        = = Shop = =        ");
		send(player,"/shop food - For food shop");
		send(player,"/shop weapon - For weapon shop");
		send(player,"/shop armor - For armor shop");
		send(player,"/shop misc - For miscellaneous shop");
		}
		else if (args[0].equalsIgnoreCase("food")){
		send(player, "To purchase type /shop food <ID>");
		send(player, "1 - Golden Apple | 20 Coins");
		send(player, "2 - Steak | 15 Coins");
		send(player, "3 - Cooked Chicken | 10 Coins");
		send(player, "4 - Cooked Porkchop | 10 Coins");
		send(player, "5 - Cooked Fish | 10 Coins");
		
		if (second == 1)
		PurchaseItem(player, 20, Material.GOLDEN_APPLE, 1);
		else if (second == 2)
		PurchaseItem(player, 15, Material.COOKED_BEEF, 1);
		else if (second == 3)
		PurchaseItem(player, 10, Material.COOKED_CHICKEN, 1);
		else if (second == 4)
		PurchaseItem(player, 10, Material.GRILLED_PORK, 1);
		else if (second == 5)
		PurchaseItem(player, 10, Material.COOKED_FISH, 1);
		else if (second == 0){}
		else
		send(player, "Sorry, invalid ID | It appears like this: # - Item Name");
		
		}
		else if (args[0].equalsIgnoreCase("weapon")){
			send(player, "To purchase type /shop weapon <ID>");
			send(player, "1 - Wood Sword | 10 Coins");
			send(player, "2 - Stone Sword | 25 Coins");
			send(player, "3 - Iron Sword | 40 Coins");
			send(player, "4 - Diamond Sword | 90 Coins");
			send(player, "5 - Bow | 30 Coins");
			if (second == 1)
			PurchaseItem(player, 10, Material.WOOD_SWORD, 1);
			else if (second == 2)
			PurchaseItem(player, 25, Material.STONE_SWORD, 1);
			else if (second == 3)
			PurchaseItem(player, 40, Material.IRON_SWORD, 1);
			else if (second == 4)
			PurchaseItem(player, 90, Material.DIAMOND_SWORD, 1);
			else if (second == 5)
			PurchaseItem(player, 30, Material.BOW, 1);
			else
			send(player, "Sorry, invalid ID | It appears like this: # - Item Name");
		}
		else if (args[0].equalsIgnoreCase("armor")){
			send(player, "To purchase type /shop weapon <ID>");
			send(player, "1 - Leather set | 30 Coins");
			send(player, "2 - Chain set | 50 Coins");
			send(player, "3 - Iron set | 80 Coins");
			send(player, "4 - Gold set | 40 Coins");
			send(player, "5 - Diamond set | 200 Coins");
			
			if (second == 1)
			PurchaseItem(player, 30, Material.LEATHER, 1);
			else if (second == 2)
			PurchaseItem(player, 50, Material.STONE, 1);
			else if (second == 3)
			PurchaseItem(player, 80, Material.IRON_BLOCK, 1);
			else if (second == 4)
			PurchaseItem(player, 30, Material.GOLD_BLOCK, 1);
			else if (second == 5)
			PurchaseItem(player, 200, Material.DIAMOND_BLOCK, 1);
			else
			send(player, "Sorry, invalid ID | It appears like this: # - Item Name");
		}
		else if (args[0].equalsIgnoreCase("misc")){
			send(player, "To purchase type /shop misc <ID>");
			send(player, "1 - Ender pearl | 20 Coins");
			send(player, "2 - Flint & Steel | 20 Coins");
			send(player, "3 - Arrows (16x) | 25 Coins");
			send(player, "4 - Enchantment Table | 15 Coins");
			send(player, "5 - Milk | 10 Coins");

			if (second == 1)
			PurchaseItem(player, 20, Material.ENDER_PEARL, 1);
			else if (second == 2)
			PurchaseItem(player, 20, Material.FLINT_AND_STEEL, 1);
			else if (second == 3)
			PurchaseItem(player, 25, Material.ARROW, 16);
			else if (second == 4)
			PurchaseItem(player, 15, Material.ENCHANTMENT_TABLE, 1);
			else if (second == 5)
			PurchaseItem(player, 10, Material.MILK_BUCKET, 1);
			else
			send(player, "Could not parse ID");
		}
		else{
		send(player,"Invalid format /shop <Catagory> <ID>");
		}
		}
		else {
		send(player, "You can only purchase during a game.");
		}
		}
		else{
		send(player, "Only gamers can purchase from shop.");
		}
		return true;
		}
		if ((cmd.getName().equalsIgnoreCase("feastspawn")) &&(player.hasPermission("bg.spawnfeast")))
		{
        plugin.getServer().broadcastMessage(ChatColor.RED + "Gamemaker: " + ChatColor.GOLD + player.getName() + " has just spawned the feast.");
		plugin.getServer().broadcastMessage(" ");
		plugin.getServer().broadcastMessage(" ");
        BGFeast.spawnFeast();
		return true;
		}
		Player p = player;
		if (cmd.getName().equalsIgnoreCase("chest")) {
			if (p.hasPermission("bg.admin.chest")
			|| p.hasPermission("bg.admin.*")) {
			plugin.chest.spawnChest(p.getLocation());
			return true;
			}
			notallowed(p);
			return false;
			}

			if (cmd.getName().equalsIgnoreCase("rchest")) {
			if (p.hasPermission("bg.admin.rchest")
			|| p.hasPermission("bg.admin.*")) {
			plugin.chest.spawnChest();
			return true;
			}
			notallowed(p);
			return false;
			}

			if (cmd.getName().equalsIgnoreCase("table")) {
			if (p.hasPermission("bg.admin.table")
			|| p.hasPermission("bg.admin.*")) {
			plugin.chest.spawnTable(p.getLocation());
			return true;
			}
			notallowed(p);
			return false;
			}

			if (cmd.getName().equalsIgnoreCase("rtable")) {
			if (p.hasPermission("bg.admin.rtable")
			|| p.hasPermission("bg.admin.*")) {
			plugin.chest.spawnTable();
			return true;
			}
			notallowed(p);
			return false;
			}
			  if ((cmd.getName().equalsIgnoreCase("alltolobby")) || (cmd.getName().equalsIgnoreCase("lobbyall")) || ((cmd.getName().equalsIgnoreCase("allto")) && (sender.hasPermission("hubkick.kickall")))) {
				    ABListener.kickall();
				  }
				  else if ((cmd.getName().equalsIgnoreCase("shutdown")) || ((cmd.getName().equalsIgnoreCase("stop")) && (sender.hasPermission("hubkick.shutdown")))) {
				    ABListener.kickshutdown();
				  }
		return false;
	}


	public void notallowed(Player p) {
		BGChat.printPlayerChat(p, "You are not allowed to do this.");
	}
}