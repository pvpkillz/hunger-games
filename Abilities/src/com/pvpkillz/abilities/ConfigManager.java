package com.pvpkillz.abilities;

import java.io.File;
import java.util.LinkedHashMap;
import java.util.Map.Entry;


import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

public class ConfigManager extends JavaPlugin {

	private Main plugin;

private File configFile;
private FileConfiguration config;

	public ConfigManager(Main plugin) {
		
		this.plugin = plugin;
		manageConfig();
	}
	
	private void manageConfig() {
		
		configFile = new File(plugin.getDataFolder() + "/config.yml");
		config = YamlConfiguration.loadConfiguration(configFile);
		LinkedHashMap<String , String> configMap = new LinkedHashMap<String, String>();
		config.options().header("Config/Description file, for the Abilities plugin by Undeadkillz\nThis copy is strictly made for \nPvPKillz.com\nAnd\nInsaneHypo.com");
		
		configMap.put("Abilities.41.Desc", "When you drink Milk, you receive; Regen, Fire Resistance, and Speed for 10 seconds.");
		configMap.put("Abilities.41.Cooldown", "10");
		
		configMap.put("Abilities.42.Desc", "1 Hit Player with a great deal of damage. (Out of 20)");
		configMap.put("Abilities.42.LOST", "4");
		configMap.put("Abilities.42.Cooldown", "60");
		
		configMap.put("Abilities.43.Desc", "Hunt all hostile mobs. Throw an egg, and capture them. POKEMON!");

		configMap.put("Abilities.44.Desc", "Chance of Poisening who ever attacks you for 5 seconds.");
		configMap.put("Abilities.44.Chance", "35");
		
		configMap.put("Abilities.45.Desc", "Snowball causes damage.");
		configMap.put("Abilities.45.Damage", "2");
		
		configMap.put("Abilities.46.Desc", "Place a beacon, and anyone around it receives Strength! Time depends on how close the players are.");
		
		configMap.put("Abilities.47.Desc", "Player receives a Strength boost, when he attacks a player. But gets confusion.(10 Seconds)");
		
		configMap.put("Abilities.48.Desc", "Player receives less damage, when crouched. (But can't attack)");
		configMap.put("Abilities.48.DamageBlocked", "1");

		configMap.put("Abilities.49.Desc", "Gain strength and speed at night, but weakness in the day.");

		configMap.put("Abilities.50.Desc", "Player can stay underwater for 14 minutes, and receives strength when underwater.");

		configMap.put("Abilities.51.Desc", "Player can stay in Lava for 14 minutes, and receives strength and fire-resistance.");

		//configMap.put("Abilities.52.Desc", "Player places flowers, wherever he/she walks. (GRASS ONLY) Player has Smoke coming out of the character.");

		configMap.put("Abilities.53.Desc", "Player can smelt ores, by just right-clicking while holding them");

		configMap.put("Abilities.54.Desc", "Player can super jump, with the use of paper.");
		configMap.put("Abilities.54.Cooldown", "10");


		configMap.put("Abilities.55.Desc", "Turn Invisible, when crouched, for 1 minute. But smoke will come out.");
		
		configMap.put("Abilities.56.Desc", "Gain XP Levels for each kill, the more players killed, the more XP rewarded.");

		configMap.put("Abilities.57.Desc", "Player has the chance to drop X of iron, when it's mined.");
		configMap.put("Abilities.57.Chance", "35");
		configMap.put("Abilities.57.Amount", "2");
		
		configMap.put("Abilities.58.Desc", "Player can Climb walls holding right-click.");
		configMap.put("Abilities.61.Desc", "Player spews a fire ring, around him. (Beta)");
		configMap.put("Abilities.65.Desc", "When player places TNT, it is automatically activated.");
		configMap.put("Abilities.66.Desc", "Player has a chance of poisoning and confusing victim.");
		configMap.put("Abilities.66.Chance", "33");
		configMap.put("Abilities.67.Desc", "Throwing a snowball, creates a small platform of snow, and ice.");
		configMap.put("Abilities.68.Desc", "A Feather, allows you to fly.");
		configMap.put("Abilities.68.Cooldown", "60");
		configMap.put("Abilities.68.Length", "15");
		configMap.put("Abilities.69.Desc", "Player is capable of using Landmines");
		configMap.put("Abilities.75.Desc", "Player is able to teleport with the use of a wand (Blaze Rod)");
		configMap.put("Abilities.76.Desc", "Can backstab players, for insta-kills");


		for (Entry<String, String> entry : configMap.entrySet()) {
			String path = entry.getKey();
			Object value = entry.getValue();
			
			if (!config.contains(path)) {
				config.set(path, value);
			}
		}
		
		try{
			config.save(configFile);
		}catch (Exception e) {
			e.printStackTrace();
			plugin.log.info("[Abilities] Unable to save config.yml!");
		}
	}
	
	public String readString(String path) {
		
		return config.getString(path);
	}
	
	public int readInt(String path) {
		
		String string = config.getString(path);
		int zahl;
		try{
			zahl = Integer.parseInt(string);
			return zahl;
		}catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}
}
