package com.pvpkillz.abilities;




import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Logger;

import main.BGMain;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Snowball;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityRegainHealthEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerToggleSneakEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.ScoreboardManager;
import org.bukkit.util.Vector;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;


import utilities.BGCornucopia;
import utilities.BGKit;
import utilities.enums.GameState;
import bukkitgames.API;
import fr.neatmonster.nocheatplus.checks.CheckType;
import fr.neatmonster.nocheatplus.hooks.NCPExemptionManager;

public class ABListener extends JavaPlugin implements Listener{

Logger log = Logger.getLogger("Minecraft");

public static Main plugin;
public Cooldown cool;
public ArrayList<Player> MilkManList = new ArrayList<Player>();
public ArrayList<Player> JumperList = new ArrayList<Player>();
public ArrayList<Player> FlyingAway = new ArrayList<Player>();
public ArrayList<Player> TeleportList = new ArrayList<Player>();
public ArrayList<Player> Gamers = new ArrayList<Player>();
HashMap<Location, Player> Claymore = new HashMap<Location, Player>();
static HashMap<String, Integer> Kills = new HashMap<String, Integer>();
HashMap<String, Integer> ClaymoresPlaced = new HashMap<String, Integer>();
public static HashMap<String, Objective> Objectives = new HashMap<String, Objective>();
public static HashMap<String, org.bukkit.scoreboard.Scoreboard> Boards = new HashMap<String, org.bukkit.scoreboard.Scoreboard>();

public ABListener(Main mainclass) {
	plugin = mainclass;
	mainclass.getServer().getPluginManager().registerEvents(this, mainclass);
	}


public ArrayList<String> newplayer;
boolean Taken = false;
HashMap<String, Integer> MilkCount = new HashMap<String, Integer>();
static org.bukkit.scoreboard.Scoreboard board;
static ScoreboardManager manager;
public static Objective objective;

@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = false)
public void onPlayerJoinEvent(final PlayerJoinEvent event) {
final Player p = event.getPlayer();
startInfections(p);
Gamers.add(p);
if (!Kills.containsKey(p.getName())){
    Kills.put(p.getName(), 0);
}
manager = Bukkit.getScoreboardManager();
board = manager.getNewScoreboard();
objective = board.registerNewObjective("test", "dummy");
objective.setDisplaySlot(DisplaySlot.SIDEBAR);

if (!Objectives.containsKey(p.getName()))
Objectives.put(p.getName(), objective);

if (!Boards.containsKey(p.getName()))
Boards.put(p.getName(), board);
/*
for (Player pl : plugin.getServer().getOnlinePlayers()){
	Thread thread = new Thread(new RunnableThread(pl), "thread");
	thread.start();
}
*/

}
public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
  if ((commandLabel.equalsIgnoreCase("alltolobby")) || (commandLabel.equalsIgnoreCase("lobbyall")) || ((commandLabel.equalsIgnoreCase("allto")) && (sender.hasPermission("hubkick.kickall")))) {
    kickall();
  }
  else if ((commandLabel.equalsIgnoreCase("shutdown")) || ((commandLabel.equalsIgnoreCase("stop")) && (sender.hasPermission("hubkick.shutdown")))) {
    kickshutdown();
  }
  return false;
}

public void send(Player p) {
    p.sendMessage(ChatColor.RED + "Round has ended.");
    ByteArrayDataOutput out = ByteStreams.newDataOutput();
    out.writeUTF("Connect");
    out.writeUTF("hub");
    p.sendPluginMessage(plugin, "BungeeCord", out.toByteArray());
}

public static void kickall() {
  for (Player p : Bukkit.getServer().getOnlinePlayers()) {
    p.sendMessage(ChatColor.RED + "Round has ended.");
    ByteArrayDataOutput out = ByteStreams.newDataOutput();
    out.writeUTF("Connect");
    out.writeUTF("hub");
    p.sendPluginMessage(plugin, "BungeeCord", out.toByteArray());
  }
}

public static void kickshutdown()
{
  kickall();
  new BukkitRunnable() {
    public void run() {
      Bukkit.shutdown();
    }
  }
  .runTaskLater(plugin, 40L);
}



@EventHandler
public void OnKills(PlayerDeathEvent e){
	for (ItemStack item : e.getDrops()){
		if ((item.getTypeId() >= 298) && (item.getTypeId() <= 317)){
			double ran = Math.random();
			if (ran <= 0.7){
			item.setTypeId(0);
			}
		}
	}
	if (e.getEntity() instanceof Player){
	Player player = e.getEntity();
	if (player.getKiller() instanceof Player)
	Kills.put(player.getKiller().getName(), Kills.get(player.getKiller().getName()) + 1);
	}
}
static public void ScoreBoard(Player player){
	objective = Objectives.get(player.getName());
	board = Boards.get(player.getName());
	String Status = "None";
	if (API.getGameState() == GameState.PREGAME)
	Status = "Pre-Game";
	else if (API.getGameState() == GameState.INVINCIBILITY)
	Status = "Invincibility";
	else if (API.getGameState() == GameState.GAME)
	Status = "In-Progress";
	objective.setDisplayName(ChatColor.RED + "Status" + ChatColor.YELLOW +  ":  " + ChatColor.GOLD + Status);
	GiveStat(player, objective, API.getOnlineGamers().length, ChatColor.GREEN + "Players:", ChatColor.GREEN);
    GiveStat(player, objective, ABListener.Kills.get(player.getName()), "Kills:", ChatColor.RED);
    int Num = 0;
    String Type = "Time left:";
    if (API.getGameState() == GameState.PREGAME){
    Num = BGMain.COUNTDOWN;
    }
    else if (API.getGameState() == GameState.GAME){
    Num = BGMain.GAME_ENDING_TIME - BGMain.GAME_RUNNING_TIME - 30;
    }
    else if (API.getGameState() == GameState.PREGAME){
    Num = BGMain.GAME_ENDING_TIME;
    }
    GiveStat(player, objective, Num, ChatColor.YELLOW + Type, ChatColor.YELLOW);
    player.setScoreboard(board);
}
public static void GiveStat(Player player, Objective objective, Integer integer, String string, ChatColor COLOR) {
	  Score score = objective.getScore(Bukkit.getOfflinePlayer(COLOR + string)); //Get a fake offline player
	  score.setScore(integer); //Integer only!
}
	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent event) {
		final Player player = event.getPlayer();
		
		
		
	     if ((event.getAction().equals(Action.PHYSICAL)) && (event.getClickedBlock().getTypeId() == 70)) 
	     {
	       Location blockloc = event.getClickedBlock().getLocation();
			if (Claymore.get(blockloc) != null)// If the check succeeds then, create and explosion at location and remove the Claymore.
			{
				Bukkit.getWorld(player.getWorld().getName()).createExplosion(blockloc.getX(), blockloc.getY(), blockloc.getZ(), 3.0F, false, false);
				blockloc.getBlock().setTypeId(0);
				Location location = new Location(blockloc.getWorld(), blockloc.getBlockX(),blockloc.getBlockY(),blockloc.getBlockZ());
				Player placer = Claymore.get(location);
				CommandListener.send(placer, "Your landmine has exploded, and killed " + player.getName());
				Claymore.remove(location);
			}
	     }
		
		
		
		
		
		
		if (((event.getAction() == Action.RIGHT_CLICK_AIR) || (event.getAction() == Action.RIGHT_CLICK_BLOCK)) && (player.getItemInHand().getType() == Material.BOW)){
			plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
			    @Override 
			    public void run() {
	      			  Material hand = player.getItemInHand().getType();
	      		      if (((hand == Material.BOW) || (hand == Material.APPLE) || (hand == Material.BREAD) || (hand == Material.COOKED_FISH)) && (player.isSprinting()) && (player.getInventory().contains(new ItemStack(Material.ARROW)))){
	      		    	  player.kickPlayer(ChatColor.RED + "Hacking Detected | No Slow Down");
			    }
			   }
			}, 10L);  
			
			
		}
		
		if ((BGKit.hasAbility(player, 75)) && (event.getAction() == Action.LEFT_CLICK_AIR)){
			
			if ((player.getItemInHand().getType() == Material.BLAZE_ROD) && (!TeleportList.contains(player))){
				
				Block b = player.getTargetBlock(null, 50);
				Location bl = b.getLocation();
				if (player.getWorld().getBlockAt(bl).getType() == Material.AIR)
				CommandListener.send(player, "The wand refuses to travel that far.");
				else{
				Location teleport = new Location(player.getWorld(),bl.getBlockX(), bl.getBlockY() + 2, bl.getBlockZ());
				player.teleport(teleport);
				}
				Inform(player, ChatColor.AQUA + "You have been teleported with the power of your wand!");
				TeleportList.add(player);
				plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
				    @Override 
				    public void run() {
				    	Inform(player, "Your wand has cooled down.");
				    	TeleportList.remove(player);
				    }
				}, 2 * 60 * 20L);
			}
			else if ((TeleportList.contains(player)) && (player.getItemInHand().getType() == Material.BLAZE_ROD)){
				Inform(player, "The wand is currently too hot to be used again");
			}
		}
		if (BGKit.hasAbility(player, 68))
		{
			if ((player.getItemInHand().getType() == Material.FEATHER) && (!FlyingAway.contains(player)) && ((event.getAction().equals(Action.RIGHT_CLICK_BLOCK) || event.getAction().equals(Action.RIGHT_CLICK_AIR))))
		    NCPExemptionManager.isExempted(player.getEntityId(), CheckType.MOVING_SURVIVALFLY);
			player.setFlying(true);
			player.setVelocity(new Vector(0,2,0));
			NCPExemptionManager.unexempt(player.getEntityId(), CheckType.MOVING_SURVIVALFLY);
	        player.getInventory().removeItem(new ItemStack (Material.FEATHER, 1));
			FlyingAway.add(player);
			player.sendMessage(ChatColor.YELLOW + "And you begin flying...");
				TimerTask action = new TimerTask() {
					
					public void run(){
				    player.setAllowFlight(false);
					player.setFlying(false);
					}
				};
				Timer timer = new Timer();
				timer.schedule(action, plugin.config.readInt("Abilities.68.Length") * 1000);
		}
		//Ability MilkMan
		if (BGKit.hasAbility(player, 41))
			{
                ItemStack bucket = new ItemStack(Material.MILK_BUCKET);
                //ItemStack cookedChicken = new ItemStack(Material.COOKED_CHICKEN);
                ItemStack hand = player.getItemInHand();
                //ItemStack amount = new ItemStack(player.getItemInHand().getAmount());
                if (!MilkCount.containsKey(player)){
                    MilkCount.put(player.getName(), 3);
                }
                
                
                if ((!MilkManList.contains(player)) && (hand.equals(bucket)) && (MilkCount.get(player.getName()) <= 4) && (Taken == false)){
					MilkManList.add(player);
					cool.MilkCooldown(player);
    				player.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, 200, 0));
    				player.addPotionEffect(new PotionEffect(PotionEffectType.FIRE_RESISTANCE, 200, 0));
    				player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 200, 0));
    				int Left = MilkCount.get(player.getName()) ; 
    				if (Left > 1){
    				player.sendMessage(ChatColor.BLUE+"Uses left: "+Left);
					MilkCount.put(player.getName(), MilkCount.get(player) - 1);
    				}
    				else if (Left == 1){
        				player.sendMessage(ChatColor.RED+"Uses left: None");
    					MilkCount.put(player.getName(), MilkCount.get(player) - 1);
    				}
    				else if(Left == 0){
    					player.sendMessage(ChatColor.RED + "You have used up all your special milk.");
    				}
			}
					else if (MilkManList.contains(player)){
						player.sendMessage(ChatColor.RED+"You can't use Milk ability yet.");
					}
		}
		
		if (BGKit.hasAbility(player, 53))
		{
            ItemStack ore = new ItemStack(Material.IRON_ORE, 1);
            //ItemStack cookedChicken = new ItemStack(Material.COOKED_CHICKEN);
            ItemStack hand = player.getItemInHand();
		  if((event.getAction() == Action.RIGHT_CLICK_AIR) && (hand.equals(ore))) {
		        player.getInventory().removeItem(new ItemStack (Material.IRON_ORE, 1));
		        player.getInventory().addItem(new ItemStack(Material.IRON_INGOT, 1));
		        player.sendMessage(ChatColor.RED+"You have just smelted your ore, into an ingot.");
		  	}
		}
		if (BGKit.hasAbility(player, 54))
		{
            ItemStack paper = new ItemStack(Material.PAPER, 1);
            //ItemStack cookedChicken = new ItemStack(Material.COOKED_CHICKEN);
            ItemStack hand = player.getItemInHand();
		  if(((event.getAction() == Action.RIGHT_CLICK_AIR) || (event.getAction() == Action.RIGHT_CLICK_BLOCK)) && (hand.equals(paper) && (!JumperList.contains(player)))) {
			  player.sendMessage(ChatColor.YELLOW+"You have just released the strength in your feet.");
			  NCPExemptionManager.isExempted(player.getEntityId(), CheckType.MOVING_SURVIVALFLY);
			  player.setVelocity(new Vector(0,2,0));
			  NCPExemptionManager.unexempt(player.getEntityId(), CheckType.MOVING_SURVIVALFLY);
				JumperList.add(player);				
				  new BukkitRunnable() {
					    public void run() {
					        player.sendMessage(ChatColor.RED + "Your feet, has regained their power! " + ChatColor.GREEN + "You may now use your super-jump!");
					        JumperList.remove(player);
					    }
					  }
					  .runTaskLater(plugin, 40 * 20L);
		}
		  else if (JumperList.contains(player) && (hand.equals(paper))){
			  player.sendMessage(ChatColor.YELLOW+"Your feet are too tired for another jump.");
		  }
		}
		if (BGKit.hasAbility(player, 58))
		{
        if(event.getAction().equals(Action.RIGHT_CLICK_BLOCK))
        {
                player.setVelocity(player.getVelocity().add(new Vector(0.0D, 0.40D, 0.0D)));
		}
	}
	}
	private void Inform(Player player, String string) {
		
		player.sendMessage(ChatColor.RED + "[Pkz] " + ChatColor.YELLOW + string);
	}
	////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////
	public void startInfections(final Player p) {
		plugin.getServer().getScheduler().runTaskTimerAsynchronously(plugin, new Runnable() {
		    @SuppressWarnings("deprecation")
			@Override  
		    public void run() {
				double Ran = Math.random();
				double Randi = Math.random();
				if (Randi <= 0.1){
				if 		(Ran <= .10){
				SendM(p, "You have been unluckly diseased by a some infected grass..");
				p.addPotionEffect(new PotionEffect(PotionEffectType.CONFUSION, 20 * 50, 0));
				p.addPotionEffect(new PotionEffect(PotionEffectType.POISON, 20 * 5, 0));
				}
				else if (Ran <= .20){
				SendM(p, "You find an open potion lying around, and its effects seem mysterious.");
				p.addPotionEffect(new PotionEffect(PotionEffectType.WEAKNESS, 20 * 60 * 2, 0));
				
				// Add some Random Effects and messages Healing, Poison, Blindness.
				}
				else if (Ran <= .30){
				SendM(p, "Parasites had found their way to you.. D:");
				p.damage(5);
				}
				else if (Ran <= .40){
				SendM(p, "A potion of healing has been left around you.");
				double RandiPot = Math.random();
				if (RandiPot <= .50){
					p.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 20 * 10, 0));
				}
				else if (RandiPot <= 1.00){
					p.addPotionEffect(new PotionEffect(PotionEffectType.HEAL, 20 * 5, 0));
				}
				// Add some Randomness, make it positive and negative
				}
				else if (Ran <= 0.50){
				SendM(p, "Something must have burnt you.. It seems to be a long-lasting burn.");
				for (int Hurt = 0; Hurt <= 5; Hurt++){
				    Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable()
				    {
				        @Override
				        public void run()
				        {
				        p.damage(1);
				        }
				    }, 20 * Hurt);
				}
				
				
				}
				else if (Ran <= .60){
				SendM(p, "You passed by a dead body, and you retrieved some potions.");
				PlayerInventory inv = p.getInventory();
				double RandPot = Math.random();
				
				if (RandPot <= 0.25){
				ItemStack item = new ItemStack(373, 2, (short) 8194); // Speed 3:00
				inv.addItem(item);
				}
				else if (RandPot <= 0.50){
				ItemStack item = new ItemStack(373, 1, (short) 8201); // Strength 3:00
				inv.addItem(item);
				}
				else if (RandPot <= 0.75){
				ItemStack item = new ItemStack(373, 2, (short) 16396); // Dmg Potion
				inv.addItem(item);
				}
				else{
				ItemStack item = new ItemStack(373, 1, (short) 16452); // Poison 1:30
				inv.addItem(item);
				}
				p.updateInventory();
				}
				else if (Ran <= .70){
				SendM(p, "You have been just lucky... You almost attracted wolves.");
				}
				else if (Ran <= .90){
				SendM(p, "Your life will end soon... Enjoy your time being.");
				}
				else{
					
				}
				}
		    }
		}, 20 * 60 * 7, 20 * 60 * 2 );
		}


	private void SendM(Player p, String string) {
		p.sendMessage(ChatColor.YELLOW + string);
	}
	@EventHandler
    public void onBlockPlace(final BlockPlaceEvent event) {
            Material block = event.getBlock().getType();
            final Block blockl = event.getBlock();
			final Location loc = event. getBlock ( ) . getLocation ( ) ;
            final Player player = event.getPlayer();
            final World world = player.getWorld();
        	////////////////////////////////////////////////////////////////////////////////////////
        	////////////////////////////////////////////////////////////////////////////////////////
        	////////////////////////////////////////////////////////////////////////////////////////
            /*
			if (BGKit.hasAbility(player, 69))
			{
            if (!ClaymoresPlaced.containsKey(player.getName())){
          		ClaymoresPlaced.put(player.getName(), 0);
      		}
            if ((blockl.getType() == Material.STONE_PLATE) && (ClaymoresPlaced.get(player.getName()) <= 5)){
            Claymore.put(loc, player);
            ClaymoresPlaced.put(player.getName(), ClaymoresPlaced.get(player.getName()) + 1);
            } 
			}
            
            
            
            
            
            
            
			if (BGKit.hasAbility(player, 65))
			{
			if (block == Material.TNT){
				event.getBlock().setType(Material.AIR);
				event.getBlock().getWorld().spawn(loc.add(0.5, 0.5, 0.5), TNTPrimed.class);
			}
			}
            
            */
			if (BGKit.hasAbility(player, 59))
			{
				if (block.equals(Material.ENCHANTMENT_TABLE)){
	            plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable()
	            {
	                @Override
	                public void run()
	                {
			            int IVBlocks = 0;
						for(int x = loc.getBlockX() - 2; x <= loc.getBlockX() + 2; x++)
						{
						    for(int y = loc.getBlockY() - 4; y <= loc.getBlockY() + 0; y++)
						    {
						        for(int z = loc.getBlockZ() - 2; z <= loc.getBlockZ() + 2; z++)
						        {
						        	
						        	Block Check = loc.getWorld().getBlockAt(x, y, z);
						        	if (!BGCornucopia.isCornucopiaBlock(Check))
						        	{
						        		loc.getWorld().getBlockAt(x, y, z).setTypeId(0);
						        	}
						        	else {
						        	IVBlocks++;
						        	}
						    }
						}
	                    }
						int x1 = loc.getBlockX();
						int y1 = loc.getBlockY();
						int z1 = loc.getBlockZ();
						loc.getWorld().createExplosion(x1, y1, z1, 2.0F);
						if (IVBlocks != 0){
							player.sendMessage(ChatColor.RED + "Warning: " + IVBlocks + " were not removed due to protection.");
						}
						else {}
	                }
	            }, 35L);
				}
			}
			if ((BGKit.hasAbility(player, 46)) && (block == Material.BEACON)) {
				player.sendMessage(ChatColor.RED+"You have just released your anger!");
				int maxDist = 10;
                for(int q = 1;q <= 3; q++){
                	final Block newBlock = world.getBlockAt(blockl.getX(), blockl.getY()+q, blockl.getZ());
                    newBlock.setTypeId(85);
    	            Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable()
    	            {
    	                @Override
    	                public void run()
    	                {
    	                    newBlock. setTypeId ( 0 ) ;
    	                    
    	                    }
    	            }, 120L);
                }
            	final Block newBlock11 = world.getBlockAt(blockl.getX()+1, blockl.getY()+4, blockl.getZ());
                newBlock11.setTypeId(35);
            	final Block newBlock112 = world.getBlockAt(blockl.getX()-1, blockl.getY()+4, blockl.getZ());
                newBlock112.setTypeId(35);
            	final Block newBlock111 = world.getBlockAt(blockl.getX(), blockl.getY()+4, blockl.getZ()-1);
                newBlock111.setTypeId(35);
            	final Block newBlock1111 = world.getBlockAt(blockl.getX(), blockl.getY()+4, blockl.getZ()+1);
                newBlock1111.setTypeId(35);
                final Block newBlock1 = world.getBlockAt(blockl.getX(), blockl.getY()+4, blockl.getZ());
                newBlock1.setTypeId(35);
        		plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
	                @Override
	                public void run()
	                {
	                    newBlock1. setTypeId ( 0 ) ;
	                    newBlock112. setTypeId ( 0 ) ;
	                    newBlock11. setTypeId ( 0 ) ;
	                    newBlock111. setTypeId ( 0 ) ;
	                    newBlock1111. setTypeId ( 0 ) ;
	                    }
	            }, 120L);
        		plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
	                @Override
	                public void run()
	                {
	                    newBlock1. setTypeId ( 0 ) ;
	                    
	                    }
	            }, 120L);
				for (Player other : Bukkit.getOnlinePlayers()) {
				  if (other.getLocation().distance(player.getLocation()) <= maxDist) {
					  double length = event.getBlock().getLocation().distance(other.getLocation());
					  int time = 1200 - ((int)length * 120);
					  if (other != player){
							other.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, time, 0));
							other.sendMessage(ChatColor.YELLOW + "You were given a Strength Buff, by " + player.getPlayerListName() + " and it lasts " + time/20 + " seconds");
					  }
					  else if (other == player){
							player.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 1200, 1));
					  }
							other.sendMessage(ChatColor.RED+"You have 10 seconds, to run from the fire...");
							other.addPotionEffect(new PotionEffect(PotionEffectType.FIRE_RESISTANCE, 200, 1));
				            world.strikeLightning(event. getBlock ( ) . getLocation ( ) );
				            final Block bbbbbbvv = world. getBlockAt ( loc ) ;
				            plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable()
				            {
				                @Override
				                public void run()
				                {
				                    bbbbbbvv. setTypeId ( 0 ) ;
				                    
				                    }
				            }, 120L);
			        	}
				}
				
                }
			
			////////////////////////////////////////////////////////////////////////////////////////
			////////////////////////////////////////////////////////////////////////////////////////
			////////////////////////////////////////////////////////////////////////////////////////
            
	}
	  @EventHandler
	  public void onProjectileHit(ProjectileHitEvent event) {
      Entity damager = event.getEntity();
	  if (damager instanceof Snowball){
		  Player dam = (Player) event.getEntity().getShooter();
		  final Location loc = damager.getLocation();
	    	if (BGKit.hasAbility(dam, 67)) {
				for(int x = loc.getBlockX() - 3; x <= loc.getBlockX() + 3; x++)
				{
				        for(int z = loc.getBlockZ() - 3; z <= loc.getBlockZ() + 3; z++)
				        {
				        	
				        	Block Check = loc.getWorld().getBlockAt(x, loc.getBlockY(), z);
				        	Block Check1 = loc.getWorld().getBlockAt(x, loc.getBlockY() - 1, z);
				        	if ((!BGCornucopia.isCornucopiaBlock(Check1)) && (!BGCornucopia.isCornucopiaBlock(Check)) && (Check1.getType() != Material.AIR))
				        	{
				        		loc.getWorld().getBlockAt(x, loc.getBlockY(), z).setType(Material.ICE);
				    		    plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable()
				    		    {
				    		        @Override
				    		        public void run()
				    		        {
				    					for(int x = loc.getBlockX() - 3; x <= loc.getBlockX() + 3; x++)
				    					{
				    					        for(int z = loc.getBlockZ() - 3; z <= loc.getBlockZ() + 3; z++)
				    					        {
						        		loc.getWorld().getBlockAt(x, loc.getBlockY() + 1, z).setType(Material.SNOW);					        		
				    			                }
				    			    	}
				    		        }
				    		    }, 10L);
				        	}
				        	else {
				        	}
				    }
                }
	    	}
	  }
	  }
	
	
    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
	public void onBlockBreak(BlockBreakEvent event){
	    Player breaker = event.getPlayer();
	    Block broken = event.getBlock();
		////////////////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////////////////
    	if (BGKit.hasAbility(breaker, 57)) {
    		Block block = event.getBlock();
    		Location centerOfBlock = block.getLocation();
    		Random r = new Random();
    		int Chance = plugin.config.readInt("Abilities.57.Chance");
    		int Amount = plugin.config.readInt("Abilities.57.Amount");
    		double randomValue = 100 * r.nextDouble();
    		if ((Chance >= randomValue) && (broken.getType() == Material.IRON_ORE)){
    		block.getWorld().dropItemNaturally(centerOfBlock, new ItemStack(Material.IRON_INGOT, Amount));
            centerOfBlock.getBlock().setType(Material.AIR);
    		}
    		else if ((Chance <= randomValue) && (broken.getType() == Material.IRON_ORE)) {
    			breaker.sendMessage(ChatColor.YELLOW+"Better luck next time :P");
        		block.getWorld().dropItemNaturally(centerOfBlock, new ItemStack(Material.IRON_INGOT, 1));
                centerOfBlock.getBlock().setType(Material.AIR);
    		}
    		
        }
    	
    	////////////////////////////////////////////////////////////////////////////////////////
    	////////////////////////////////////////////////////////////////////////////////////////
    	////////////////////////////////////////////////////////////////////////////////////////
        }
    
	@EventHandler
	public void onEntityDamage(EntityDamageEvent event) {
		Entity player = event.getEntity();
		if (player instanceof Player){
		Player play = (Player) player;
		if ((BGKit.hasAbility(play, 54)))
		{
    if (player instanceof Player && event.getCause() == EntityDamageEvent.DamageCause.FALL) {
        event.setCancelled(true);
    }
	}
		if ((BGKit.hasAbility(play, 58)))
		{
    if ((player instanceof Player && event.getCause() == EntityDamageEvent.DamageCause.FALL) && (JumperList.contains(player))) {
        event.setCancelled(true);
    }
	}
	}
	}
	
    private boolean byWall(Player player)
    {
        Block pBlock = player.getLocation().add(new Vector(0, 1, 0)).getBlock();
        if(!byBlock(pBlock, new Vector(0, 1, 0)).isEmpty())
            return true;
        if(!byBlock(pBlock, new Vector(1, 0, 0)).isEmpty())
            return true;
        if(!byBlock(pBlock, new Vector(-1, 0, 0)).isEmpty())
            return true;
        if(!byBlock(pBlock, new Vector(0, 0, 1)).isEmpty())
            return true;
        return !byBlock(pBlock, new Vector(0, 0, -1)).isEmpty();
    }

    private Block byBlock(Block block, Vector diff)
    {
        Location blockLoc = block.getLocation();
        return blockLoc.getWorld().getBlockAt(blockLoc.add(diff));
    }
    @EventHandler
    public void OnRegainHealth(EntityRegainHealthEvent e){
    	Entity user = e.getEntity();
    	if (user instanceof Player){
    		Player p = (Player)user;
    		if (BGKit.hasAbility(p, 76)){
    		if (p.getHealth() <= 5.00){
				p.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 100000, 1));
    		}
    		else{
				p.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 0, 1), true);
    		}
    		}
    	}
    }
	@EventHandler
	public void onPlayerMove(final PlayerMoveEvent event){
	      final Player player = event.getPlayer();
	      /*
	      final String playerN = event.getPlayer().getName();
	      if (API.getOnlineGamers().toString().contains(playerN) == true){
				for (Player other : Bukkit.getOnlinePlayers()) {
					String otherp = other.getPlayer().getName();
					  if ((other.getLocation().distance(player.getLocation()) <= 3) && (BGMain.getSpectators().toString().contains(otherp))) {  
			                other.setVelocity(other.getVelocity().add(new Vector(0.0D, 0.20D, 0.0D)));
			                other.sendMessage("You were too close to the Tribute.");
					  }
					  else {}
	      }
	      }
	      else {
	    	  
	      }
	      */
	        // Night-Monster
			if (BGKit.hasAbility(player, 49)) {
				Location loc = new Location(player.getWorld(), player.getLocation().getBlockX(), player.getLocation().getBlockY(), player.getLocation().getBlockZ());
				if (player.getWorld().getBlockAt(loc).getLightLevel() == 5){
				player.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 400, 0));
				player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 400, 1));
				}
				else {
					player.addPotionEffect(new PotionEffect(PotionEffectType.WEAKNESS, 400, 0));
				}
			}
			
			if ((BGKit.hasAbility(player, 45)) && (Arrays.asList(BGMain.getGamers()).contains(player))) {
				
				for (int x = -1; x <= 1; x++){
					for (int z = -1; z <= 1; z++){
				Location loc = new Location(player.getWorld(), player.getLocation().getBlockX() + x, player.getLocation().getBlockY(), player.getLocation().getBlockZ() + z);
				Location loc2 = new Location(player.getWorld(), player.getLocation().getBlockX() + x, player.getLocation().getBlockY() - 1, player.getLocation().getBlockZ() + z);

				Block blockunder = player.getWorld().getBlockAt(loc);
				Block blockunderunder = player.getWorld().getBlockAt(loc2);

				if ((blockunderunder.getTypeId() != 0) && (blockunder.getTypeId() == 0) && (blockunderunder.getTypeId() != 78) && (blockunderunder.getType() != Material.LONG_GRASS) && (blockunder.getType() != Material.SIGN)
				 && (blockunder.getType() != Material.WHEAT)&& (blockunder.getType() != Material.CHEST) && (blockunder.getType() != Material.LADDER)&& (blockunder.getType() != Material.GRAVEL)&& (blockunder.getType() != Material.WATER)){
					blockunder.setTypeId(78);
				}
				}
				}
				}
		// Mermaid
		if (BGKit.hasAbility(player, 50)) {
			if ((player.getLocation().getBlock().getType() == Material.WATER) || (player.getLocation().getBlock().getType() == Material.STATIONARY_WATER))
			{
				player.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 1000000, 0));
				player.addPotionEffect(new PotionEffect(PotionEffectType.WEAKNESS, 0, 0), true);
				player.addPotionEffect(new PotionEffect(PotionEffectType.WATER_BREATHING, 1000000, 0));
		      }
			else {
				player.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 0, 0), true);
				player.addPotionEffect(new PotionEffect(PotionEffectType.WEAKNESS, 1000000, 0));
				player.addPotionEffect(new PotionEffect(PotionEffectType.WATER_BREATHING, 0, 0), true);
			}
			}
		// Lava-God
		if (BGKit.hasAbility(player, 51)) {
			if ((player.getLocation().getBlock().getType() == Material.LAVA) || (player.getLocation().getBlock().getType() == Material.STATIONARY_LAVA))
			{
				player.addPotionEffect(new PotionEffect(PotionEffectType.FIRE_RESISTANCE, 1000000, 2));
				player.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 1000000, 0));
		      }
			else {
				player.addPotionEffect(new PotionEffect(PotionEffectType.FIRE_RESISTANCE, 0, 0), true);
				player.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 0, 0), true);
			}
			}
		if (BGKit.hasAbility(player, 58)) {
		if (byWall(player))
        {
            Location oldLoc = event.getFrom();
            Location newLoc = event.getTo();
            if(oldLoc.getY() > newLoc.getY())
            {
                newLoc.setY(oldLoc.getY());
                player.setVelocity(player.getVelocity().setY(0));
                event.setTo(newLoc);
            }
        }
		}
		/*
        // FlowerGirl/Boy
		if (BGKit.hasAbility(player, 52)) {
			Location loc = event. getPlayer ( ) . getLocation ( ) ;
			Location loca = event. getPlayer ( ) . getLocation ( ) ;
            World w = loc. getWorld ( ) ;

			if (player.getLocation().getBlock().getType() == Material.AIR)
			{
				loc.setY(loc.getY() -1);
				 
				int block = loc.getWorld().getBlockTypeIdAt(loc);
				if (block == 2){
            final Block bbbbbbvv = w. getBlockAt ( loca ) ;
            bbbbbbvv. setTypeId ( 37 ) ;
            plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable()
            {
                @Override
                public void run()
                {
                    bbbbbbvv. setTypeId ( 0 ) ;
                    
                    }
            }, 60L);
			}
			}
			w.playEffect(loc, Effect.SMOKE, 2000);
			}
		*/
		final Location loc = event. getPlayer ( ) . getLocation ( ) ;
		final World w = loc. getWorld ( ) ;
		final Boolean isSneaking = player.isSneaking();
		if((isSneaking == false) && (BGKit.hasAbility(player, 55))) {
		    plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable()
		    {
		        @Override
		        public void run()
		        {
		    		Location locaa = event. getPlayer ( ) . getLocation ( ) ;
		    		if (locaa == loc){
		            	player.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, 10000, 0));
		    		}
		    		if (locaa != loc){
		    			w.playEffect(locaa, Effect.SMOKE, 2000);
		    			player.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, 0, 0), true);
		    		}
		        }
		    }, 20L);
		}
			}
@EventHandler
public void onPlayerToggleSneak(PlayerToggleSneakEvent event) {
// Invisi-Runner - Really Buggy Check over code again
final Player player = event.getPlayer();
final Boolean isSneaking = player.isSneaking();
if((isSneaking == false) && (BGKit.hasAbility(player, 55))) {
    plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable()
    {
        @Override
        public void run()
        {
        	player.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, 1200, 0));
        }
    }, 40L);
} else if ((isSneaking == true) && (BGKit.hasAbility(player, 55)))
{
	player.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, 0, 0), true);
}
}

}
	/*	
	@EventHandler
	public void onPlayerDamage(EntityDamageEvent event) {
		
		Entity en = event.getEntity();
		if (en.getType() == EntityType.PLAYER) {
			Player player = (Player) en;
			if (BGKit.hasAbility(player, 100) && event.getDamage() > 1) {
				event.setDamage(event.getDamage() - 1);
			}
		}
	}
	@EventHandler
	public void onEntityTarget(EntityTargetEvent event) {
		
		Entity entity = event.getTarget();
		if (entity != null) {	
			if (entity.getType() == EntityType.PLAYER) {
			
				Player player = (Player) entity;
				if (BGKit.hasAbility(player, 120) && event.getReason() == TargetReason.CLOSEST_PLAYER) {
					event.setCancelled(true);
				}
			}
		}
	}
		*/
