package com.pvpkillz.abilities;




import java.util.logging.Logger;

import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;



import bukkitgames.API;

public class Main extends JavaPlugin{

	Logger log = Logger.getLogger("Minecraft");
	  public ConfigManager config;
	  public BGChest chest;
	  public Main plugin;
    public void onEnable()
    {
        this.config = new ConfigManager(this);
        abdesc();
        new Cooldown(this);
        new ABListener(this);
        new BGChest(this);
        new DeathListener(this);
        new FightListener(this);
        getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
        commandlisten();
        startChestspawn();
        log.info("[Extra-Abilities] Made by Undeadkillz, For PvPKillz");
        log.info("[Extra-Abilities] If you purchased this plugin, then please contact Undeadkillz on Bukkit.org");
        log.info("[Extra-Abilities] This plugin requires The BukkitGames to function correctly.");
    }

    public void startChestspawn() {
    	
    	this.getServer().getScheduler().runTaskTimerAsynchronously(this, new Runnable() {
    	    @Override  
    	    public void run() {
    	        for (Player pl : getServer().getOnlinePlayers()){
    	        	ABListener.ScoreBoard(pl);
    	        	}
    	    }
    	}, 35L, 35L);
        
        
    	this.getServer().getScheduler().runTaskTimerAsynchronously(this, new Runnable() {
    	    @Override  
    	    public void run() {
    	    	chest.spawnChest();
    	    }
    	}, 20 * 60 * 8, 20 * 60 * 5);
    	}
	public void commandlisten() {
    	getCommand("feastspawn").setExecutor(new CommandListener(this));
    	getCommand("rchest").setExecutor(new CommandListener(this));
    	getCommand("chest").setExecutor(new CommandListener(this));
    	getCommand("table").setExecutor(new CommandListener(this));
    	getCommand("rtable").setExecutor(new CommandListener(this));
    	getCommand("shutdown").setExecutor(new CommandListener(this));
    	getCommand("alltolobby").setExecutor(new CommandListener(this));
    	getCommand("coins").setExecutor(new CommandListener(this));
    	getCommand("shop").setExecutor(new CommandListener(this));

	}

	public void onDisable()
    {
        log.info("[Extra-Abilities] Plugin disabled");
    }

    public void abdesc()
    {
        API.addAbility(Integer.valueOf(41), config.readString("Abilities.41.Desc"));
        API.addAbility(Integer.valueOf(42), config.readString("Abilities.42.Desc"));
        API.addAbility(Integer.valueOf(43), config.readString("Abilities.43.Desc"));
        API.addAbility(Integer.valueOf(44), config.readString("Abilities.44.Desc"));
        API.addAbility(Integer.valueOf(45), config.readString("Abilities.45.Desc"));
        API.addAbility(Integer.valueOf(46), config.readString("Abilities.46.Desc"));
        API.addAbility(Integer.valueOf(47), config.readString("Abilities.47.Desc"));
        API.addAbility(Integer.valueOf(48), config.readString("Abilities.48.Desc"));
        API.addAbility(Integer.valueOf(49), config.readString("Abilities.49.Desc"));
        API.addAbility(Integer.valueOf(50), config.readString("Abilities.50.Desc"));
        API.addAbility(Integer.valueOf(51), config.readString("Abilities.51.Desc"));
        //API.addAbility(Integer.valueOf(52), config.readString("Abilities.52.Desc"));
        API.addAbility(Integer.valueOf(53), config.readString("Abilities.53.Desc"));
        API.addAbility(Integer.valueOf(54), config.readString("Abilities.54.Desc"));
        API.addAbility(Integer.valueOf(55), config.readString("Abilities.55.Desc"));
        API.addAbility(Integer.valueOf(56), config.readString("Abilities.56.Desc"));
        API.addAbility(Integer.valueOf(57), config.readString("Abilities.57.Desc"));
        API.addAbility(Integer.valueOf(58), config.readString("Abilities.58.Desc"));
        API.addAbility(Integer.valueOf(59), config.readString("Abilities.59.Desc"));
        API.addAbility(Integer.valueOf(60), config.readString("Abilities.60.Desc"));
        API.addAbility(Integer.valueOf(65), config.readString("Abilities.65.Desc"));
        API.addAbility(Integer.valueOf(69), config.readString("Abilities.69.Desc"));
        API.addAbility(Integer.valueOf(75), config.readString("Abilities.75.Desc"));
        API.addAbility(Integer.valueOf(76), config.readString("Abilities.76.Desc"));
    }
}
