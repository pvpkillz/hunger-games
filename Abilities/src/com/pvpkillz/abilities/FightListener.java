package com.pvpkillz.abilities;




import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
import java.util.logging.Logger;



import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Effect;
import org.bukkit.Material;
import org.bukkit.entity.Animals;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Egg;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Snowball;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import utilities.BGKit;
import utilities.BGTeam;

public class FightListener extends JavaPlugin implements Listener
{
	
	Logger log = Logger.getLogger("Minecraft");

	public Main plugin;
	public Cooldown cool;
	public FightListener(Main mainclass) {
		plugin = mainclass;
		mainclass.getServer().getPluginManager().registerEvents(this, mainclass);
		}
	public ArrayList<Player> HitterList = new ArrayList<Player>();
	public static HashMap<String, Double> AttackChecks = new HashMap<String, Double>();

	@EventHandler
	public void onEntityDamageByEntity(EntityDamageByEntityEvent event) {
		
		Entity damager = event.getDamager();
		Entity defender = event.getEntity();
	    if(event.getDamager() instanceof Egg){
	        Egg egg = (Egg) event.getDamager();
	         
	        if(egg.getShooter() instanceof Player){
		        Player shooter = (Player) egg.getShooter();
		        if(event.getEntity() instanceof Animals){
		        	Animals mob = (Animals) event.getEntity();
			    	if (BGKit.hasAbility(shooter, 43)) {
			        if(mob.getType() == EntityType.COW){
			        	mob.remove();
			        	PlayerInventory inv = shooter.getInventory();
			        	ItemStack item = new ItemStack(383, 1, (short) 92);
			        	inv.addItem(item);
		        	}
			        if(mob.getType() == EntityType.SHEEP){
			        	mob.remove();
			        	PlayerInventory inv = shooter.getInventory();
			        	ItemStack item = new ItemStack(383, 1, (short) 91);
			        	inv.addItem(item);
		        	}
			        if(mob.getType() == EntityType.MUSHROOM_COW){
			        	mob.remove();
			        	PlayerInventory inv = shooter.getInventory();
			        	ItemStack item = new ItemStack(383, 1, (short) 96);
			        	inv.addItem(item);
		        	}
			        if(mob.getType() == EntityType.PIG){
			        	mob.remove();
			        	PlayerInventory inv = shooter.getInventory();
			        	ItemStack item = new ItemStack(383, 1, (short) 90);
			        	inv.addItem(item);
		        	}
			        if(mob.getType() == EntityType.WOLF){
			        	mob.remove();
			        	PlayerInventory inv = shooter.getInventory();
			        	ItemStack item = new ItemStack(383, 1, (short) 95);
			        	inv.addItem(item);
		        	}
			        if(mob.getType() == EntityType.CHICKEN){
			        	mob.remove();
			        	PlayerInventory inv = shooter.getInventory();
			        	ItemStack item = new ItemStack(383, 1, (short) 93);
			        	inv.addItem(item);
		        	}
			    	}
			  }
			}
	        }
		    if(event.getDamager() instanceof Snowball){
		        Snowball snowball = (Snowball) event.getDamager();
		        Player shooter = (Player) snowball.getShooter();
		        if(snowball.getShooter() instanceof Player){
		        	if (event.getEntity() instanceof Player){
		        		Player def = (Player) event.getEntity();
						if ((BGTeam.isInTeam(shooter, def.getName())) && (BGTeam.isInTeam(def, shooter.getName()))){
							event.setCancelled(true);
							shooter.sendMessage(ChatColor.RED+ "No friendly fire | " + ChatColor.YELLOW + def.getName() + " is in your team, and you are on theirs.");
						}
						else if ((BGTeam.isInTeam(shooter, def.getName())) && (!BGTeam.isInTeam(def, shooter.getName()))){
							shooter.sendMessage(ChatColor.YELLOW + def.getName() + " is in your team, but you are not on theirs.");
						}
						else if ((!BGTeam.isInTeam(shooter, def.getName())) && (BGTeam.isInTeam(def, shooter.getName()))){
							shooter.sendMessage(ChatColor.YELLOW + def.getName() + " is not in your team, but you are on theirs.");
						}
						
						
						
						
						else if (BGKit.hasAbility(shooter, 45)) {
	        if(event.getEntity() instanceof Player){
			int dmg = plugin.config.readInt("Abilities.45.Damage");
	    	event.setDamage(dmg);
	    	}
			}
	        }
		    }
		    }
			  if ((damager instanceof Arrow) && (defender instanceof Player) && (damager instanceof Player)){
				  Arrow arrow = (Arrow) damager;
				  Player def = (Player) defender;
				  Player dam = (Player) arrow.getShooter();
				  
					if (BGTeam.isInTeam(dam, def.getName())){
						event.setCancelled(true);
						dam.sendMessage(ChatColor.RED+ "No friendly fire | " + ChatColor.YELLOW + def.getName() + " is in your team.");
					}
			    	if (BGKit.hasAbility(dam, 66)) {
			    		Random r = new Random();
			    		int Chance = plugin.config.readInt("Abilities.66.Chance");
			    		double randomValue = 100 * r.nextDouble();
			    		if (Chance >= randomValue){
		    				def.addPotionEffect(new PotionEffect(PotionEffectType.CONFUSION, 140, 3));
		    				def.addPotionEffect(new PotionEffect(PotionEffectType.POISON, 100, 2));     
			    		}
			    	}
			  }
		if (damager.getType() == EntityType.PLAYER && defender.getType() == EntityType.PLAYER) {
			// Creatures/Entities
			final Player dam = (Player)damager;
			final Player def = (Player)defender;
			/*
			double time = System.currentTimeMillis();
			if (AttackChecks.get(dam.getName()) == null)
			AttackChecks.put(dam.getName(), time);
			else{
				AttackChecks.get(dam.gern)
			}
			*/
			
			/*
			String teamdam;
			String teamdef;
				teamdam = BGTeam.getTeamList(dam).toString();
				teamdef = BGTeam.getTeamList(def).toString();
			if ((teamdef.contains(dam.getName()) == true) || (teamdam.contains(def.getName()) == true)){
			String TEAMED1 = teamdam.replace("[", " ");
			String TEAMED = TEAMED1.replace("]", " ");
			dam.sendMessage(ChatColor.RED + "REMINDER:" + ChatColor.YELLOW +" Your team has" + TEAMED + "in it. So avoid hitting them.");
		}
			else if ((teamdef.contains(dam.getName()) == true) && (teamdam.contains(def.getName()) == true)){
			String TEAMED1 = teamdam.replace("[", " ");
			String TEAMED = TEAMED1.replace("]", " ");
			dam.sendMessage(ChatColor.RED + "REMINDER:" + ChatColor.YELLOW +" Your team has" + TEAMED + "in it. So avoid hitting them.");
		event.setCancelled(true);
			}
			else {
			}
			*/
			// Work In Progress Code - Team blocking - Error In Console with Array -> String
			// End of Creatures/Entities
			if ((BGTeam.isInTeam(dam, def.getName())) && (BGTeam.isInTeam(def, dam.getName()))){
				event.setCancelled(true);
				dam.sendMessage(ChatColor.RED+ "No friendly fire | " + ChatColor.YELLOW + def.getName() + " is in your team, and you are on theirs.");
			}
			else if ((BGTeam.isInTeam(dam, def.getName())) && (!BGTeam.isInTeam(def, dam.getName()))){
				dam.sendMessage(ChatColor.YELLOW + def.getName() + " is in your team, but you are not on theirs.");
			}
			else if ((!BGTeam.isInTeam(dam, def.getName())) && (BGTeam.isInTeam(def, dam.getName()))){
				dam.sendMessage(ChatColor.YELLOW + def.getName() + " is not in your team, but you are on theirs.");
			}
			else{
			// OneHitter (Functional)
	    	if (BGKit.hasAbility(def, 48)) {
				int dmg = plugin.config.readInt("Abilities.48.DamageBlocked");
	    		if (def.isSneaking() == true) {
	    			if (dmg > event.getDamage()){
	    		    	event.setDamage(event.getDamage() - 1);
	    			}
	    			else {
	    	event.setDamage(event.getDamage() - dmg);
	    		}
	    			}
	    		if (dam.isSneaking() == true){
	    			event.setCancelled(true);
	    		}
	    				}
				if (BGKit.hasAbility(dam, 42) && event.getCause() == DamageCause.ENTITY_ATTACK) {
					if (!HitterList.contains(dam)) {
						HitterList.add(dam);
					cool.HitterCooldown(dam);
					int Hurt = plugin.config.readInt("Abilities.42.LOST");
					event.setDamage(event.getDamage()+ Hurt);
					dam.sendMessage(ChatColor.BLUE+"You just used your strongest hit..");
					def.sendMessage(ChatColor.RED+"Ahh! You were hit with "+dam.getName()+"'s strongest punch.");
					}
					else if (HitterList.contains(dam)){
						
					}
	                }
				}
				
				// Poisoner
				if (BGKit.hasAbility(def, 44) && event.getCause() == DamageCause.ENTITY_ATTACK) {
		    		Random r = new Random();
		    		int Chance = plugin.config.readInt("Abilities.44.Chance");
		    		double randomValue = 100 * r.nextDouble();
		    		if ((Chance >= randomValue) && (event.getCause() == DamageCause.ENTITY_ATTACK)){
    				dam.addPotionEffect(new PotionEffect(PotionEffectType.POISON, 100, 0));
		    		}
		    		
				}
		        // Monster (Gain powerups when attacking players, but receive confusion.)
			if (BGKit.hasAbility(dam, 47)) {
            	double ran = Math.random();
            	if (ran > 0.7){
				def.addPotionEffect(new PotionEffect(PotionEffectType.POISON, 80, 1));
				CommandListener.send(def, ChatColor.RED + "You have been poisoned!");
				CommandListener.send(dam, ChatColor.YELLOW + "You have poisoned " + def.getName());
				dam.getWorld().playEffect(dam.getLocation(), Effect.ZOMBIE_CHEW_IRON_DOOR, 1F);
            	}
				dam.getWorld().playEffect(dam.getLocation(), Effect.ZOMBIE_DESTROY_DOOR, 1F);
	            plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable()
	            {
	                @Override
	                public void run()
	                {
	    				dam.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 160, 0));
	                    }
	            }, 20L);      
			}
			if (BGKit.hasAbility(dam, 76)) {
				Material hand = dam.getItemInHand().getType();
				if ((Math.abs(dam.getLocation().getDirection().angle(def.getLocation().getDirection())) < 1) && 
					(hand == Material.STONE_SWORD)){
					CommandListener.send(dam,ChatColor.RED + "You backstabbed " + def.getName());
					CommandListener.send(def, "You were back stabbed, by " + dam.getName());
					event.setDamage(def.getHealth());
					}
				}
			}
		}
	}
