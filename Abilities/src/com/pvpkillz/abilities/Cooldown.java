package com.pvpkillz.abilities;

import java.util.Timer;
import java.util.TimerTask;


import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class Cooldown extends JavaPlugin {
	
	private Main plugin;

	public Cooldown(Main plugin) {
		
		this.plugin = plugin;
	}
	ABListener list;
	FightListener fight;
	public void HitterCooldown(final Player player) {
		
		TimerTask action = new TimerTask() {
			
			public void run(){
				
				fight.HitterList.remove(player);
			}
		};
		
		Timer timer = new Timer();
		timer.schedule(action, plugin.config.readInt("Abilities.42.Cooldown") * 1000);
	}
	public void MilkCooldown(final Player player) {
		
		TimerTask action = new TimerTask() {
			
			public void run(){
				
				list.MilkManList.remove(player);
			}
		};
		
		Timer timer = new Timer();
		timer.schedule(action, plugin.config.readInt("Abilities.41.Cooldown") * 1000);
	}
	public void FlyingAway(final Player player) {
		
		TimerTask action = new TimerTask() {
			
			public void run(){
				
				list.FlyingAway.remove(player);
			}
		};
		
		Timer timer = new Timer();
		timer.schedule(action, plugin.config.readInt("Abilities.68.Cooldown") * 1000);
	}
}
