package com.pvpkillz.abilities;


import java.text.DecimalFormat;
import java.util.List;
import java.util.Random;

import main.BGMain;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Chest;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;

import utilities.BGChat;
import utilities.BGFiles;
import utilities.enums.BorderType;

public class BGChest {
	public BGChest chest;
	public Main plugin;
	
	public BGChest(Main mainclass)
{
		plugin = mainclass;
}


public void spawnChest() {
Location loc;
do{
loc = BGMain.getRandomLocation();
}
while(!BGMain.inBorder(loc, BorderType.STOP));
plugin.getServer().getWorlds().get(0).loadChunk(plugin.getServer().getWorlds().get(0).getBlockAt(loc).getChunk());
spawnChest(loc);
}

public void spawnChest(Location l) {
plugin.getServer().getWorlds().get(0).loadChunk(plugin.getServer().getWorlds().get(0).getBlockAt(l).getChunk());
l.getBlock().setType(Material.CHEST);
Chest c = (Chest) l.getBlock().getState();

List<String> items = BGFiles.cornconf.getStringList("ITEMS");
for(String item : items) {
Random r = new Random();
String[] oneitem = item.split(",");
/*
if(maxAmount == minAmount)
amount = maxAmount;
else
amount = minAmount + r.nextInt(maxAmount - minAmount + 1);


if (item.contains(":") == true)
{
String[] IT = item.split(":");
int ID = Integer.parseInt(IT[0]);
short DURABILITY = Short.parseShort(IT[1]);
ItemStack i = new ItemStack(ID, amount);
i.setDurability(DURABILITY);
if(oneitem.length == 6) {
i.addUnsafeEnchantment(Enchantment.getById(Integer.parseInt(oneitem[4])),
Integer.parseInt(oneitem[5]));
}
c.getInventory().addItem(new ItemStack[] { i });
}
else
{
	int ID = Integer.parseInt(oneitem[0]);
	ItemStack i = new ItemStack(ID, amount);
	if(oneitem.length == 6) {
		i.addUnsafeEnchantment(Enchantment.getById(Integer.parseInt(oneitem[4])),
		Integer.parseInt(oneitem[5]));
		}
	c.getInventory().addItem(new ItemStack[] { i });
}
*/
if(Boolean.parseBoolean(oneitem[3]) == true) {
double random = Math.random();
if (random < 0.2){

Integer id = Integer.valueOf(Integer.parseInt(oneitem[0]));
Integer minAmount = Integer.valueOf(Integer.parseInt(oneitem[1]));
Integer maxAmount = Integer.valueOf(Integer.parseInt(oneitem[2]));
String itemid = oneitem[0];
Short durability = null;
Integer amount = r.nextInt((maxAmount+1)-minAmount) + minAmount;


if (item.contains(":") == true) {
String[] it = itemid.split(":");
id = Integer.parseInt(it[0]);
durability = Short.parseShort(it[1]);
} 
else {
id = Integer.parseInt(itemid);
}
if(maxAmount == minAmount){
amount = maxAmount;
}
else{
amount = minAmount + r.nextInt(maxAmount - minAmount + 1);
}
ItemStack i = new ItemStack(id, amount);

if (item.contains(":") == true) {
i.setDurability(durability);
}
else 
{
}
if(oneitem.length == 6) {
i.addUnsafeEnchantment(Enchantment.getById(Integer.parseInt(oneitem[4])),
Integer.parseInt(oneitem[5]));
}

c.getInventory().addItem(new ItemStack[] { i });
}
}

else if(Boolean.parseBoolean(oneitem[3]) == false) {
double random = Math.random();
if (random < 0.03){

	Integer id = Integer.valueOf(Integer.parseInt(oneitem[0]));
	Integer minAmount = Integer.valueOf(Integer.parseInt(oneitem[1]));
	Integer maxAmount = Integer.valueOf(Integer.parseInt(oneitem[2]));
	String itemid = oneitem[0];
	Short durability = null;
	Integer amount = r.nextInt((maxAmount+1)-minAmount) + minAmount;

	if (item.contains(":") == true) {
		String[] it = itemid.split(":");
		id = Integer.parseInt(it[0]);
		durability = Short.parseShort(it[1]);
		} 
		else {
		id = Integer.parseInt(itemid);
		}
		if(maxAmount == minAmount){
		amount = maxAmount;
		}
		else{
		amount = minAmount + r.nextInt(maxAmount - minAmount + 1);
		}
		ItemStack i = new ItemStack(id, amount);

		if (item.contains(":") == true) {
		i.setDurability(durability);
		}
		else 
		{
		}
		if(oneitem.length == 6) {
		i.addUnsafeEnchantment(Enchantment.getById(Integer.parseInt(oneitem[4])),
		Integer.parseInt(oneitem[5]));
		}

		c.getInventory().addItem(new ItemStack[] { i });
		}
		}
}

c.update(true);
DecimalFormat df = new DecimalFormat("##.#");
BGChat.printInfoChat("A Random chest was spawned... Location X: " + df.format(l.getX())
+ " | Y: " + df.format(l.getY()) + " | Z: "
+ df.format(l.getZ()));
}

public void spawnTable() {

Location loc;

do{

loc = BGMain.getRandomLocation();
}while(!BGMain.inBorder(loc, BorderType.STOP));

spawnTable(loc);
}

public void spawnTable(Location l) {
plugin.getServer().getWorlds().get(0).loadChunk(plugin.getServer().getWorlds().get(0).getBlockAt(l).getChunk());

l.getBlock().setType(Material.ENCHANTMENT_TABLE);
DecimalFormat df = new DecimalFormat("##.#");
BGChat.printInfoChat("An Enchanting table was spawned... Location X: "
+ df.format(l.getX()) + " | Y: " + df.format(l.getY())
+ " | Z: " + df.format(l.getZ()));
}
}