package com.pvpkillz.abilities;


import java.util.HashMap;
import java.util.logging.Logger;



import org.bukkit.ChatColor;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Monster;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.plugin.java.JavaPlugin;

import utilities.BGKit;

public class DeathListener extends JavaPlugin implements Listener
{
	
	Logger log = Logger.getLogger("Minecraft");

	public Main plugin;
	
	public DeathListener(Main mainclass) {
		plugin = mainclass;
		mainclass.getServer().getPluginManager().registerEvents(this, mainclass);
		}
	
	HashMap<String, Integer> KillCount = new HashMap<String, Integer>();
	
	
@EventHandler
public void WhenPlayerKills (PlayerDeathEvent event){
	Entity damager = event.getEntity().getKiller();
	Entity defender = event.getEntity().getPlayer();

	double randomValue = Math.random();
	if (damager instanceof Monster){
	if ((0.50 <= randomValue) && (0 >= randomValue)){
	event.setDeathMessage(damager +  " has slaughtered " + defender);
	}
	else if ((1 <= randomValue) && (.51 >= randomValue)){
		event.setDeathMessage(defender + " was killed by an angry " + damager);
	}
	}
	else if (damager instanceof Player){
		
		Player def = (Player)defender;
		Player dam = (Player)damager;
		String Weapon = dam.getItemInHand().getType().toString().toLowerCase().replace("_", " ");
		/*
		BGMain.SQLquery("INSERT INTO `WEAPONS` ('WEAPON', `HOLDER`, `KILLED`) VALUES ("
	+ Weapon.toString()
	+ ","
	+ BGMain.getPlayerID(dam.getName())
	+ ","
	+ BGMain.getPlayerID(def.getName())
	+ ") ;");
		*/
		boolean Weaponb = dam.getItemInHand().getType().isBlock();
		
		Player dp = event.getEntity();
		Player killer = dp.getKiller();
		if (Weapon.contains("sword") == true){
		if (randomValue < 0.20){
		event.setDeathMessage(dam.getName() +  " has finished off " + def.getName() + " with their mighty " + Weapon);
		}
		else if (randomValue < 0.40){
			event.setDeathMessage(dam.getName() +  " has ended the life of " + def.getName() + " with a " + Weapon);
		}
		else if (randomValue < 0.60){
			event.setDeathMessage(dam.getName() +  " has taken away the soul of " + def.getName() + " wielding a " + Weapon);
		}
		else if (randomValue < 0.80){
			event.setDeathMessage(def.getName() + " did not notice " + dam.getName());
		}
		else if (randomValue < 1){
			event.setDeathMessage(dam.getName() +  " attacked " + def.getName() + " ending their life with a " + Weapon);
		}
		else {
		}
		}
		else if (Weapon.contains("bow") == true){
			event.setDeathMessage(def.getName() +  " has taken an arrow to the knee by " + dam.getName());
		}
		else if (Weaponb == true){
			event.setDeathMessage(dam.getName() +  " has humiliated " + def.getName() + " with a " + Weapon);
		}
		else {
			if (Weapon.contains("air")){
				event.setDeathMessage(dam.getName() +  " embarrassed " + def.getName() + " with their hand");
			}
			event.setDeathMessage(dam.getName() +  " slayed " + def.getName() + " with a " + Weapon);
		}
		if (BGKit.hasAbility(killer, 56)) {
			if (!KillCount.containsKey(killer.getName())){
			    KillCount.put(killer.getName(), 0);
			}
		
		KillCount.put(killer.getName(), KillCount.get(killer.getName()) + 1);
		if (KillCount.get(killer.getName()) >= 10)
		{
		killer.giveExpLevels(8);
		}
		else if (KillCount.get(killer.getName()) == 9)
		{
		killer.giveExpLevels(7);
		}
		else if (KillCount.get(killer.getName()) == 8)
		{
		killer.giveExpLevels(6);
		}
		else if (KillCount.get(killer.getName()) == 7)
		{
		killer.giveExpLevels(5);
		}
		else if (KillCount.get(killer.getName()) == 6)
		{
		killer.giveExpLevels(4);
		}
		else if (KillCount.get(killer.getName()) == 5)
		{
		killer.giveExpLevels(3);
		}
		else if (KillCount.get(killer.getName()) <= 4)
		{
		killer.giveExpLevels(1);
		}
		killer.sendMessage(ChatColor.GREEN+"[Enchanter] So far you have Killed: " + KillCount.get(killer.getName()) + " tributes.");
		}
	}
	else 
	{
	}
		}
	
	
}
