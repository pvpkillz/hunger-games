package kz.pvp.PvPGames.Utilities;

import kz.PvP.PkzAPI.methods.ConvertTimings;
import kz.PvP.PkzAPI.methods.itemModifications;
import kz.pvp.PvPGames.Main.MainHG;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.BlockState;
import org.bukkit.block.Chest;
import org.bukkit.inventory.ItemStack;
import java.util.List;
import java.util.Random;
public class ChestSpawn {
    public MainHG plugin;

    public ChestSpawn(MainHG mainclass) {
        plugin = mainclass;
    }

    public static void spawnChest (Location loc) {
        loc.getChunk().load();
        for (int y = 0; y <= 40; y++){
        	loc.clone().add(0,y,0).getBlock().setType(Material.AIR);
        }
        loc.getWorld().spawnFallingBlock(loc.add(0,40,0), Material.PISTON_BASE, (byte) 6);
        
        BlockState bs = loc.getBlock().getState();

        if (bs.getBlock().getType() == Material.CHEST) {
            Chest chest = (Chest) bs.getBlock().getState();
            
            chest.update();
        }
    }
}