package kz.pvp.PvPGames.Utilities;

import kz.PvP.PkzAPI.methods.ConvertTimings;
import kz.PvP.PkzAPI.utilities.Message;
import kz.pvp.PvPGames.Main.MainHG;
import kz.pvp.PvPGames.Methods.Generation;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public class Abilities {
	/*                    Ability IDs                    */
	public static final int AbilityExplosiveArrow = 1;// Done
	public static final int AbilitySnowballConfuse = 2;// Done
	public static final int AbilityEnderPearlRoom = 3;// Done
	public static final int AbilityInstaBowKill = 4;// Done
	public static final int AbilityThor = 5;// Done
	public static final int AbilityStoneDmg = 6;// Done
	public static final int AbilityExplodeDeath = 7;// Done
	public static final int AbilityHideFromMobs = 8;// Done
	public static final int AbilityInvisibilityFood = 9;// Done
	public static final int AbilitySuperJump = 10;// Done
	public static final int AbilityFrost = 11;// Done
	public static final int AbilitySnowGround = 12;// Done
	public static final int AbilityCrouchBlock = 13;// Done
	public static final int AbilityTNTActivate = 14;// Done
	public static final int AbilityInstantBackstab = 15;// Done
	public static final int AbilityTeleportWand = 16;// Done
	public static final int AbilityArcherUpgrade = 17;// Done
	public static final int AbilityCharmeleon = 18;// Done
	public static final int AbilityExplosivePlates = 19;// Done
	public static final int AbilityKnockBackShift = 20;// Done
	public static final int AbilityBreakAllLogs = 21;//Done
	public static final int AbilityCookieStrength = 22;// Done
	public static final int AbilityPigHunter = 23;// Done
	public static final int AbilityBreakFall = 24;// Done
	public static final int AbilityCropsGrow = 25;// Not needed at this time
	public static final int AbilityHealWithHit = 26;// Done
	public static final int AbilityRefillFoodKill = 27;// Done
	public static final int AbilityStealItems = 28;// Done
	public static final int AbilityPoisonHit = 29;// Done
	public static final int AbilityPokemonHunt = 30;// Done
	public static final int AbilityMilkDrinker = 31;// Done?
	public static final int AbilitySnowballDmg = 32;// Done
	public static final int AbilityBeaconPowerup = 33;// Done
	public static final int AbilityNightHunter = 34;// Done
	public static final int AbilityUnderWaterPower = 35;// Done
	public static final int AbilitySmeltOres = 36;// Done
	public static final int AbilityXPKiller = 37;//Done
	public static final int AbilitySnowSpawner = 38;//Done
	public static final int AbilitySuperFly = 39;//Done
	public static final int AbilityHider = 40;// Not needed atm?


	/*               Abilities Settings               */
	public static final int SnowballConfusionRadius = 4;
	public static final int SnowballConfusionLength = 10;
	public static final int SnowballCooldown = 5;
	public static final int EnderPearlRoomCooldown = 120;
	public static final double ArrowInstaKillDistance = 25;
	public static final int ThorCooldown = 30;
	public static final Material InvisibilityFoodMaterial = Material.APPLE;
	public static final int InvisibilityFoodLength = 25;
	public static final int InvisibilityFoodCooldown = 15;
	public static final Material SuperJumpMaterial = Material.PAPER;
	public static final int SuperJumpCooldown = 25;
	public static final Material FrostGround = Material.SNOW_BLOCK;
	public static final Material FrostGroundWater = Material.ICE;
	public static final Material FrostGroundLava = Material.OBSIDIAN;
	public static final Material InstantBackstabWeapon = Material.WOOD_SWORD;
	public static final double InstantBackstabHP = 6;
	public static final Material TeleportWandItem = Material.BLAZE_ROD;
	public static final int TeleportWandCooldown = 65;
	public static final int TeleportWandStrength = 75;// 75 Blocks...
	public static final Material SuperFlyItem = Material.FEATHER;
	public static final int SuperFlyLength = 10;
	public static final int SuperFlyCooldown = 60;
	public static final int CookieStrengthLength = 30;// Seconds for strength to stay
	public static final int StealItemsCooldown = 45;
	public static final Material StealItemsStick = Material.STICK;
	public static final int MilkDrinkingCooldown = 50;
	public static final int PoisonHitChance = 15;
	public static final double SnowballDamage = 5;// Preset damage for the snowball damage.




	public static MainHG plugin;

	public static HashMap<Integer, HashMap<String, Long>> Cooldown = new HashMap<Integer,  HashMap<String, Long>>();// GameMakers in the server / Hashmap incase we want to convert to multiple arenas
	public static HashMap<String, ArrayList<Location>> PlateLocations = new HashMap<String,  ArrayList<Location>>();// Plate Locations...
	public static HashMap<String, Integer> HoldTimers = new HashMap<String, Integer>();// Collect the kills with bow
	public static HashMap<String, Integer> HoldStrength = new HashMap<String, Integer>();// Collect the kills with bow
	public static HashMap<String, UUID> SpecialArrow = new HashMap<String, UUID>();// Collect the kills with bow
	public static ArrayList<String> ShootingSpecial = new ArrayList<String>();// Collect the kills with bow

	public static HashMap<String, Integer> MilkDrinks = new HashMap<String, Integer>();// Collect the kills with bow
	public static HashMap<String, Integer> ArcherUpgrade = new HashMap<String, Integer>();// Collect the kills with bow
	public static HashMap<String, Location> EnderPearlRoomLocs = new HashMap<String, Location>();// GameMakers in the server / Hashmap incase we want to convert to multiple arenas

	public Abilities(MainHG mainclass) {
		plugin = mainclass;

		for (String entry : FilesHG.abilitiesconf.getCustomConfig().getConfigurationSection("Abilities").getKeys(false)){
			List<String> Description = FilesHG.abilitiesconf.getCustomConfig().getStringList("Abilities." + entry + ".Description");
			for (String desc : Description){
				KitsHG.addAbility(Integer.parseInt(entry), desc);
			}
		}
	}


	public static boolean isCooled(Player p, int ID) {
		if (!Cooldown.containsKey(ID)){
			Cooldown.put(ID, new HashMap<String, Long>());
		}
		if (Cooldown.get(ID).containsKey(p.getName()))
			return false;
		else
			return true;
	}

	// Setting of the cooldown... We simplified it by ALOT from the old version of the plugin
	public static void cooldown(final Player p, final int ID, int Seconds) {
		if (!Cooldown.containsKey(ID)) {
			Cooldown.put(ID, new HashMap<String, Long>());
		}

		Cooldown.get(ID).put(p.getName(), System.currentTimeMillis()/1000);

		plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
			public void run() {
				Cooldown.get(ID).remove(p.getName());
			}
		}, 20L * Seconds);
	}

	public static int getCooldownLeft(Player p, int ID, int totalCooldown){
		int timeLeft = 0;

		if (Cooldown.containsKey(ID)){
			if (Cooldown.get(ID).containsKey(p.getName())){
				Long curTime = Cooldown.get(ID).get(p.getName());
				timeLeft = (int) ((System.currentTimeMillis()/1000) - curTime);
				timeLeft = totalCooldown - Math.abs(timeLeft);
			}
		}
		return timeLeft;
	}


	// Get players within a radius of a location
	public static List<Player> getPlayersAround(Location startLoc, int i) {
		List<Player> players = new ArrayList<Player>();
		for (String pl : MainHG.Gamers){
			if (Bukkit.getPlayer(pl).getLocation().distance(startLoc) <= i){
				players.add(Bukkit.getPlayer(pl));
			}
		}
		return players;
	}


	public static void generateEnderPearlRoom(final Location location, Player shooter) {
		generateRoom(location, 3, 5, Material.OBSIDIAN);
		int Time = 30;
		EnderPearlRoomLocs.put(shooter.getName(), shooter.getLocation());
		shooter.teleport(location);

		Message.P(shooter, Message.Replacer(MessageHG.EnderpearlTP, ConvertTimings.convertTime(Time), "%time"), true);
		final Player shoot = shooter;
		plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
			public void run() {
				Message.P(shoot, MessageHG.EnderpearlBack, true);
				generateRoom(location, 3, 5, Material.AIR);
				shoot.teleport(EnderPearlRoomLocs.get(shoot.getName()));
				EnderPearlRoomLocs.remove(shoot.getName());
			}
		}, 20L * Time);
	}

	public static void generateRoom(Location loc, int Radius, int Height, Material material) {

		for (int x = -Radius; x <= Radius; x++) {
			for (int y = -Radius; y <= Radius; y++) {
				for (int z = -Radius; z <= Radius; z++) {
					if (y >= 0){
						Location temp = loc.clone().add(x, y, z);
						if (temp.distanceSquared(loc) <= (Radius * Radius)) {
							if (temp.distanceSquared(loc) < ((Radius - 2) * Radius)) {
								if (y == 0)
									loc.getWorld().getBlockAt(temp).setType(material);
								else
									loc.getWorld().getBlockAt(temp).setType(Material.AIR);
								Generation.cblocks.add(temp);
							} else {
								if (y == 0 || temp.getBlockZ() == loc.getBlockZ() || temp.getBlockX() == loc.getBlockX())
									loc.getWorld().getBlockAt(temp).setType(Material.GLOWSTONE);
								else
									loc.getWorld().getBlockAt(temp).setType(material);
								Generation.cblocks.add(temp);
							}
						}
					}
				}
			}
		}
	}

	// I don't want to keep making a huge line such as this, a lot. This is a nice method.
	public static void sendCooldown(Player p, int ID, int totalCooldown) {
		Message.P(p, Message.Replacer(MessageHG.WaitCoolDown, ConvertTimings.convertTime(Abilities.getCooldownLeft(p, ID, totalCooldown)), "%time"), false);
	}

	// Useful method to grab blocks within a radius
	public static ArrayList<Block> getBlocksAround(Location location, int Radius, int Height) {
		ArrayList<Block> blocks = new ArrayList<Block>();
		for (int x = -Radius; x <= Radius; x++){
			for (int z = -Radius; z <= Radius; z++){
				for (int y = -Height; y <= Height; y++){
					Location loc = location.clone();
					Block b = loc.add(x,y,z).getBlock();
					if (b.getType() != Material.AIR)
						blocks.add(b);
				}
			}
		}

		return blocks;
	}

	// Decrease ItemStack by 1... Will be used a lot...
	public static void decreaseItemStack(ItemStack item, int i) {
		int Amount = item.getAmount();

		if (Amount >= 2) {
			item.setAmount(Amount - 1);
		} else if (Amount == 1) {
			item.setType(Material.AIR);
		}
	}

	//
	public static void setBlock(Block block, Material newMaterial) {
		if (!Generation.isCornucopiaBlock(block)) {
			block.setType(newMaterial);
		}
	}


	public static void upgradeBow(Player dam, int i) {
		ItemStack bow = null;
		if (dam.getItemInHand().getType() == Material.BOW && dam.getItemInHand().getAmount() == 1) {
			bow = dam.getItemInHand();
		} else {
			for (ItemStack is : dam.getInventory().getContents()) {
				if (is.getType() == Material.BOW && is.getAmount() == 1) {
					bow = is;
					break;
				}
			}
		}

		if (bow != null) {
			if (!(bow.hasItemMeta() && bow.getItemMeta().hasDisplayName() && bow.getItemMeta().getDisplayName().contains("Level " + i))) {
				ItemMeta im = bow.getItemMeta();
				im.setDisplayName(ChatColor.RED + "Bow" + ChatColor.GRAY + " - " + ChatColor.GREEN + "Level " + i);
				bow.setItemMeta(im);
			}
		}
	}

	public static void causeExplosion(Location loc, float strength) {
		loc.getWorld().createExplosion(loc, strength);
	}

	public static boolean isShootingSpecial(Player shooter) {
		return ShootingSpecial.contains(shooter.getName());
	}
}
