package kz.pvp.PvPGames.Utilities;

import kz.PvP.PkzAPI.methods.PlayersInfo;
import kz.PvP.PkzAPI.utilities.Message;
import kz.PvP.PkzAPI.utilities.Mysql;
import kz.PvP.PkzAPI.utilities.Stats;
import kz.pvp.PvPGames.Enums.ModType;
import kz.pvp.PvPGames.Main.MainHG;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

public class StatsHG {
	public static MainHG plugin;
	public static HashMap<String, Integer> totalScore = new HashMap<String, Integer>();// Total score of a player - Polled from DB
	public static HashMap<String, Integer> roundScore = new HashMap<String, Integer>();// Round Score of a player - During round
	public static HashMap<String, ArrayList<String>> Assists = new HashMap<String, ArrayList<String>>();// Total Assists during game
	public static HashMap<String, ArrayList<String>> teams = new HashMap<String, ArrayList<String>>();// Team list
	public static HashMap<UUID, Boolean> wonLastRound = new HashMap<UUID, Boolean>();// Total score of a player - Polled from DB


	public StatsHG (MainHG mainclass){
		plugin = mainclass;
	}

	/*  All add Methods */

	public static void addToAssist(Player dam, Player def) {
		if (!Assists.containsKey(dam.getName())) {
			Assists.put(dam.getName(), new ArrayList<String>());
		}

		if (!Assists.get(dam.getName()).contains(def.getName())) {
			Assists.get(dam.getName()).add(def.getName());
		}
	}


	public static void modifyTeam (Player p, String teamPlayer, ModType mt) {
		if (MainHG.EnableTeams) {
			if (mt == ModType.Add) {
				if (!isInTeam(p, teamPlayer)) {
					StatsHG.teams.get(p.getName()).add(teamPlayer);
				}
			} else if (mt == ModType.Remove) {
				if (isInTeam(p, teamPlayer)) {
					StatsHG.teams.get(p.getName()).remove(teamPlayer);
				}
			}
		}
	}

	/*  All get Methods */

	public static boolean isInTeam(Player p, String teamPlayer) {
		if (!teams.containsKey(p.getName())) {
			teams.put(p.getName(), new ArrayList<String>());
		}

		return teams.get(p.getName()).contains(teamPlayer);
	}

	public static ArrayList<String> getTeam(Player p) {
		if (!teams.containsKey(p.getName())) {
			teams.put(p.getName(), new ArrayList<String>());
		}

		return teams.get(p.getName());
	}

	public static ArrayList<String> getAssists(Player p) {
		if (!Assists.containsKey(p.getName())) {
			Assists.put(p.getName(), new ArrayList<String>());
		}

		ArrayList<String> Assist = Assists.get(p.getName());
		return Assist;
	}

	/*
	 * MySQL Functions below... For agthering data from DB... :D
	 */

	public static void addScore(String p) {
		if (!roundScore.containsKey(p)) {
			roundScore.put(p, 0);
		}

		if (!Stats.RoundFame.containsKey(p)) {
			Stats.RoundFame.put(p, 0);
		}

		int amount;

		if (Stats.RoundKills.get(p) <= 4) {
			amount = Stats.getKills(p);
		} else if (Stats.RoundKills.get(p) <= 6) {
			amount = (Stats.RoundFame.get(p) + 2);
		} else {
			amount = (Stats.RoundFame.get(p) + 4);
		}

		if (PlayersInfo.getPlayer(p) != null) {
			Player pl = PlayersInfo.getPlayer(p);
			if (pl.hasPermission(MessageHG.Perm1X5Score)) {
				amount = (int) Math.round(amount * 1.5);
			} else if (pl.hasPermission(MessageHG.PermDoubleScore)) {
				amount = amount * 2;
			}
		}

		Stats.RoundFame.put(p, amount);
		Message.P(PlayersInfo.getPlayer(p), Message.Replacer(MessageHG.EarnedFame, "" + amount, "<fame>"), false);
		roundScore.put(p, roundScore.get(p) + amount);
		totalScore.put(p, Stats.getScore(p));
	}

	public static boolean lastRoundWin(Player p) {
		if (!wonLastRound.containsKey(p.getUniqueId())){
			ResultSet rs = Mysql.PS.getSecureQuery("SELECT * FROM HungerGames_Rounds WHERE Contestants Like CONCAT('%', ?, '%') OR CONCAT('%', ?) OR CONCAT(?, '%')", 
					","+Mysql.getUserID(p.getName())+",",
					","+Mysql.getUserID(p.getName()),
					Mysql.getUserID(p.getName()) + ",");
			try {
				if (rs.next()){
					// Last round player was in
					if (rs.getInt("Winner") == Mysql.getUserID(p.getName())){
						// He won last round.
						wonLastRound.put(p.getUniqueId(), true);
						return true;
					}
				}
			} catch (SQLException e) {
				Message.C("Failed to grab last game by " + p.getName() + " and reason is " + e.getMessage());
			}
			wonLastRound.put(p.getUniqueId(), false);
			return false;
		}
		else
			return wonLastRound.get(p.getUniqueId());
	}

}
