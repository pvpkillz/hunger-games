package kz.pvp.PvPGames.Utilities;

import kz.PvP.PkzAPI.utilities.Message;
import kz.PvP.PkzAPI.utilities.Stats;
import kz.pvp.PvPGames.Enums.Team;
import kz.pvp.PvPGames.Main.MainHG;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.sql.SQLException;
import java.util.ArrayList;



public class MessageHG {
    public static MainHG plugin;

	public static String mapHasWon = "&6The map chosen for this round is &7'&a%map&7'";

	public static String spawnedCornucopia = "&a&lThe cornucopia chests have been spawned!";

	public static String stoleFrom = "You have stolen an item.";

	public static String stolenFrom = "You have been stolen from...";

	public static String unableToDrinkMilk = "You have outgrown your ability to drink milk.. :(";

	public static String startingIn = "&aGame starting in %time";

    public static final String PREFIX = ChatColor.RED + "[" + ChatColor.GOLD + "PkzGames" + ChatColor.RED + "] " + ChatColor.GREEN;
	/*
	 *  Translation file (Will be extra, since this plugin will be used in English, but I need a challenge)
	 */

    public static final String ServerName = "PvPKillz";
    public static final String NotEnoughOn = ChatColor.GREEN + "Not enough players on to begin the tournament.";
    public static final String DoNotNotify = ChatColor.GRAY + "You currently are using the default kit. To choose a kit type " + ChatColor.GREEN + "/class" + ChatColor.GRAY + ". Turn off this notification, with " + ChatColor.RED + "/mv notify off";
    public static final String TimeLeft = "This tournament will end in %time.";
    //public static final String TypeHelpforHelp = "Type /help at anytime for more help.";
    //public static final String FeedbackPlease = "&aPlease help us &7make this gamemode better by leaving your feedback. Type &a/Feedback &7for more info.";
    public static final String CantChooseKit = "You are unable to choose a kit at this time.";
    public static final String ChoosenKit = "&6You have chosen the &7%kit&6 kit for this round.";
    public static final String InvalidKit = "&cError: &aInvalid kit. Please try again.";
    private static final String YourKits = "Your Kits";
    private static final String SpecialKits = "Special Kits";
    private static final String GetMoreKits = SpecialKits + " can be purchased from " + MainHG.WebsiteForKits;
    public static final String GameIsInProgress = "&5The game is currently in progress.";
    public static final String GameIsFull = "&bThe game is currently &cfull&b.";
    public static final String JoinedAsPlayerSwitchToGM = "&7You have joined as a player, to switch to gamemaker mode type &a/gamemaker.";
    public static final String Day = "day";
    public static final String Days = "days";
    public static final String HaveTimeLeft = "&7You have &5%time&7 to enter the command.";
    public static final String Hours = "hours";
    public static final String Hour = "hour";
    public static final String Minutes = "minutes";
    public static final String Minute = "minute";
    public static final String Second = "second";
    public static final String Seconds = "seconds";
    public static final String MapVoteEnds = "&aMap vote ends in &c%time&a.";
    public static final String GameBegins = "&7Tournament &abegins&7 in &c%time&a on &a&l%map.";
    public static final String ReachedEndOfMap = "You have reached the border of this map.";
    public static final String GracePeriodStarted = "&aGrace period is active for &c%time&a.";
    public static final String BattleActive = "The battle is now active.";
    public static final String MayTheOdds = "&aMay the odds be ever in your favour!";
    public static final String UnableAtThisTime = "&cError: &7You are unable to do this at this time.";
    public static final String LightningLocatorActive = "&aLightning locator will now strike-upon all tributes.";
    public static final String LightningLocatorInactive = "&aLightning locator has been &cdeactivated.";
    public static final String FireworksLocatorActive = "&aFireworks locator will now launch from all tributes.";
    public static final String FireworksLocatorInactive = "&aFireworks locator has been &cdeactivated.";
    public static final String TypeHelpforHelp = "Type /help for more info about this game.";
    public static final String HardCoreName = "a Hardcore Hunger-Games round";
    public static final String HardcoreModeBegins = "&cHardcore mode&a will begin in %time.";
    public static final String CannotDoAtThisTime = "&cYou are unable to do this at this time.";
    public static final String HardcoreIsActive = "&6Hardcore Hunger-Games is currently active";
    public static final String HardCore = "&8= &7Hardcore Hunger-Games &8=";
    public static final String LocateCompassDesc = "Right-click while holding \nthis compass to \nlocate nearby players.";
    public static final String SpectateCompassDesc = "Right-click while holding \nthis compass to \nview a list of players \ncurrently alive.";
    public static final String GamemakerPanelDesc = "Right-click while holding \nthis item, to \nview a list of executable \nactions for gamemakers.";
    public static final String LocateCompassName = "&6Locator Compass";
    public static final String GamemakerPanelName = "&4*&a Gamemaker Panel &4*";
    public static final String SpectateCompassName = "&7Spectate Compass";
    public static final String UserLocated = ChatColor.GRAY + "The compass is tracking tribute %user.";
    public static final String NoCompassResult = ChatColor.GRAY + "There are no players nearby. Pointing to cornucopia.";
    public static final String YourAreSpectating = "You are now a spectator of this round!";
    public static final String YourAreGamemaking = "You are now gamemaking this round, modify the game responsibly. For the gamemaker control panel type /cp";
    public static final String CommandNotUsable = "Sorry, but this command has ended its timeout.";
    public static final String KitNeedsPurchase = "Sorry, but this kit is special. Purchase it at: " + MainHG.WebsiteForKits;
    public static final String YouHaveRejoinedTheGame = "You have been re-entered into the tournament.";
    public static final String TributeFellDC = "Tribute %user has fallen, and has been eliminated. (Disconnected for too long)";
    public static final String WehaveaWinner = "The winner of this round is %winner.";
    public static final String YouHaveWonThisRound = "Congratulations! You have won the Hunger-Games tournament!";
    public static final String GivenBandaids = "Every tribute has been given %bandaids bandaids.";
    public static final String DontScrewWithMe = "Buddy, don't screw around with me. I am only a machine.";
    public static final String GivenJumpboost = "Every tribute has been given jump boost.";
    public static final String IncreaseTime = "Time is too short...";
    public static final String ThisFunctionIsDisabled = "Sorry, but this function is disabled.";
    public static final String BandaidName = ChatColor.RED + "+ " + ChatColor.WHITE + "Player Bandaid" + ChatColor.RED + " +";
    public static final String AtFullHealth = "You are already at full health.";
    public static final String HealedToFull = "You have healed yourself to full health & removed all potion effects.";
    public static final String HardcoreModeBegan = "Hardcore mode has now been activated.";
    public static final String FeedbackHelp = "Thank you for your intrest in giving us your feedback.";
    public static final String FeedbackHelp1 = "Please return to us what you believe will improve the hunger-games experience.";
    public static final String FeedbackHelp2 = "/Feedback <Response>";
    public static final String ThanksForFeedback = "Thank you for your feedback!";
    public static final String FeedbackWillRead = "This message will be inspected by our team of staff, and any bugs/issues will be fixed ASAP.";
    public static final String PermGamemaker = "pvpgames.gamemaker";
    public static final String PermJoinFull = "pvpgames.join.full";
    public static final String PermSpectator = "pvpgames.spectator";
    public static final String PermAdminPanel = "pvpgames.admin.panel";
    public static final String NoPermission = "You do not have permission to do this.";
    public static final String NotPlayer = "You need to be a player to do this.";
    public static final String PermKitPre = "pvpgames.kit";
    public static final String PermVIPKit = "pvpgames.vip.allkits";
    public static final String EliminatedReason = "%killer has eliminated you.";
    public static final String ChatBorder = "&c=================================";
    public static final String CurrentMap = "This round's map will be: &a%map";
    public static final String MapAuthor = "Map is made by: &a%author";
    public static final String SubmitAMap = ChatColor.GREEN + "" + ChatColor.UNDERLINE + "How to submit a map?";
    public static final String HowToSubmitAMap = "To submit a map, you must know the name of the map and the original creator, once you have these 2 pieces of information, you proceed by entering this information in the feedback command in this format: 'Mapname: NAME, Author: NAME, LinkToDL: LINK' We suggest using http://bit.ly to shorten the link. If you would like you can also place the author and map info in a text file and link it to us.";
    public static final String MobsSpawningPer = "Toggled: Mobs spawning close to %player.";
    public static final String MobsSpawningAll = "Toggled: Mobs spawning close to all tributes.";
    public static final String KitInfo = "====== Kit Info of %kit ======";
    public static final String CorrectKitInfoCmd = "/KitInfo <KitName>";
    public static final String KitItems = "Items:";
    public static final String KitPotions = "Potions:";
    public static final String ChestSpawnedRan = "A random chest has been spawned at %loc.";
    public static final String ChestSpawned = "A chest has been spawned at %loc.";
    public static final String KitSwitchHappens = "All players have been given random kits.";
    public static final String KitswitchName = "a randomization of kits round";
    public static final String KitSwitchVote1 = "&aA random kit round vote has been launched!";
    public static final String KitSwitchVote2 = "&8For more info about Random Kit Round, type &7/RandomKit";
    public static final String UHCVote1 = "&aA hardcore round vote has been launched!";
    public static final String VoteInfo = "&8Cast your vote by typing &a/vote &7[&ayes&7|&cno&7]";
    public static final String UHCVote2 = "&8For more info about Hardcore HG, type &7/HCHG";
    public static final String VoteTime = "&8Voting will last for a period of %time.";
    public static final String KitSwitchDetails = "A randomization of kits round, is a round which resets everyone's chosen kit, with a random kit on the server. The selection may include VIP kits.";
    public static final String LatestEndedGame = "%winner has just won a Hunger-Games match.";
    public static final String GracePeriodEnds = "Grace period ends in %time.";
    public static final String FeastSpawn = "The feast has spawned at %loc";
    public static final String AlreadyAType = "You are already a %type. No need to rejoin.. :P";
    protected static final String FinalBattleIn = "Final battle will begin in %time.";
    public static final String FinalBattle = "Final battle has began.";
    protected static final String EnderpearlTP = "You are now protected for %time. You will later be teleported back...";
    protected static final String EnderpearlBack = "You have been returned back to your original spot.";
    public static final String GotHeadShotDam = "You just got a headshot on %def.";
    public static final String GotHeadShotDef = "You just got headshotted by %dam. The effects will be active in a few seconds...";
    public static final String HelmetSavedYourLife = "Your helmet has saved your life. You almost got headshot.";
    public static final String NeedToWaitForCooldown = "You must wait for cooldown to expire... Wait a little bit of time :)";
    public static final String YouAreNowInvisible = "You are now invisible for a short period of time.";
    public static final String Sponsorhelp1 = "&5How to sponsor?";
    public static final String Sponsorhelp2 = "&7To sponsor you will have to run the sponsor command, once the command is run, you will be propted with a chest menu, which you can use to specify who you want to sponsor and with what items.";
    public static final String SponsoredGamer = "You have sponsored %gamer. They thank you!";
    public static final String SponsoredBy = "You have been sponsored by %sponsor. Good luck!";
    public static final String FeastSpawnWill = "The feast will spawn in %time at co-ords of %loc";
    public static final String WaitCoolDown = "You must wait %time until cooldown ends.";
    public static final String YouAreCrouching = "You are only dealing 33% of your damage since you are crouching.";
    public static final String Backstabbed = "You have backstabbed %def like a boss.";
    public static final String BackstabbedBy = "You have been backstabbed by %dam.";
    public static final String TeleportWandTooFar = "The teleport location is too far.";
    public static final String TeleportWandSuccess = "You have teleported successfully.";
    public static final String DisguiseAs = "You are now disguised as a %disguise.";
    public static final String Undisguised = "You have been undisguised by %dam.";
    public static final String PlatePlanted = "The landmine has been planted.";
    public static final String PlateUnplanted = "The landmine has been disarmed.";
    public static final String LandmineExploded = "You landmine has exploded under %damaged.";
    public static final String LandminePlanted = "You have stepped on %planter's landmine.";
    public static final String YouHaveKnockedNearby = "You have knocked nearby players.";
    public static final String OpponentIsFriend = "You are attacking a teammate. Damage reduced by 30%.";
    public static final String FriendMayBeFoe = "Your teammate %dam has just attacked you.";
    public static final String TeamListClear = "Your team list has been cleared.";
    public static final String TeamListView = "&6Your team:";
    public static final String TeamListAdded = "Added %player to your team.";
    public static final String TeamListRemoved = "Removed %player from your team.";
    public static final String InvalidUser = "This user is not online or is invalid.";
    public static final String NotInTeam = "%player is not in your team.";
    public static final String AlreadyInTeam = "%player is already in your team.";
    public static final String NoOneInTeam = "No body is in your team.";
    public static final String DoNotAddYourself = "You cannot add your self to your own team. *facepalm*";
    public static final String RemindKit = "choose a kit with /Kit or using the kit selector.";
    public static final String KitSelectorName = ChatColor.GREEN + "Kit Selector";
    public static final String KitSelectorDesc = "&7Right-click while holding\nto choose a kit\nfor this round.";
    public static final String TributesRemain = "&c%remain &7tributes remain alive.";
    public static final String YouAreShootingSpecial = "Your next shot is an explosive arrow.";
    public static final String PlayerIsNotGaming = "The player who you want to spectate is not a tribute...";
    public static final String YouAreNotSpectator = "You are not a spectator, you can't spectate :P";
    public static final String StatsView = "Statistics of %player:";
    public static final String WebStats = "More detailed stats can be found on PvP.Kz";
    public static final String Kills = "Kills: &7%kills";
    public static final String Deaths = "Deaths: &7%deaths";
    public static final String Wins = "Wins: &7%wins";
    public static final String KillDeathRatio = "K/D: &7%kd";
    public static final String HowToViewStats = "/Stats &8<PlayerName> &7 | To view stats of player.";
    public static final String EarlyStartPlayers = "Timer has decreased to %time because %amount players are in-game.";
    public static final String LostAccessToDefaultKit = "&7You have &clost &7access to &8%kit&7 kit. Type &a/kit &7to set a new default kit.";
    public static final String YouDontHaveAccess = "&cYou don't have access to this kit.";
    public static final String DefaultKithasbeenSet = "&bYour default kit has been set to %kit.";
    public static final String EarnedFame = "You have just earned %fame fame!";
    public static final String PermDoubleScore = "pvpgames.score.2";
    public static final String Perm1X5Score = "pvpgames.score.1.5";
    public static final String PermSpecialRank = "pvpgames.ranks.special";
	public static final String VotedForMap = "&7You have &avoted&7 for&6&l %map&7.";

	public static final String VoteForMap = "&6Vote by clicking on any of the options below.";

    public MessageHG (MainHG mainclass){
        plugin = mainclass;
    }

    private static String ConvertMessagewithFields(Player p, String message) throws SQLException {
        if (!StatsHG.Assists.containsKey(p.getName())) {
            StatsHG.Assists.put(p.getName(), new ArrayList<String>());
        }
        message = Message.Replacer(message, "" + Stats.RoundKills.get(p.getName()), "%rkills");
        message = Message.Replacer(message, "" + Stats.TotalKills.get(p.getName()), "%tkills");
        message = Message.Replacer(message, "" + Stats.TotalDeaths.get(p.getName()), "%tdeaths");
        message = Message.Replacer(message, "" + StatsHG.Assists.get(p.getName()).size(), "%assists");
        message = Message.Replacer(message, "" + KitsHG.getPlayerKit(p.getName()).getKitName(), "%kit");

        return message;
    }


    public static void g(String message, Team team, boolean Prefix) {
        message = ChatColor.translateAlternateColorCodes('&', ChatColor.GRAY + message);

        if (team == Team.Player) {
            for (String pl : MainHG.Gamers) {
                Message.P(Bukkit.getPlayer(pl), message, Prefix);
            }
        } else if (team == Team.Spectator) {
            for (String pl : MainHG.Spectators){
                Message.P(Bukkit.getPlayer(pl), message, Prefix);
            }
        } if (team == Team.Gamemaker) {
            for (String pl : MainHG.GameMakers){
                Message.P(Bukkit.getPlayer(pl), message, Prefix);
            }
        }

        plugin.getLogger().info(ChatColor.stripColor(PREFIX) + ChatColor.stripColor(message) + " | sent to all players of " + team + " group.");
    }


    /////////////////////////// Chat Functions we will use /////////////////////////////////

    // We want to print the kit menu for the user
    public static void printKitMenu(Player p) throws SQLException {
        ArrayList<String> yourKits = new ArrayList<String>();
        ArrayList<String> specialKits = new ArrayList<String>();

        for (String kitName : KitsHG.kitList.keySet()) {
            if (KitsHG.canUserAccess(kitName, p)) {
                yourKits.add(Message.CleanCapitalize(kitName));
            } else {
                specialKits.add(Message.CleanCapitalize(kitName));
            }
        }

        String yourKitsList = yourKits.toString().replace('[', ' ').replace(']', ' ');
        String specialKitsList = specialKits.toString().replace('[', ' ').replace(']', ' ');

        Message.P(p, " ", false);
        Message.P(p, " ", false);
        Message.P(p, "&7Choose a Kit: &a/Kit [KitName]", false);
        Message.P(p, " ", false);

        Message.P(p, ChatColor.GRAY + MessageHG.YourKits + " (" + yourKits.size() + "): &a" + yourKitsList, false);

        if (specialKits.size() != 0) {
            Message.P(p, ChatColor.AQUA + MessageHG.SpecialKits + " (" + specialKits.size() + "): &c" + specialKitsList, false);
            if (MainHG.WebsiteForKits != null) {
                Message.P(p, MessageHG.GetMoreKits, false);
            }
        }
    }
}