package kz.pvp.PvPGames.Utilities;

import kz.PvP.PkzAPI.methods.itemModifications;
import kz.PvP.PkzAPI.utilities.Message;
import kz.PvP.PkzAPI.utilities.Mysql;
import kz.PvP.PkzAPI.utilities.Stats;
import kz.pvp.PvPGames.Enums.Gamestatus;
import kz.pvp.PvPGames.Enums.KitInfo;
import kz.pvp.PvPGames.Main.MainHG;
import kz.pvp.PvPGames.Methods.Teams;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

public class KitsHG {
	public static HashMap<String, String> KitChoice = new HashMap<String, String>();
	//public static HashMap<String, String> KitDesc = new HashMap<String, String>();
	public static HashMap<Integer, List<String>> AbilityDesc = new HashMap<Integer, List<String>>();
	//public static HashMap<String, ArrayList<Integer>> KitAbilities = new HashMap<String, ArrayList<Integer>>();
	//public static ArrayList<String> Kits = new ArrayList<String>();
	public static HashMap<String, KitInfo> kitList = new HashMap<String, KitInfo>();

	public KitsHG(MainHG mainclass) {
		FileConfiguration kitsConf = FilesHG.kitconf.getCustomConfig();
		Set<String> kitsConfList = kitsConf.getKeys(false);
		for (String kitName : kitsConfList) {
			kitName = kitName.toLowerCase();
			//Kits.add(kit.toLowerCase());            
			KitInfo kitInfo = new KitInfo();

			if (kitName != null)
				kitInfo.setKitName(kitName);

			if (kitsConf.contains(kitName + ".IconMenu"))
				kitInfo.setIconMenu(itemModifications.getItemInfo(kitsConf.getString(kitName + ".IconMenu"), null));
			if (kitsConf.contains(kitName + ".Price"))
				kitInfo.setPrice(kitsConf.getInt(kitName + ".Price"));

			if (kitsConf.contains(kitName + ".Items"))
				for (String kitItem : kitsConf.getStringList(kitName + ".Items"))
					kitInfo.addKitItem(itemModifications.getItemInfo(kitItem, null));

			if (kitsConf.contains(kitName + ".Potions"))
				for (String potionItem : kitsConf.getStringList(kitName + ".Potions"))
					kitInfo.addPotionEffect(getPotionInfo(potionItem, null));


			if (kitsConf.contains(kitName + ".Abilities"))
				for (String abilityID : kitsConf.getStringList(kitName + ".Abilities"))
					kitInfo.addAbility(Integer.parseInt(abilityID));



			kitList.put(kitName, kitInfo);
		}
	}

	public static void giveKit(Player p) throws SQLException {
		p.getInventory().clear();
		p.getInventory().setChestplate(null);
		p.getInventory().setLeggings(null);
		p.getInventory().setBoots(null);
		p.setExp(0);
		p.setLevel(0);
		p.eject();
		for (PotionEffect pe : p.getActivePotionEffects())
			p.removePotionEffect(pe.getType());


		for (PotionEffect effect : p.getActivePotionEffects()) {
			p.removePotionEffect(effect.getType());
		}

		if (Teams.isGamer(p)) {
			KitInfo kitInfo = getPlayerKit(p.getName());
			for (ItemStack i : kitInfo.getKitItems()){
				int id = i.getTypeId();
				if ((id < 298) || (317 < id)) {
					ItemStack is = new ItemStack(i.getType(), i.getAmount());
					i.setItemMeta(is.getItemMeta());
					p.getInventory().addItem(i);
				} else if ((id == 298) || (id == 302) || (id == 306) || (id == 310) || (id == 314)) {
					p.getInventory().setHelmet(i);
				} else if ((id == 299) || (id == 303) || (id == 307) || (id == 311) || (id == 315)) {
					p.getInventory().setChestplate(i);
				} else if ((id == 300) || (id == 304) || (id == 308) || (id == 312) || (id == 316)) {
					p.getInventory().setLeggings(i);
				} else if ((id == 301) || (id == 305) || (id == 309) || (id == 313) || (id == 317)) {
					p.getInventory().setBoots(i);
				}
			}
			for (PotionEffect potionEffect : kitInfo.getPotionEffects())
				p.addPotionEffect(potionEffect);
		}
	}



	public static KitInfo getPlayerKit(String name) {
		KitInfo kitInfo = new KitInfo();

		if (KitsHG.KitChoice.containsKey(name))// User chose a kit...
			kitInfo = KitsHG.kitList.get(KitsHG.KitChoice.get(name).toLowerCase());
		else{
			if (MainHG.DefaultKitEnable)
				kitInfo = KitsHG.kitList.get(MainHG.DefaultKit);
			else
				kitInfo.setKitName("None");
		}
		return kitInfo;
	}

	@SuppressWarnings("deprecation")
	public static PotionEffect getPotionInfo(String pot, Player p) {
		PotionEffect PE = null;
		if (pot != null & pot != "") {
			if (!pot.equals(0)) {
				String[] potion = pot.split(",");
				if (Integer.parseInt(potion[0]) != 0) {
					if (Integer.parseInt(potion[1]) == 0) {
						PE = new PotionEffect(PotionEffectType
								.getById(Integer.parseInt(potion[0])),
								120000, Integer
								.parseInt(potion[2]));
						if (p != null)
							p.addPotionEffect(PE);
					} else {
						PE = new PotionEffect(PotionEffectType
								.getById(Integer.parseInt(potion[0])), Integer
								.parseInt(potion[1]) * 20, Integer
								.parseInt(potion[2]));
						if (p != null)
							p.addPotionEffect(PE);
					}
				}
			}
		}
		return PE;
	}

	public static void giveBandAids(Player p, int bandaids) {
		ItemStack is = new ItemStack(Material.NETHER_STAR, bandaids);
		ItemMeta im = is.getItemMeta();
		im.setDisplayName(MessageHG.BandaidName);
		List<String> lore = new ArrayList<String>();
		lore.add(ChatColor.WHITE + "Use this item, to");
		lore.add(ChatColor.WHITE + "instantly heal yourself.");
		lore.add(ChatColor.WHITE + " ");
		lore.add(ChatColor.WHITE + " ");

		if (p.hasPermission("pvpgames.vip.instantheal")) {
			lore.add(ChatColor.WHITE + "- Tap shift to instantly heal");
		} else {
			lore.add(ChatColor.RED + "[VIP+] - Tap shift to instantly heal");
		}

		im.setLore(lore);
		is.setItemMeta(im);
		p.getInventory().addItem(is);
		lore.clear();
	}

	public static void choose(Player p, String kitName, boolean bypass) throws SQLException {
		if (Teams.isSpectator(p) || (MainHG.DisableKitChoosing && !bypass) || MainHG.Game != Gamestatus.Pregame) {
			Message.P(p, MessageHG.CantChooseKit, true);
		} else {
			if (kitList.containsKey(kitName.toLowerCase())) {
				if (canUserAccess(kitName.toLowerCase(), p)){
					// Kit is free...
					Message.P(p, Message.Replacer(MessageHG.ChoosenKit, kitName, "%kit"), true);
					KitChoice.put(p.getName(), kitName.toLowerCase());
				} else {
					if (kitList.get(kitName.toLowerCase()).getPrice() > 0){
						if (Mysql.modifyTokens(p.getName(), -KitsHG.kitList.get(kitName.toLowerCase()).getPrice(), true)){
							Message.P(p, Message.Replacer(MessageHG.ChoosenKit, kitName, "%kit"), true);
							KitChoice.put(p.getName(), kitName.toLowerCase());
						}
						else
							Message.P(p, Message.Replacer(Message.Replacer(Message.CannotAfford, ""+Stats.getTokens(p.getName()), "%bal"), ""+KitsHG.kitList.get(kitName.toLowerCase()).getPrice(), "%cost"), true);
					}
					else
						Message.P(p, MessageHG.KitNeedsPurchase, true);
				}
			} else 
				Message.P(p, MessageHG.InvalidKit, true);
			if (KitChoice.containsKey(p.getName()))
				if (KitChoice.get(p.getName()) != null)
					KitsHG.setDefaultKit(p, kitName.toLowerCase());
		}
	}

	public static boolean canUserAccess(String kitname, Player p) throws SQLException{
		if (!kitList.containsKey(kitname.toLowerCase()))
			return false;
		return StatsHG.lastRoundWin(p) || checkIfKitIsCurrentlyFree(kitname) || kitList.get(kitname.toLowerCase()).getPrice() == 0 || (p.hasPermission(MessageHG.PermVIPKit) ||  p.hasPermission(MessageHG.PermKitPre + "." + kitname.toLowerCase()));
	}

	// Just a new function recommended by the community... :P
	public static boolean checkIfKitIsCurrentlyFree(String kitName) throws SQLException {
		long curTime = System.currentTimeMillis()/1000;
		ResultSet kit = Mysql.PS.getSecureQuery("SELECT * FROM HungerGames_VIPAllocate WHERE ? >= Start AND ? < End AND KitName = ? LIMIT 1", curTime+"", curTime+"", kitName.toLowerCase());
		return kit != null && kit.next();
	}

	public static boolean hasAbility(Player p, Integer id) {
		return (Teams.isGamer(p) && MainHG.Game != Gamestatus.Pregame && KitsHG.getPlayerKit(p.getName()).getAbilities().contains(id) );
	}


	public static void addAbility(Integer id, String description){
		if (AbilityDesc.containsKey(id)) {
			AbilityDesc.get(id).add(description);
		} else {
			AbilityDesc.put(id, new ArrayList<String>());
			addAbility(id, description);// We do recursive method... I know there are other methods, but this is a more suitable set-up.
		}
	}



	public static void setDefaultKit(Player p, String kitName) throws SQLException {
		ResultSet rs = Mysql.PS.getSecureQuery("SELECT * FROM HungerGames_DefaultKits WHERE User = ?", ""+Mysql.getUserID(p.getName()));
		if (rs.next()) {
			Mysql.PS.getSecureQuery("UPDATE HungerGames_DefaultKits SET KitName = ? WHERE User = ?", kitName, ""+Mysql.getUserID(p.getName()));
		} else {
			Mysql.PS.getSecureQuery("INSERT INTO HungerGames_DefaultKits (User, KitName) VALUES (?,?)", ""+Mysql.getUserID(p.getName()), kitName);
		}
	}

	public static String getDefaultKit(Player p) throws SQLException {
		ResultSet rs = Mysql.PS.getSecureQuery("SELECT * FROM HungerGames_DefaultKits WHERE User = ?", ""+Mysql.getUserID(p.getName()));
		String defaultKit = null;
		if (rs.next()) 
			defaultKit = rs.getString("KitName");

		return defaultKit;
	}
}
