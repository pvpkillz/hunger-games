package kz.pvp.PvPGames.Utilities;

import kz.pvp.PvPGames.Enums.Config;
import kz.pvp.PvPGames.Main.MainHG;


public class FilesHG{
    public static MainHG plugin;

    public static Config bookconf;
    public static Config config;
    public static Config kitconf;
    public static Config cornconf;
    public static Config worldconf;
    public static Config abilitiesconf;
    public static Config sponsorconf;
    
    
	public FilesHG (MainHG mainclass){
		plugin = mainclass;
		config = new Config("config", plugin);
		worldconf = new Config("world", plugin);
		kitconf = new Config("kit", plugin);
		cornconf = new Config("cornucopia", plugin);
		bookconf = new Config("book", plugin);
		abilitiesconf = new Config("abilities", plugin);
		sponsorconf = new Config("sponsor", plugin);
	}
}