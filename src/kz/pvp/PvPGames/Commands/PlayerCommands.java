package kz.pvp.PvPGames.Commands;

import kz.PvP.PkzAPI.Main;
import kz.PvP.PkzAPI.methods.ConvertTimings;
import kz.PvP.PkzAPI.methods.PlayersInfo;
import kz.PvP.PkzAPI.methods.ProtocolLib;
import kz.PvP.PkzAPI.methods.itemModifications;
import kz.PvP.PkzAPI.utilities.FancyMessage;
import kz.PvP.PkzAPI.utilities.IconMenu;
import kz.PvP.PkzAPI.utilities.Message;
import kz.PvP.PkzAPI.utilities.Mysql;
import kz.PvP.PkzAPI.utilities.Stats;
import kz.PvP.PkzAPI.utilities.statHooks.HungerGames;
import kz.pvp.PvPGames.Enums.Gamestatus;
import kz.pvp.PvPGames.Enums.KitInfo;
import kz.pvp.PvPGames.Enums.ModType;
import kz.pvp.PvPGames.Enums.mapInfo;
import kz.pvp.PvPGames.Main.MainHG;
import kz.pvp.PvPGames.Methods.Hardcore;
import kz.pvp.PvPGames.Methods.Kits;
import kz.pvp.PvPGames.Methods.Sponsor;
import kz.pvp.PvPGames.Methods.Teams;
import kz.pvp.PvPGames.Utilities.FilesHG;
import kz.pvp.PvPGames.Utilities.KitsHG;
import kz.pvp.PvPGames.Utilities.MessageHG;
import kz.pvp.PvPGames.Utilities.StatsHG;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.potion.PotionEffect;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.Map.Entry;

public class PlayerCommands implements CommandExecutor {
	public static MainHG plugin;

	public static HashMap<String, IconMenu> menus = new HashMap<String, IconMenu>();
	public static HashMap<String, Integer> Count = new HashMap<String, Integer>();

	int count;
	int task;

	public PlayerCommands(MainHG main) {
		plugin = main;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, final String[] args) {
		if (!(sender instanceof Player)) {
			sender.sendMessage(MessageHG.NotPlayer);
			return true;
		}

		final Player p = (Player)sender;

		if (cmd.getName().equalsIgnoreCase("kit")) {
			try {
				if (args.length == 0) {
					if (MainHG.KitMenu) {
						OpenKitMenu(p);
					} else {
						MessageHG.printKitMenu(p);
					}

					Message.P(p, "&7Set a default kit &a/set &7<KitName>", false);
				} else {
					KitsHG.choose(p, args[0], false);
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		} 
		else if (cmd.getName().equalsIgnoreCase("map")){
			if (MainHG.Game == Gamestatus.Pregame){
				boolean voted = false;

				if (args.length >= 1){
					String choiceStr = args[0];
					if (ConvertTimings.isInteger(choiceStr)){
						Integer choice = Integer.parseInt(choiceStr);
						if (MainHG.mapVotes.size() >= choice && choice > 0){
							voted = true;
							for (Entry<mapInfo, ArrayList<String>> entries : MainHG.mapVotes.entrySet())
								if (entries.getValue().contains(p.getName())){
									MainHG.mapVotes.get(entries.getKey()).remove(p.getName());
									MainHG.voters.remove(p.getUniqueId());
									break;
								}
							for (Entry<mapInfo, ArrayList<String>> entries : MainHG.mapVotes.entrySet())
								if (entries.getKey().getIndex() == choice){
									MainHG.mapVotes.get(entries.getKey()).add(p.getName());
									MainHG.voters.add(p.getUniqueId());
									Message.P(p, Message.Replacer(MessageHG.VotedForMap, entries.getKey().getMapName(), "%map"), true);
									break;
								}
						}
					}	
				}
				if (!voted){
					Message.P(p,Message.Replacer( Message.HeaderMenu, "--------", "&a PkzAPI &6&m"), false);

					Message.P(p, MessageHG.VoteForMap, false);
					for (mapInfo maps : MainHG.maps){
						new FancyMessage(
								"&6&l  " + maps.getIndex() + " &7 - " + maps.getMapName() + "&7  - &a" + MainHG.mapVotes.get(maps).size() + " &7votes")
						.tooltip("&7 Click to vote for " + maps.getMapName())
						.command("/map "+maps.getIndex()).send(p);
					}
				}
			}
			else
				Message.P(p, MessageHG.CannotDoAtThisTime, true);
			return true;
		}
		else if (cmd.getName().equalsIgnoreCase("team")) {
			Message.P(p, " ", false);

			if (args.length == 0) {
				Message.P(p, "&7======== &aTeams &7========", false);
				Message.P(p, "&a/team add <player> &7Add player to team", false);
				Message.P(p, "&a/team remove <player> &7Remove player from team", false);
				Message.P(p, "&a/team list &7List team", false);
				Message.P(p, "&a/team clear &7Clear team list", false);
			} else if (args.length >= 1) {
				if (args[0].equalsIgnoreCase("clear")) {
					// Clear team
					Message.P(p, MessageHG.TeamListClear, true);
					StatsHG.getTeam(p).clear();
				} else if (args[0].equalsIgnoreCase("list")) {
					if (StatsHG.getTeam(p).size() >= 1) {
						Message.P(p, MessageHG.TeamListView, false);
						for (String teamPlayer : StatsHG.getTeam(p)) {
							Message.P(p, "  ► " + teamPlayer, false);
						}
					} else {
						Message.P(p, MessageHG.NoOneInTeam, true);
					}
				} else if(args.length >= 2) {
					if (args[0].equalsIgnoreCase("add")) {
						if (args[1] != null && Teams.getPlayerFromSome(args[1]) != null) {
							Player playerTeam = Teams.getPlayerFromSome(args[1]);
							if (!StatsHG.isInTeam(p, playerTeam.getName())) {
								if (p != playerTeam) {
									Message.P(p, Message.Replacer(MessageHG.TeamListAdded, playerTeam.getName(), "%player"), true);
									StatsHG.modifyTeam(p, playerTeam.getName(), ModType.Add);
								} else {
									Message.P(p, MessageHG.DoNotAddYourself, true);
								}
							} else {
								Message.P(p, Message.Replacer(MessageHG.AlreadyInTeam, playerTeam.getName(), "%player"), true);
							}
						} else {
							Message.P(p, MessageHG.InvalidUser, true);
						}
					} else if (args[0].equalsIgnoreCase("remove")) {
						if (args[1] != null && Teams.getPlayerFromSome(args[1]) != null) {
							Player playerTeam = Teams.getPlayerFromSome(args[1]);
							if (StatsHG.isInTeam(p, playerTeam.getName())) {
								Message.P(p, Message.Replacer(MessageHG.TeamListRemoved, playerTeam.getName(), "%player"), true);
								StatsHG.modifyTeam(p, playerTeam.getName(), ModType.Remove);
							} else {
								Message.P(p, Message.Replacer(MessageHG.NotInTeam, playerTeam.getName(), "%player"), true);
							}
						} else {
							Message.P(p, MessageHG.InvalidUser, true);
						}
					}
				}
			}
		}/* else if (cmd.getName().equalsIgnoreCase("sponsorhelp")) {
			Message.P(p, " ", false);
			Message.P(p, MessageHG.Sponsorhelp1, false);
			Message.P(p, MessageHG.Sponsorhelp2, false);
		}*/else if (cmd.getName().equalsIgnoreCase("sponsor")) {
			if (MainHG.Game == Gamestatus.Game){
				try {
					Sponsor.OpenSponsorMenuPlayers(p);
				} catch (SQLException e) {
					e.printStackTrace();
				}
			} else {
				Message.P(p, MessageHG.UnableAtThisTime, true);
			}
		} /*else if (cmd.getName().equalsIgnoreCase("set")) {
			if (args.length >= 1) {
				String KitName = args[0].toLowerCase();
				try {
					if (KitsHG.kitList.containsKey(KitName.toLowerCase())) {
						if (KitsHG.canUserAccess(KitName, p)) {
							KitsHG.setDefaultKit(p, KitName);
							Message.P(p, Message.Replacer(MessageHG.DefaultKithasbeenSet, KitName, "%kit"), true);
						} else {
							Message.P(p, MessageHG.YouDontHaveAccess, true);
						}
					} else {
						Message.P(p, MessageHG.InvalidKit, true);
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
			} else {
				Message.P(p, MessageHG.SetDefaultKit, true);
			}
		}else if (cmd.getName().equalsIgnoreCase("hchg")) {
			Message.P(p, MessageHG.HardCore, true);
			Message.P(p, MessageHG.HardcoreVoteDetails, false);
		} else if (cmd.getName().equalsIgnoreCase("randomizekit")) {
			Message.P(p, MessageHG.KitswitchName, true);
			Message.P(p, MessageHG.KitSwitchDetails, false);
		} else if (cmd.getName().equalsIgnoreCase("kitinfo")) {
			if (args.length >= 1) {
				String kitname = args[0].toLowerCase();
				if (KitsHG.kitList.containsKey(kitname.toLowerCase())) {
					Message.P(p, Message.Replacer(MessageHG.KitInfo, Message.CleanCapitalize(kitname), "%kit"), false);
					Message.P(p, MessageHG.KitItems, false);
					for (String itemone : FilesHG.kitconf.getCustomConfig().getStringList(kitname + ".Items")) {
						ItemStack is = itemModifications.getItemInfo(itemone, null);
						String Item = Message.CleanCapitalize(is.getType().toString());
						Message.P(p, "&c● &7" + is.getAmount() + "&8x &7" + Item, false);
					}
					Message.P(p, MessageHG.KitPotions, false);
					for (String itemone : FilesHG.kitconf.getCustomConfig().getStringList(kitname + ".Potions")) {
						PotionEffect pe = KitsHG.getPotionInfo(itemone, null);
						String Potion = Message.CleanCapitalize(pe.getType().getName());
						String Time = ConvertTimings.convertTime(pe.getDuration()/20);
						if (Time.contains("hour")) {
							Time = "| &7Permenant";
						} else {
							Time = "for &7" + Time;
						}

						Message.P(p, "&c● &7" + Potion + " " + Time, false);
					}
				} else {
					Message.P(p, MessageHG.InvalidKit, true);
				}
			} else {
				Message.P(p, MessageHG.CorrectKitInfoCmd, true);
			}
		}else if (cmd.getName().equalsIgnoreCase("vote")) {
			if (MainHG.VoteActive) {
				if (args.length == 0){
					Message.P(p, MessageHG.VoteDetails, true);
					Message.P(p, MainHG.VoteDesc, true);
				} else if (args.length == 1) {
					if (args[0].equalsIgnoreCase("yes")) {
						Message.P(p, MessageHG.VotedYes, true);
						Hardcore.Vote.put(p.getName(), 1);
						Kits.Vote.put(p.getName(), 1);
					} else if (args[0].equalsIgnoreCase("no")) {
						Message.P(p, MessageHG.VotedNo, true);
						Kits.Vote.put(p.getName(), 0);
					}
				}
			} else {
				Message.P(p, MessageHG.NoVoteAtThisTime, false);
			}
		} else if (cmd.getName().equalsIgnoreCase("submitmap")) {
			Message.P(p, " ", false);
			Message.P(p, MessageHG.SubmitAMap, true);
			Message.P(p, MessageHG.HowToSubmitAMap, false);
			Message.P(p, " ", false);
		} */else 
			return false;
		return true;
	}

	public static void OpenKitMenu(final Player p) throws SQLException {
		Collection<KitInfo> kitList = KitsHG.kitList.values();
		if (menus.containsKey(p.getName())){
			menus.get(p.getName()).destroy();
			menus.remove(p.getName());
		}

		int invsize;
		if (kitList.size() % 9 == 0) 
			invsize = kitList.size();
		else 
			invsize = kitList.size() + (9 - (kitList.size() % 9));

		int pos = 0;
		int othpos = 0;

		IconMenu menu = new IconMenu(ChatColor.GOLD + "Kit selection menu", p.getName(), invsize, new IconMenu.OptionClickEventHandler() {
			public void onOptionClick(IconMenu.OptionClickEvent event) throws SQLException {
				KitsHG.choose(p, ChatColor.stripColor(event.getName()), false);
				event.setWillClose(true);
				event.setWillDestroy(false);
				//if (menus.containsKey(p.getName()))
				//   menus.get(p.getName()).destroy();
			}
		}, plugin);
		for (KitInfo kitInfo : kitList) {

			String cleankit = Message.CleanCapitalize(kitInfo.getKitName());

			ArrayList<String> container = new ArrayList<String>();


			for (ItemStack is : kitInfo.getKitItems()) {
				String item = Message.CleanCapitalize(is.getType().toString());
				if (is.hasItemMeta() && is.getItemMeta().hasDisplayName()) {
					item = Message.CleanCapitalize(is.getItemMeta().getDisplayName());
				}
				container.add("&c ● &7" + is.getAmount() + "&8x &7" + item);
			}


			ItemStack iconMenu = kitInfo.getIconMenu();


			if (kitInfo.getPotionEffects().size() >= 1){
				container.add(" ");
				container.add(ChatColor.GOLD + "" + ChatColor.ITALIC + "&a● &7&m------&a Potion Effects &7&m------&a●");
				for(PotionEffect pe : kitInfo.getPotionEffects()) {
					if (pe != null){
						String Potion = Message.CleanCapitalize(pe.getType().getName());
						String time = ConvertTimings.convertTime(pe.getDuration()/20);
						if (time.contains("hour")) {
							time = "| &7Permenant";
						} else {
							time = "for &7" + time;
						}

						container.add("&c ● &7" + Potion + " " + time);
					}
				}
			}

			if (kitInfo.getAbilities().size() >= 1) {
				container.add("&a● &7&m------&a Abilities &7&m------&a●");
				for (Integer abilityId : kitInfo.getAbilities()) {
					if (KitsHG.AbilityDesc.containsKey(abilityId)) {
						for (String abilityDesc : KitsHG.AbilityDesc.get(abilityId)) {
							ArrayList<String> lines = itemModifications.multilineFromString(abilityDesc);
							int x = 0;
							for (String line : lines){
								if (x == 0)
									line = "&7 ● " + line;
								container.add("&7" + line);
								x++;
							}
						}
					}
				}
			}
			container.add(" ");


			int FinalPos;
			if (!KitsHG.canUserAccess(kitInfo.getKitName(), p)){
				if (kitInfo.getPrice() > 0){
					container.add("Cost: " + kitInfo.getPrice());
				}
				else
					container.add("&cNo Access");


				cleankit = ChatColor.RED + Message.CleanCapitalize(kitInfo.getKitName());
				FinalPos = (invsize - 1) - othpos;
				othpos++;
			}
			else{
				FinalPos = pos;
				pos++;

				cleankit = ChatColor.GREEN + Message.CleanCapitalize(kitInfo.getKitName());
			}

			ArrayList<String> coloredcontainer = new ArrayList<String>();
			for (String continfo : container) {
				if (continfo != null) {
					coloredcontainer.add(Message.Colorize(continfo));
				}
			}

			String[] info = new String[coloredcontainer.size()];
			info = coloredcontainer.toArray(info);

			menu.setOption(FinalPos, iconMenu, cleankit, info);

			container.clear();
			coloredcontainer.clear();
		}
		menu.open(p);
		menus.put(p.getName(), menu);
	}

	public static void OpenPlayersMenu(final Player p) throws SQLException {
		int maxpos = 0;
		if (MainHG.Gamers.size() % 9 == 0)
			maxpos = MainHG.Gamers.size();
		else 
			maxpos = MainHG.Gamers.size() + (9 - (MainHG.Gamers.size() % 9));

		if (menus.containsKey(p.getName())){
			menus.get(p.getName()).destroy();
			menus.remove(p.getName());
		}
		IconMenu menu = new IconMenu(ChatColor.GOLD + "Current Players Alive", p.getName(), maxpos, new IconMenu.OptionClickEventHandler() {
			public void onOptionClick(IconMenu.OptionClickEvent event) {
				p.teleport(PlayersInfo.getPlayer((ChatColor.stripColor(event.getName()))));
				Message.P(p, "You are now spectating " + ChatColor.stripColor(event.getName()), true);
				event.setWillClose(true);
				event.setWillDestroy(false);
			}
		}, plugin);
		menu = getPlayerContainer(menu);
		menu.open(p);
		menus.put(p.getName(), menu);
	}

	public static IconMenu getPlayerContainer(IconMenu menu) throws SQLException {
		int pos = 0;
		int maxpos = 0;
		if (MainHG.Gamers.size() % 9 == 0)
			maxpos = MainHG.Gamers.size();
		else 
			maxpos = MainHG.Gamers.size() + (9 - (MainHG.Gamers.size() % 9));



		for (String player : MainHG.Gamers) {
			if (pos == maxpos) {
				break;
			}

			ArrayList<String> container = new ArrayList<String>();			
			container.add("&cKills: &a" + Stats.getKills(player));
			container.add("&aAssists: &a" + Stats.getAssists(Bukkit.getPlayer(player)).size());
			if (StatsHG.getTeam(Bukkit.getPlayer(player)).size() >= 1) {
				container.add("&5District: ");
				for (String teamPlayer : StatsHG.getTeam(Bukkit.getPlayer(player))) {
					container.add("&5● &7" + teamPlayer);
				}
			}
			container.add("&7ID: " + (int)Mysql.getUserID(player));

			ArrayList<String> coloredcontainer = new ArrayList<String>();

			for (String continfo : container) {
				if (continfo != null) {
					coloredcontainer.add(Message.Colorize(continfo));
				}
			}

			String[] info = new String[coloredcontainer.size()];
			info = coloredcontainer.toArray(info);

			menu.setOption(pos, KitsHG.getPlayerKit(player).getIconMenu(), ChatColor.RED + player, info);
			pos++;
			container.clear();
			coloredcontainer.clear();
		}
		return menu;
	}
}
