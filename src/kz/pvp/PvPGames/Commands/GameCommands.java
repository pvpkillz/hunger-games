package kz.pvp.PvPGames.Commands;

import kz.PvP.PkzAPI.methods.ConvertTimings;
import kz.PvP.PkzAPI.utilities.IconMenu;
import kz.PvP.PkzAPI.utilities.Message;
import kz.pvp.PvPGames.Enums.Gamestatus;
import kz.pvp.PvPGames.Main.GameFunctions;
import kz.pvp.PvPGames.Main.MainHG;
import kz.pvp.PvPGames.Methods.*;
import kz.pvp.PvPGames.Utilities.ChestSpawn;
import kz.pvp.PvPGames.Utilities.KitsHG;
import kz.pvp.PvPGames.Utilities.MessageHG;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.HashMap;

public class GameCommands implements CommandExecutor {
	public static MainHG plugin;
	public static HashMap<String, IconMenu> menus = new HashMap<String, IconMenu>();
	public static HashMap<String, Integer> count = new HashMap<String, Integer>();
	Material from = null;
	Material to = null;
	Material fromSta = null;
	Material toSta = null;

	public GameCommands(MainHG main) {
		plugin = main;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, final String[] args) {
		if (!(sender instanceof Player)) {
			sender.sendMessage(MessageHG.NotPlayer);
			return true;
		}

		final Player p = (Player)sender;

		if (cmd.getName().equalsIgnoreCase("cp")) {
			// The PvPGames control panel | Cotrolling all the aspects of the game
			if (p.hasPermission(MessageHG.PermAdminPanel)) {
				if (args.length == 0) {
					Message.P(p, "&c= &a= &cPvPKillz &5HG &acontrol panel &c= &a=", false);
					Message.P(p, "&a1 - &7Generate Random Chest", false);
					Message.P(p, "&a2 - &7Generate Chest at eyesight location", false);
					Message.P(p, "&a3 - &7Spawn in the feast", false);
					Message.P(p, "&a4 - &7Modify Start Game Timer", false);
					Message.P(p, "&a5 - &7Enable locators on last x players", false);
					Message.P(p, "&a6 - &7Generate final battle room", false);
					Message.P(p, "&a7 - &7Spawn mobs around players", false);
					Message.P(p, "&7More options &b/" + label + " more", false);
				} else if (args.length == 1 && args[0].equalsIgnoreCase("more")) {
					Message.P(p, "&c= &a= &cPvPKillz &5HG &acontrol panel &c= &a=", false);
					Message.P(p, "&a8 - &7Spawn x really strong wolves", false);
					Message.P(p, "&a9 - &7Randomize all kits", false);
					Message.P(p, "&a10 - &7Randomize locations of players on start", false);
					Message.P(p, "&a11 - &7Give everyone jump boost", false);
					Message.P(p, "&a12 - &7Give everyone bandaids", false);
				} else if (args.length == 1) {
					if (args[0].equalsIgnoreCase("1")) { // Generate random chest
						Message.P(p, "=== /cp " + args[0] + " help ===", false);
						Message.P(p, "This command will generate a random chest", false);
						Message.P(p, "in a random location in the map, after the time specified.", false);
						Message.P(p, "Usage: /cp " + args[0] + " <TimeInSeconds>", false);
					} else if (args[0].equalsIgnoreCase("2")) { // Generate chest at eyesight location
						Message.P(p, "=== /cp " + args[0] + " help ===", false);
						Message.P(p, "This command will generate a random chest", false);
						Message.P(p, "at your eyesight location, after the time specified.", false);
						Message.P(p, "Usage: /cp " + args[0] + " <TimeInSeconds>", false);
					} else if (args[0].equalsIgnoreCase("3")) { // Spawn feast
						Message.P(p, "=== /cp " + args[0] + " help ===", false);
						Message.P(p, "This command will generate a feast", false);
						Message.P(p, "which will help tributes get materials", false);
						Message.P(p, "Usage: /cp " + args[0] + " spawnfeast", false);
					} else if (args[0].equalsIgnoreCase("4")) { // Modify game start time
						Message.P(p, "=== /cp " + args[0] + " help ===", false);
						Message.P(p, "This command will modify the pregame", false);
						Message.P(p, "time to a new time. Useful for quick-start games.", false);
						Message.P(p, "Usage: /cp " + args[0] + " <NewTimeInSeconds>", false);
					} else if (args[0].equalsIgnoreCase("5")) { // Enable Locations on last x players
						Message.P(p, "=== /cp " + args[0] + " help ===", false);
						Message.P(p, "This command will toggle location", false);
						Message.P(p, "assistance with the following options:", false);
						Message.P(p, "1 - Lightning    | 2 - Fireworks", false);
						Message.P(p, "Only works with 4 or less tributes alive.", false);
						Message.P(p, "Usage: /cp " + args[0] + " <Option>", false);
					} else if (args[0].equalsIgnoreCase("6")) { // Generate final battle room
						Message.P(p, "=== /cp " + args[0] + " help ===", false);
						Message.P(p, "This command will generate", false);
						Message.P(p, "the final battle room, for", false);
						Message.P(p, "quick ending games.", false);
						Message.P(p, "Only works with 8 or less tributes alive.", false);
						Message.P(p, "Usage: /cp " + args[0] + " teleport", false);
					} else if (args[0].equalsIgnoreCase("7")) { // Spawn mobs around players
						Message.P(p, "=== /cp " + args[0] + " help ===", false);
						Message.P(p, "This command will spawn mobs", false);
						Message.P(p, "close to alive tributes, for more", false);
						Message.P(p, "difficult games.", false);
						Message.P(p, "Usage: /cp " + args[0] + " spawnmobs", false);
					} else if (args[0].equalsIgnoreCase("8")) { // Spawn x amount of strong wolves
						Message.P(p, "=== /cp " + args[0] + " help ===", false);
						Message.P(p, "This command will launch", false);
						Message.P(p, "X custom and strong wolves into", false);
						Message.P(p, "the wild, to hunt for tributes.", false);
						Message.P(p, "Only works with 7 or less tributes alive.", false);
						Message.P(p, "Usage: /cp " + args[0] + " <howManyWolves>", false);
					} else if (args[0].equalsIgnoreCase("9")) { // Randomize all kits on game start
						Message.P(p, "=== /cp " + args[0] + " help ===", false);
						Message.P(p, "This command will cause the current", false);
						Message.P(p, "round to have everyone use a randomly", false);
						Message.P(p, "chosen kit. Vote must pass.", false);
						Message.P(p, "Usage: /cp " + args[0] + " randomizekit", false);
					} else if (args[0].equalsIgnoreCase("10")) { // Randomize locations of players on start
						Message.P(p, "=== /cp " + args[0] + " help ===", false);
						Message.P(p, "This command will teleport", false);
						Message.P(p, "all players in random locations", false);
						Message.P(p, "on round start.", false);
						Message.P(p, "Usage: /cp " + args[0] + " randomizetp", false);
					} else if (args[0].equalsIgnoreCase("11")) { // Give everyone jump boost
						Message.P(p, "=== /cp " + args[0] + " help ===", false);
						Message.P(p, "This command will give all tributes", false);
						Message.P(p, "a jump boost for the remaining time of the round.", false);
						Message.P(p, "Usage: /cp " + args[0] + " applyjump <TimeInSeconds>", false);
					} else if (args[0].equalsIgnoreCase("12")) { // Give everyone bandaids
						Message.P(p, "=== /cp " + args[0] + " help ===", false);
						Message.P(p, "This command will give every tribute", false);
						Message.P(p, "a random amount of bandaids", false);
						Message.P(p, "to satisfy their needs.", false);
						Message.P(p, "Usage: /cp " + args[0] + " <amount>", false);
					}
				} else if (args.length == 2 || args.length == 3) {
					if (args[0].equalsIgnoreCase("1") && Integer.parseInt(args[1]) >= 0) {// Generate random chest
						final Location loc = p.getTargetBlock(null, 100).getLocation().add(0, 1, 0);
						int Delay = Integer.parseInt(args[1]);
						Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
							@Override
							public void run() {
								Message.G(Message.Replacer(MessageHG.ChestSpawned, loc.getBlockX() + ", " + loc.getBlockY() + ", " + loc.getBlockZ(), "%loc"), true);
								ChestSpawn.spawnChest(loc);
							}
						}, 20L * Delay);
					} else if (args[0].equalsIgnoreCase("2") && Integer.parseInt(args[1]) >= 0) { // Generate chest at eyesight location
						final Location loc = Maps.getRandomLocation();
						int Delay = Integer.parseInt(args[1]);
						plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
							@Override
							public void run() {
								Message.G(Message.Replacer(MessageHG.ChestSpawnedRan, loc.getBlockX() + ", " + loc.getBlockY() + ", " + loc.getBlockZ(), "%loc"), true);
								ChestSpawn.spawnChest(loc);
							}
						}, 20L * Delay);
					} else if (args[0].equalsIgnoreCase("3") && args[1].equalsIgnoreCase("spawnfeast")) { // Spawn feast
						Message.P(p, MessageHG.UnableAtThisTime, true);
					} else if (args[0].equalsIgnoreCase("4") && Integer.parseInt(args[1]) >= 0) { // Modify game start time
						int NewTime = Integer.parseInt(args[1]);
						if (MainHG.Game == Gamestatus.Pregame || MainHG.Game == Gamestatus.GracePeriod) {
							MainHG.TimeLeft = NewTime + 1;
						} else {
							Message.P(p, MessageHG.UnableAtThisTime, true);
						}
					} else if (args[0].equalsIgnoreCase("5") && Integer.parseInt(args[1]) == 1  || Integer.parseInt(args[1]) == 2){ // Enable Locations on last x players
						int Option = Integer.parseInt(args[1]);
						if (MainHG.Gamers.size() <= 4) {
							if (MainHG.LocatorsActive) {
								// Cancel all tasks for locators
								if (Locators.FireworksTimer != 0) {
									Bukkit.getScheduler().cancelTask(Locators.FireworksTimer);
									Locators.LightningTimer = 0;
									Message.G(MessageHG.FireworksLocatorInactive, true);
								} else if (Locators.LightningTimer != 0) {
									Bukkit.getScheduler().cancelTask(Locators.LightningTimer);
									Locators.LightningTimer = 0;
									Message.G(MessageHG.LightningLocatorInactive, true);
								}
								MainHG.LocatorsActive = false;
							} else {
								MainHG.LocatorsActive = true;
								// Begin tasks for locators
								if (Option == 1){
									// Lightning
									Locators.beginLightningLocator();
								}
								else if (Option == 2){
									// Fireworks
									Locators.beginFireworksLocator();
								}
							}
						} else {
							Message.P(p, MessageHG.UnableAtThisTime, true);
						}
					} else if (args[0].equalsIgnoreCase("6")) { // Generate final battle room
						if (!MainHG.FinalBattleSpawned)
							GameFunctions.generateFinalBattle();
						else
							Message.P(p, "Already spawned.", true);

					} else if (args[0].equalsIgnoreCase("7")) { // Spawn mobs around players
						if (Teams.getPlayerFromSome(args[1]) != null){
							Message.P(p, Message.Replacer(MessageHG.MobsSpawningPer, Teams.getPlayerFromSome(args[1]).getName(), "%player"), false);
							MobsGM.ToggleMobsAroundPlayers(Teams.getPlayerFromSome(args[1]));
						} else if (args[1].equalsIgnoreCase("all")) {
							MobsGM.ToggleMobsAroundPlayers(null);
						}
					} else if (args[0].equalsIgnoreCase("8")) {// Spawn x amount of strong wolves
						Message.P(p, "=== /cp " + args[0] + " help ===", false);
						Message.P(p, "This command will launch", false);
						Message.P(p, "X custom and strong wolves into", false);
						Message.P(p, "the wild, to hunt for tributes.", false);
						Message.P(p, "Only works with 7 or less tributes alive.", false);
						Message.P(p, "Usage: /cp " + args[0] + " <howManyWolves>", false);
					} else if (args[0].equalsIgnoreCase("9")) { // Randomize locations of players on start
						Message.P(p, "=== /cp " + args[0] + " help ===", false);
						Message.P(p, "This command will teleport", false);
						Message.P(p, "all players in random locations", false);
						Message.P(p, "on round start.", false);
						Message.P(p, "Usage: /cp " + args[0] + " randomizetp", false);
					} else if (args[0].equalsIgnoreCase("10")) {
						Message.P(p, MessageHG.ThisFunctionIsDisabled, true);
					} else if (args[0].equalsIgnoreCase("11") && args[1].equalsIgnoreCase("applyjump")) { // Give everyone jump boost
						if (args.length == 3) {
							int time = Integer.parseInt(args[2]);
							if (time >= 30){
								Message.G(Message.Replacer(MessageHG.GivenJumpboost, ConvertTimings.convertTime(time), "%time"), true);
								for (String pl : MainHG.Gamers){
									Bukkit.getPlayer(pl).addPotionEffect(new PotionEffect(PotionEffectType.JUMP, 20 * time, 0));
								}
							} else {
								Message.P(p, MessageHG.IncreaseTime, true);
							}
						} else {
							Message.P(p, MessageHG.IncreaseTime, true);
						}
					} else if (args[0].equalsIgnoreCase("12")) { // Give everyone bandaids
						int amount = Integer.parseInt(args[1]);
						if (amount >= 1) {
							Message.G(Message.Replacer(MessageHG.GivenBandaids, "" + amount, "%bandaids"), true);
							for (String pl : MainHG.Gamers){
								KitsHG.giveBandAids(Bukkit.getPlayer(pl), amount);
							}
						} else {
							Message.P(p, MessageHG.DontScrewWithMe, true);
						}
					}
				}
			} else {
				Message.P(p, MessageHG.NoPermission, true);
			}
		} else if (cmd.getName().equalsIgnoreCase("gamemaker")) {
			if (p.hasPermission(MessageHG.PermGamemaker)) {
				if (TimedCommand.TimedCommands.get(p.getName()).contains("gamemaker")) {
					// Allow the player to convert to gamemaker
					Teams.addGamemakers(p);
				} else {
					Message.P(p, MessageHG.CommandNotUsable, true);
				}
			} else {
				Message.P(p, MessageHG.NoPermission, true);
			}
		} else {
			return false;
		}

		return true;
	}
}
