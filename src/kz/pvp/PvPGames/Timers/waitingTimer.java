package kz.pvp.PvPGames.Timers;

import java.sql.SQLException;

import kz.PvP.PkzAPI.Main;
import kz.PvP.PkzAPI.methods.ConvertTimings;
import kz.PvP.PkzAPI.utilities.FancyMessage;
import kz.PvP.PkzAPI.utilities.Message;
import kz.PvP.PkzAPI.utilities.Bar.BarAPI;
import kz.pvp.PvPGames.Enums.mapInfo;
import kz.pvp.PvPGames.Main.GameFunctions;
import kz.pvp.PvPGames.Main.MainHG;
import kz.pvp.PvPGames.Methods.Teams;
import kz.pvp.PvPGames.Utilities.KitsHG;
import kz.pvp.PvPGames.Utilities.MessageHG;
import net.minecraft.server.v1_7_R3.ChatSerializer;
import net.minecraft.server.v1_7_R3.IChatBaseComponent;
import net.minecraft.server.v1_7_R3.PacketPlayOutChat;

import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.craftbukkit.v1_7_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;




public class waitingTimer extends BukkitRunnable {

	public static BukkitTask task = null;


	public waitingTimer() {
		MainHG.TimeLeft = MainHG.PregameTime;
		BarAPI.setMessage(Message.Replacer(MessageHG.MapVoteEnds, ConvertTimings.getTime(MainHG.TimeLeft), "%time"), (float) ((float)((float)MainHG.TimeLeft / MainHG.PregameTime) * 100));
	}

	@Override
	public void run() {
		MainHG.TimeLeft--;


		for (Player p : Bukkit.getServer().getOnlinePlayers()) {
			p.setHealth(20.0);
			p.setFoodLevel(20);
			p.setAllowFlight(true);
		}


		if (MainHG.TimeLeft > 0) {
				BarAPI.setMessage(Message.Replacer(MessageHG.MapVoteEnds, ConvertTimings.getTime(MainHG.TimeLeft), "%time"), (float) ((float)((float)MainHG.TimeLeft / MainHG.PregameTime) * 100));
			if (((MainHG.TimeLeft % 30 == 0 && MainHG.TimeLeft >= 60) || (MainHG.TimeLeft % 10 == 0 && MainHG.TimeLeft <= 50) || (MainHG.TimeLeft <= 3)) && MainHG.TimeLeft != 0) {
				Message.G(Message.Replacer(MessageHG.MapVoteEnds, ConvertTimings.getTime(MainHG.TimeLeft), "%time"), true);
				
			}

			if (MainHG.TimeLeft % 45 == 0 && MainHG.TimeLeft != 0){
				for (Player p : Teams.getGamers()){
					Message.P(p,Message.Replacer( Message.HeaderMenu, "--------", "&a PkzAPI &6&m"), false);
					Message.P(p, MessageHG.VoteForMap, false);
					for (mapInfo maps : MainHG.maps){
						new FancyMessage(
								"&6&l  " + maps.getIndex() + " &7 - " + maps.getMapName() + "&7  - &a" + MainHG.mapVotes.get(maps).size() + " &7votes")
						.tooltip("&7 Click to vote for " + maps.getMapName())
						.command("/map "+maps.getIndex()).send(p);
					}
				}
			}

			if (MainHG.TimeLeft == 30) {
				for (Player p : Teams.getGamers()) {
					if (!KitsHG.KitChoice.containsKey(p.getName())) 
						Message.P(p, MessageHG.RemindKit, true);
				}
			}
		}
		else{
			// Begin the game checks...
			if (Teams.getGamers().size() >= MainHG.AtleastNeededPlayers){
				try {
					GameFunctions.gamePreparation();
				} catch (SQLException e) {e.printStackTrace();}
				task.cancel();// Cancel the timer.
			}
			else{
				// We reset the time back to original.
				Message.G(MessageHG.NotEnoughOn, true);
				MainHG.TimeLeft = MainHG.PregameTime;
			}
		}
	}
}
