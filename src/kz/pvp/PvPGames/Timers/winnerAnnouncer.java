package kz.pvp.PvPGames.Timers;

import java.sql.ResultSet;
import java.sql.SQLException;

import kz.PvP.PkzAPI.Main;
import kz.PvP.PkzAPI.utilities.Message;
import kz.PvP.PkzAPI.utilities.Mysql;
import kz.pvp.PvPGames.Main.MainHG;
import kz.pvp.PvPGames.Utilities.MessageHG;

import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;




public class winnerAnnouncer extends BukkitRunnable {

	public static BukkitTask task = null;
	
	
	public winnerAnnouncer() {}

	@Override
	public void run() {
        ResultSet rs;
        long LastGame = 0;
        int Winner = 0;
        int LastGameID = 0;
        try {
            rs = Mysql.PS.getSecureQuery("SELECT * FROM HungerGames_Rounds WHERE Winner IS NOT NULL ORDER BY End DESC LIMIT 1");

            if (rs.next()){
                LastGame = rs.getLong("End");
                LastGameID = rs.getInt("ID");
                Winner = rs.getInt("Winner");
            }

            if (LastGame != MainHG.LastGame && LastGameID != MainHG.GameID){// New game just finished
                Message.G(Message.Replacer(MessageHG.LatestEndedGame, Mysql.getUsername(Winner), "%winner"), true);
                MainHG.LastGame = LastGame;// We set the latest game to the new captured one....
            }
        } catch (SQLException e1) {e1.printStackTrace();}
	}
}
