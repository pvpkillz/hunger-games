package kz.pvp.PvPGames.Timers;

import java.sql.SQLException;

import kz.PvP.PkzAPI.Main;
import kz.PvP.PkzAPI.methods.ConvertTimings;
import kz.PvP.PkzAPI.methods.Scoreboards;
import kz.PvP.PkzAPI.utilities.Message;
import kz.PvP.PkzAPI.utilities.Bar.BarAPI;
import kz.pvp.PvPGames.Main.GameFunctions;
import kz.pvp.PvPGames.Main.MainHG;
import kz.pvp.PvPGames.Methods.Generation;
import kz.pvp.PvPGames.Methods.Teams;
import kz.pvp.PvPGames.Utilities.MessageHG;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;




public class countdownTimer extends BukkitRunnable {

	public static BukkitTask task = null;
	public countdownTimer() {
		MainHG.TimeLeft = 15;
    	BarAPI.setMessage(Message.Replacer(MessageHG.startingIn, ConvertTimings.getTime(MainHG.TimeLeft), "%time"), (float) 100 * ((float)MainHG.TimeLeft / (float)15));
	}

	@Override
	public void run() {
		
        MainHG.TimeLeft--;

        if (MainHG.TimeLeft >= 1) {
        	BarAPI.setMessage(Message.Replacer(MessageHG.startingIn, ConvertTimings.getTime(MainHG.TimeLeft), "%time"), (float) 100 * ((float)MainHG.TimeLeft / (float)15));
        	
        } else if (MainHG.TimeLeft <= 0) { // Begin the game
            try {
                GameFunctions.startGracePeriod();
            } catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            task.cancel();
        }
		
		
	}
}
