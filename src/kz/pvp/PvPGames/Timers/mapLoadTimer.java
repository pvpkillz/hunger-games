package kz.pvp.PvPGames.Timers;

import java.sql.SQLException;
import java.util.ArrayList;

import kz.PvP.PkzAPI.methods.ConvertTimings;
import kz.PvP.PkzAPI.utilities.Message;
import kz.PvP.PkzAPI.utilities.Bar.BarAPI;
import kz.pvp.PvPGames.Main.GameFunctions;
import kz.pvp.PvPGames.Main.MainHG;
import kz.pvp.PvPGames.Methods.Generation;
import kz.pvp.PvPGames.Methods.Maps;
import kz.pvp.PvPGames.Utilities.Abilities;
import kz.pvp.PvPGames.Utilities.MessageHG;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;




public class mapLoadTimer extends BukkitRunnable {

	public static BukkitTask task = null;


	public mapLoadTimer() {
		MainHG.TimeLeft = 5;
		BarAPI.setMessage(Message.Replacer(Message.Replacer(MessageHG.GameBegins, ConvertTimings.convertTime(MainHG.TimeLeft), "%time"), MainHG.gameMap.getMapName(), "%map"), (float) ((float)((float)MainHG.TimeLeft / 5) * 100));
	}

	@Override
	public void run() {
		MainHG.TimeLeft--;

		for (Player p : Bukkit.getServer().getOnlinePlayers()) {
			p.setHealth(20.0);
			p.setFoodLevel(20);
		}


		if (MainHG.TimeLeft > 0) {
			BarAPI.setMessage(Message.Replacer(Message.Replacer(MessageHG.GameBegins, ConvertTimings.convertTime(MainHG.TimeLeft), "%time"), MainHG.gameMap.getMapName(), "%map"), (float) ((float)((float)MainHG.TimeLeft / 5) * 100));

			for (Player p : Bukkit.getServer().getOnlinePlayers())
				p.playSound(p.getLocation(), Sound.CLICK, 2, 1);
		}
		else{
			
			for (Player p : Bukkit.getServer().getOnlinePlayers())
				p.playSound(p.getLocation(), Sound.LEVEL_UP, 2, 1);
			try {
				Generation.createCorn(MainHG.gameWorld.getSpawnLocation());
				GameFunctions.gameStart();
				MainHG.feastLoc = Maps.getRandomLocation();
			} catch (SQLException e) {e.printStackTrace();}
			task.cancel();
		}
	}
}
