package kz.pvp.PvPGames.Timers;

import java.sql.SQLException;

import kz.PvP.PkzAPI.Main;
import kz.PvP.PkzAPI.methods.ConvertTimings;
import kz.PvP.PkzAPI.methods.Scoreboards;
import kz.PvP.PkzAPI.utilities.Message;
import kz.pvp.PvPGames.Main.GameFunctions;
import kz.pvp.PvPGames.Main.MainHG;
import kz.pvp.PvPGames.Methods.Generation;
import kz.pvp.PvPGames.Methods.Teams;
import kz.pvp.PvPGames.Utilities.MessageHG;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;




public class gracePeriodTimer extends BukkitRunnable {

	public static BukkitTask task = null;
	public gracePeriodTimer() {
		MainHG.TimeLeft = MainHG.GracePeriod;
	}

	@Override
	public void run() {
		
        MainHG.TimeLeft--;

        if (MainHG.TimeLeft >= 1) {
            
            if (((MainHG.TimeLeft % 30 == 0 && MainHG.TimeLeft >= 60) || (MainHG.TimeLeft % 10 == 0 && MainHG.TimeLeft <= 50) || (MainHG.TimeLeft <= 5)) && MainHG.TimeLeft != 0) {
                Message.G(Message.Replacer(MessageHG.GracePeriodEnds, ConvertTimings.convertTime(MainHG.TimeLeft), "%time"), true);
            }
        } else if (MainHG.TimeLeft <= 0) { // Begin the game
            try {
                GameFunctions.OnGameStarted();
            } catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            task.cancel();
        }
		
		
	}
}
