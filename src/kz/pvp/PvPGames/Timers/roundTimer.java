package kz.pvp.PvPGames.Timers;

import java.sql.SQLException;

import kz.PvP.PkzAPI.methods.ConvertTimings;
import kz.PvP.PkzAPI.utilities.Message;
import kz.pvp.PvPGames.Main.GameFunctions;
import kz.pvp.PvPGames.Main.MainHG;
import kz.pvp.PvPGames.Methods.Generation;
import kz.pvp.PvPGames.Methods.Maps;
import kz.pvp.PvPGames.Methods.Teams;
import kz.pvp.PvPGames.Utilities.ChestSpawn;
import kz.pvp.PvPGames.Utilities.MessageHG;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;




public class roundTimer extends BukkitRunnable {

	public static BukkitTask task = null;
	public roundTimer() {
		
		MainHG.TimeLeft = MainHG.MaxGameLength;
	}

	@Override
	public void run() {
		
        for (Player p : Bukkit.getServer().getOnlinePlayers()) {
            if (!Teams.isGamer(p)) {
                if (!Teams.isGamemaker(p) && !Teams.isSpectator(p)) {
                    Teams.addSpectators(p);
                }
                p.setHealth(20.0);
                p.setFoodLevel(20);
            }
        }

        MainHG.TimeLeft--;
        if (MainHG.TimeLeft >= 1) {
        	if (MainHG.TimeLeft % 10 == 0){
        		Maps.RandomizeChests();
        	}
        	
        	
            if ((MainHG.TimeLeft % ((60 * 2) + ( 60 * ConvertTimings.randomInt(0, 3)))) == 0) {
                Location loc = Maps.getRandomLocation();
                Message.G(Message.Replacer(MessageHG.ChestSpawnedRan, loc.getBlockX() + ", " + loc.getBlockY() + ", " + loc.getBlockZ(), "%loc"), true);
                ChestSpawn.spawnChest(loc);
            }
            if ((MainHG.TimeLeft - (MainHG.MaxGameLength - MainHG.FeastSpawnTime)) == 0){
            	Message.G(Message.Replacer(MessageHG.FeastSpawn, MainHG.feastLoc.getBlockX() + ", " + MainHG.feastLoc.getBlockY() + ", " + MainHG.feastLoc.getBlockZ(), "%loc"), true);
            	Generation.createFeast(MainHG.feastLoc);
            }
            
            if ((MainHG.TimeLeft - (MainHG.MaxGameLength - MainHG.FinalBattleTime)) == 0){
            	GameFunctions.generateFinalBattle();
            }
        } else if (MainHG.TimeLeft <= 0) { // Begin the game
            try {
				GameFunctions.endGame();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            task.cancel();
        }
		
		
	}
}
