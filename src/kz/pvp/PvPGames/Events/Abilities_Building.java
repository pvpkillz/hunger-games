package kz.pvp.PvPGames.Events;

import kz.PvP.PkzAPI.utilities.Message;
import kz.pvp.PvPGames.Main.MainHG;
import kz.pvp.PvPGames.Methods.Maps;
import kz.pvp.PvPGames.Methods.Generation;
import kz.pvp.PvPGames.Utilities.Abilities;
import kz.pvp.PvPGames.Utilities.KitsHG;
import kz.pvp.PvPGames.Utilities.MessageHG;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.entity.TNTPrimed;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.world.ChunkLoadEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.ArrayList;

public class Abilities_Building implements Listener {
    public static MainHG plugin;

    public Abilities_Building (MainHG mainclass){
        plugin = mainclass;
        mainclass.getServer().getPluginManager().registerEvents(this, mainclass);
    }

    @EventHandler
    public void OnBlockPlace(BlockPlaceEvent e) {
        Player p = e.getPlayer();
        Block b = e.getBlock();

        if (b.getType() == Material.TNT && KitsHG.hasAbility(p, Abilities.AbilityTNTActivate)) {
            b.setType(Material.AIR);
            TNTPrimed tnt = (TNTPrimed) b.getWorld().spawn(b.getLocation(), TNTPrimed.class);
            tnt.setIsIncendiary(true);
            tnt.setYield(4f);
        }

        if (b.getType() == Material.STONE_PLATE && KitsHG.hasAbility(p, Abilities.AbilityExplosivePlates)) {
            if (!Abilities.PlateLocations.containsKey(p.getName())) {
                Abilities.PlateLocations.put(p.getName(), new ArrayList<Location>());
            }
            Abilities.PlateLocations.get(p.getName()).add(b.getLocation());
            Message.P(p, MessageHG.PlatePlanted, false);
        }

        if (KitsHG.hasAbility(p, Abilities.AbilityBeaconPowerup) && b.getType() == Material.BEACON) {
            int maxDist = 10;
            for (int q = 1; q <= 3; q++) {
                final Block newBlock = b.getWorld().getBlockAt(b.getX(), b.getY()+q, b.getZ());
                newBlock.setType(Material.FENCE);
                Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
                    @Override
                    public void run() {
                        newBlock.setType(Material.AIR) ;
                    }
                }, 120);
            }

            final ArrayList<Block> blocks = new ArrayList<Block>();

            blocks.add(b.getWorld().getBlockAt(b.getX()+1, b.getY()+4, b.getZ()));
            blocks.add(b.getWorld().getBlockAt(b.getX()-1, b.getY()+4, b.getZ()));
            blocks.add(b.getWorld().getBlockAt(b.getX(), b.getY()+4, b.getZ()-1));
            blocks.add(b.getWorld().getBlockAt(b.getX(), b.getY()+4, b.getZ()+1));
            blocks.add(b.getWorld().getBlockAt(b.getX(), b.getY()+4, b.getZ()));

            for (Block block : blocks) {
                block.setType(Material.WOOL);
            }

            Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
                @Override
                public void run() {
                    for (Block block : blocks) {
                        block.setType(Material.AIR);
                    }
                }
            }, 120);

            for (Player other : Bukkit.getOnlinePlayers()) {
                if (other.getLocation().distanceSquared(p.getLocation()) <= Math.pow(maxDist, 2)) {
                    double length = b.getLocation().distance(other.getLocation());
                    int time = 1200 - ((int)length * 120);
                    if (other != p) {
                        other.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, time, 0));
                    } else {
                        p.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 1200, 1));
                    }

                    other.addPotionEffect(new PotionEffect(PotionEffectType.FIRE_RESISTANCE, 200, 1));
                    b.getWorld().strikeLightning(b.getLocation());
                    final Block bbbbbbvv = b.getWorld().getBlockAt(b.getLocation());
                    Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
                        @Override
                        public void run() {
                            bbbbbbvv.setType(Material.AIR) ;
                        }
                    }, 120);
                }
            }

        }

    }

    @EventHandler
    public void OnBlockBreak (BlockBreakEvent e){
        Player p = e.getPlayer();
        Block b = e.getBlock();

        if (KitsHG.hasAbility(p, Abilities.AbilityExplosivePlates)) {
            if (b.getType() == Material.STONE_PLATE) {
                if (Abilities.PlateLocations.containsKey(p.getName())) {
                    Abilities.PlateLocations.get(p.getName()).remove(b.getLocation());
                    Message.P(p, MessageHG.PlateUnplanted, false);
                }
            }
        }
        
        if (KitsHG.hasAbility(p, Abilities.AbilityBreakAllLogs)){
        	if (b.getType() == Material.LOG || b.getType() == Material.LOG_2){
	        	if (p.getItemInHand().getType().name().toLowerCase().contains("_axe")){//Player is holding some sort of axe...
	        		Block block = b;
	        		do{
	        			block.breakNaturally();
	        			block = block.getLocation().add(0, 1, 0).getBlock();
	        		} while (block.getType() == Material.LOG || block.getType() == Material.LOG_2);
	        	}
        	}
        }
    }


    @EventHandler
    public void OnExplosion (EntityExplodeEvent e) {
        for (Block b : new ArrayList<Block>(e.blockList())) {
            if (Generation.isCornucopiaBlock(b)) {
                e.blockList().remove(b);
            }
        }
    }

    @EventHandler
    public void OnChunkLoad (ChunkLoadEvent e) {
        Maps.RandomizeChunk(e.getChunk());
    }
}
