package kz.pvp.PvPGames.Events;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Map.Entry;
import java.util.logging.Logger;

import kz.PvP.PkzAPI.utilities.Message;
import kz.pvp.PvPGames.Enums.Gamestatus;
import kz.pvp.PvPGames.Main.MainHG;
import kz.pvp.PvPGames.Methods.Teams;
import kz.pvp.PvPGames.Utilities.Abilities;
import kz.pvp.PvPGames.Utilities.KitsHG;
import kz.pvp.PvPGames.Utilities.MessageHG;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityInteractEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerToggleSneakEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;

import fr.neatmonster.nocheatplus.checks.CheckType;
import fr.neatmonster.nocheatplus.hooks.NCPExemptionManager;

public class Abilities_Interaction implements Listener{
	public static MainHG plugin;
	static Logger log = Bukkit.getLogger();

	public Abilities_Interaction (MainHG mainclass){
		plugin = mainclass;
		mainclass.getServer().getPluginManager().registerEvents(this, mainclass);
	}

	@SuppressWarnings("deprecation")
	@EventHandler
	public void onBlockInteract (PlayerInteractEvent e){
		final Player p = e.getPlayer();
		Action a = e.getAction();
		Block cb = e.getClickedBlock();
		if (Teams.isGamer(p)){
			if (a == Action.RIGHT_CLICK_AIR || a == Action.RIGHT_CLICK_BLOCK){
				if (p.getItemInHand() != null){// User is holding an item of interest maybe?
					ItemStack hand = p.getItemInHand();



					/*
					 * Thor Ability / Strike lightning when Stone Axe is used.
					 */

					if (hand.getType() == Material.STONE_AXE && a == Action.RIGHT_CLICK_BLOCK  &&  KitsHG.hasAbility(p, Abilities.AbilityThor)){// We found our thor! :D
						if (Abilities.isCooled(p, Abilities.AbilityThor)){
							// He is one cool person :P
							Abilities.cooldown(p, Abilities.AbilityThor, Abilities.ThorCooldown);
							p.getWorld().strikeLightning(e.getClickedBlock().getLocation());
						}
						else{
							// He is not cool enough! :)
							Abilities.sendCooldown(p, Abilities.AbilityThor, Abilities.ThorCooldown);
						}
					}

					/*
					 * Cookie Strength Ability / Get Strength when a cookie is consumed :D
					 */

					if (hand.getType() == Material.COOKIE  &&  KitsHG.hasAbility(p, Abilities.AbilityCookieStrength)){// We found our thor! :D

						p.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 20 * Abilities.CookieStrengthLength, 1));
						if (hand.getAmount() > 1)
							hand.setAmount(hand.getAmount() - 1);
						else
							hand.setType(Material.AIR);
					}


					/*
					 * Spy Ability / Invisibility when using an apple.
					 */

					if (KitsHG.hasAbility(p, Abilities.AbilityInvisibilityFood) && e.getItem() != null && e.getItem().getType() == Abilities.InvisibilityFoodMaterial){
						if (Abilities.isCooled(p, Abilities.AbilityInvisibilityFood)){
							p.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, Abilities.InvisibilityFoodLength * 20, 0));
							Abilities.decreaseItemStack(p.getItemInHand(), 1);
							Message.P(p, MessageHG.YouAreNowInvisible, true);
							Abilities.cooldown(p, Abilities.AbilityInvisibilityFood, Abilities.InvisibilityFoodCooldown);
						}
						else
							Abilities.sendCooldown(p, Abilities.AbilityInvisibilityFood, Abilities.InvisibilityFoodCooldown);
					}
					/*
					 * Super Jump Ability
					 */
					if (KitsHG.hasAbility(p, Abilities.AbilitySuperJump) && e.getItem().getType() == Abilities.SuperJumpMaterial){
						if (Abilities.isCooled(p, Abilities.AbilitySuperJump)){
							Abilities.cooldown(p, Abilities.AbilitySuperJump, Abilities.SuperJumpCooldown);
							NCPExemptionManager.isExempted(p.getEntityId(), CheckType.MOVING_SURVIVALFLY);
							p.setVelocity(new Vector(p.getLocation().getDirection().getX() + 1,2,p.getLocation().getDirection().getZ() + 1));
							NCPExemptionManager.unexempt(p.getEntityId(), CheckType.MOVING_SURVIVALFLY);						
						}
						else
							Abilities.sendCooldown(p, Abilities.AbilitySuperJump, Abilities.SuperJumpCooldown);
					}
					/*
					 * The Teleport ability
					 */
					if (KitsHG.hasAbility(p, Abilities.AbilityTeleportWand) && hand.getType() == Abilities.TeleportWandItem){
						if (Abilities.isCooled(p, Abilities.AbilityTeleportWand)){
							Block b = p.getTargetBlock(null, Abilities.TeleportWandStrength);
							if (b.getType() != Material.AIR){
								Abilities.cooldown(p, Abilities.AbilityTeleportWand, Abilities.TeleportWandCooldown);
								Location teleportLoc = new Location(b.getLocation().getWorld(), 
										b.getLocation().getBlockX(), 
										b.getLocation().getBlockY(), 
										b.getLocation().getBlockZ());
								Message.P(p, MessageHG.TeleportWandSuccess, false);
								p.teleport(teleportLoc.add(0,1,0));
							}
							else
								Message.P(p, MessageHG.TeleportWandTooFar, false);
						}
						else
							Abilities.sendCooldown(p, Abilities.AbilityTeleportWand, Abilities.TeleportWandCooldown);
					}

					/*
					 * Feather Fly ability
					 */
					if (KitsHG.hasAbility(p, Abilities.AbilitySuperFly) && hand.getType() == Abilities.SuperFlyItem)
					{

						NCPExemptionManager.isExempted(p.getEntityId(), CheckType.MOVING_SURVIVALFLY);
						p.setFlying(true);
						p.setVelocity(new Vector(0,2,0));
						NCPExemptionManager.unexempt(p.getEntityId(), CheckType.MOVING_SURVIVALFLY);
						p.getInventory().removeItem(new ItemStack (Material.FEATHER, 1));
						Abilities.cooldown(p, Abilities.AbilitySuperFly, Abilities.SuperFlyCooldown);
						TimerTask action = new TimerTask() {
							public void run(){
								p.setAllowFlight(false);
								p.setFlying(false);
							}
						};
						Timer timer = new Timer();
						timer.schedule(action, Abilities.SuperFlyLength* 1000);
					}

					/*
					 * MilkMan Ability
					 */
					if (KitsHG.hasAbility(p, Abilities.AbilityMilkDrinker))
					{
						ItemStack bucket = new ItemStack(Material.MILK_BUCKET);

						if (!Abilities.MilkDrinks.containsKey(p))
							Abilities.MilkDrinks.put(p.getName(), 3);
						
						
						if ((Abilities.isCooled(p, Abilities.AbilityMilkDrinker)) && (hand.equals(bucket)) && (Abilities.MilkDrinks.get(p.getName()) >= 1)){

							p.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, 200, 0));
							p.addPotionEffect(new PotionEffect(PotionEffectType.FIRE_RESISTANCE, 200, 0));
							p.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 200, 0));
							int Left = Abilities.MilkDrinks.get(p.getName());

							if (Left >= 1){
								// Print Uses left.
								Abilities.MilkDrinks.put(p.getName(), Abilities.MilkDrinks.get(p) - 1);
							}
						}
						else if (!Abilities.isCooled(p, Abilities.AbilityMilkDrinker))
							Abilities.sendCooldown(p, Abilities.AbilityMilkDrinker, Abilities.MilkDrinkingCooldown);
						else 
							Message.P(p, MessageHG.unableToDrinkMilk, true);
					}

					/*
					 * Ore Smelter
					 */
					if (KitsHG.hasAbility(p, Abilities.AbilitySmeltOres))
					{
						ItemStack ore = new ItemStack(Material.IRON_ORE, 1);

						if(hand.equals(ore)) {
							p.getInventory().removeItem(new ItemStack (Material.IRON_ORE, 1));
							p.getInventory().addItem(new ItemStack(Material.IRON_INGOT, 1));
							p.sendMessage(ChatColor.RED+"You have just smelted an iron ingot.");
						}
					}
				}
			}
			else if (a == Action.PHYSICAL){
				if (cb.getType() == Material.STONE_PLATE){
					for (Entry<String, ArrayList<Location>> entries : Abilities.PlateLocations.entrySet()){
						if (entries.getValue().contains(cb.getLocation())){
							Player planter = plugin.getServer().getPlayerExact(entries.getKey());
							Abilities.causeExplosion(cb.getLocation(), 2f);
							Message.P(planter, Message.Replacer(MessageHG.LandmineExploded, p.getName(), "%damaged"), false);
							Message.P(p, Message.Replacer(MessageHG.LandminePlanted, planter.getName(), "%planter"), false);
						}
					}
				}
			}
		}


	}
	@EventHandler
	public void OnInteractWithEntity (PlayerInteractEntityEvent e){
		Player p = e.getPlayer();
		if (e.getRightClicked() instanceof Player){
			Player clicked = (Player) e.getRightClicked();
			if (Teams.isGamer(clicked) && Teams.isGamer(p)){
				if (KitsHG.hasAbility(p, Abilities.AbilityStealItems)){

					if (p.getItemInHand().getType() == Abilities.StealItemsStick){
						if (Abilities.isCooled(p, Abilities.AbilityStealItems)){
							
							for (ItemStack is : clicked.getInventory().getContents()){
								if (is != null){
									clicked.getInventory().remove(is);
									p.getInventory().addItem(is);
									Message.P(p, MessageHG.stoleFrom, true);
									Message.P(clicked, MessageHG.stolenFrom, true);
									Abilities.cooldown(p, Abilities.AbilityStealItems, Abilities.StealItemsCooldown);
									break;
								}
							}
						}
						else
							Abilities.sendCooldown(p, Abilities.AbilityStealItems, Abilities.StealItemsCooldown);
					}

				}
			}
		}
	}

	@EventHandler
	public void OnMoveEvent (PlayerMoveEvent e){
		// Just a nice small if statement that should really make the task less laggiez
		if ((e.getFrom().getBlockX() != e.getTo().getBlockX()) 
				|| (e.getFrom().getBlockY() != e.getTo().getBlockY())
				|| (e.getFrom().getBlockZ() != e.getTo().getBlockZ())){
			Player p = e.getPlayer();
			if (Teams.isGamer(p)){
				if (KitsHG.hasAbility(p, Abilities.AbilitySnowGround) && MainHG.Game != Gamestatus.Deathmatch){
					ArrayList<Block> Blockslist = Abilities.getBlocksAround(p.getLocation().subtract(0, 1, 0), 3, 0);
					for (Block block : Blockslist){
						if (block.getType() == Material.WATER || block.getType() == Material.STATIONARY_WATER)
							Abilities.setBlock(block, Abilities.FrostGroundWater);
						else if (block.getType() == Material.LAVA || block.getType() == Material.STATIONARY_LAVA)
							Abilities.setBlock(block, Abilities.FrostGroundLava);
					}
				}
				if (KitsHG.hasAbility(p, Abilities.AbilityNightHunter)){
					if (p.getWorld().getTime() >= 18000){
						p.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 20 * 5, 0));
					}
				}
				if (KitsHG.hasAbility(p, Abilities.AbilityUnderWaterPower)){
					if (p.getLocation().getBlock().getType() == Material.LAVA){
						p.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 20 * 10, 0));
						p.addPotionEffect(new PotionEffect(PotionEffectType.FIRE_RESISTANCE, 20 * 10, 5));
					}
				}
			}
		}
	}

	@EventHandler
	public void OnCrouch (PlayerToggleSneakEvent e){
		final Player p = e.getPlayer();
		if (Teams.isGamer(p)){
			if (KitsHG.hasAbility(p, Abilities.AbilityKnockBackShift)){
				if (Abilities.HoldStrength.containsKey(p.getName()) && Abilities.HoldTimers.containsKey(p.getName())){

					for (Player pl : Abilities.getPlayersAround(p.getLocation(), 7)){
						Vector unitVector = p.getLocation().toVector().subtract(pl.getLocation().toVector()).normalize().multiply(-1 * (1 + (0.1 * Abilities.HoldStrength.get(p.getName()))));
						pl.setVelocity(unitVector);
					}
					Message.P(p, MessageHG.YouHaveKnockedNearby, false);


					Abilities.HoldStrength.remove(p.getName());

					plugin.getServer().getScheduler().cancelTask(Abilities.HoldTimers.get(p.getName()));
					Abilities.HoldTimers.remove(p.getName());
				}

				int holdTimer = 0;
				holdTimer = Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(plugin, new Runnable() {
					@Override  
					public void run() {
						if (p.isSneaking()){
							if (!Abilities.HoldStrength.containsKey(p.getName()))
								Abilities.HoldStrength.put(p.getName(), 0);
							if (Abilities.HoldStrength.get(p.getName()) <= 10)
								Abilities.HoldStrength.put(p.getName(), Abilities.HoldStrength.get(p.getName()) + 1);
						}
						else{
							if (Abilities.HoldStrength.containsKey(p.getName())){
								// Knock Back dem players :P
								for (Player pl : Abilities.getPlayersAround(p.getLocation(), 7)){
									Location t = pl.getLocation().subtract(p.getLocation());
									double distance = pl.getLocation().distance(p.getLocation()) * (1 + (0.4 * Abilities.HoldStrength.get(p.getName())));
									t.getDirection().normalize().multiply(-1);
									t.multiply(distance/1.2);

									pl.setVelocity(t.toVector());
								}
								Message.P(p, MessageHG.YouHaveKnockedNearby, false);
								Abilities.HoldStrength.remove(p.getName());
								Abilities.HoldTimers.remove(p.getName());
							}
						}
					}
				}, 10L, 10L);

				Abilities.HoldTimers.put(p.getName(), holdTimer);
			}

		}



	}





	/*
	@EventHandler
	public void OnItemConsume (PlayerItemConsumeEvent e){
		Player p = e.getPlayer();

		if (KitsHG.hasAbility(p, Abilities.AbilityInvisibilityFood)){
			if (e.getItem().getType() == Abilities.InvisibilityFoodMaterial && Abilities.isCooled(p, Abilities.AbilityInvisibilityFood)){
				p.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, Abilities.InvisibilityFoodLength * 20, 0));
				Message.P(p, Message.YouAreNowInvisible, true);
			}
		}

	}
	 */
}
