package kz.pvp.PvPGames.Events;

import kz.PvP.PkzAPI.Main;
import kz.PvP.PkzAPI.methods.PlayersInfo;
import kz.PvP.PkzAPI.utilities.Message;
import kz.PvP.PkzAPI.utilities.Stats;
import kz.pvp.PvPGames.Enums.Gamestatus;
import kz.pvp.PvPGames.Main.MainHG;
import kz.pvp.PvPGames.Methods.Teams;
import kz.pvp.PvPGames.Utilities.MessageHG;
import kz.pvp.PvPGames.Utilities.StatsHG;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.inventory.ItemStack;

import java.sql.SQLException;
import java.util.HashMap;

public class DeathListener implements Listener{
	public static MainHG plugin;
	public static HashMap<String, Integer> SpecWarnings = new HashMap<String, Integer>();

	public DeathListener (MainHG mainclass){
		plugin = mainclass;
		Bukkit.getPluginManager().registerEvents(this, mainclass);
	}


	@EventHandler(priority = EventPriority.LOWEST)
	public void onDeathEvent(PlayerDeathEvent e) throws SQLException {
		if (e.getEntity() instanceof Player){
			Player def = (Player) e.getEntity();
			Player dam = null;
			if (!PlayersInfo.recordedExtraStat.containsKey(def.getUniqueId()))
				PlayersInfo.recordedExtraStat.put(def.getUniqueId(), new HashMap<String, String>());
			PlayersInfo.recordedExtraStat.get(def.getUniqueId()).put("RoundID", "INT/"+MainHG.GameID);


			for (ItemStack i : e.getDrops()){
				if (i.getType() != Material.AIR)
					def.getWorld().dropItem(def.getLocation(), i);
			}
			e.getDrops().clear();

			if (def.getKiller() instanceof Player){
				dam = def.getKiller();
				if (MainHG.Game != Gamestatus.Pregame){// In the event of a death within game....
					if (MainHG.DeathSign) 
						SpawnADeathSign(def, dam);
					Teams.eliminateUser(def, dam, false);
				}
			} else {
				Teams.eliminateUser(def, null, false);
			}
		}
	}


	private void SpawnADeathSign(Player def, Player dam) {
		Block sign = def.getLocation().getBlock();
		sign.setType(Material.SIGN_POST);
		Sign s = (Sign)sign.getState();
		s.setLine(0, ChatColor.RED + "R.I.P");
		s.setLine(1, ChatColor.DARK_GRAY + def.getName());
		s.setLine(2, ChatColor.RED + "Slain by:");
		s.setLine(3, ChatColor.GREEN + dam.getName());
		s.update();
	}

	@EventHandler
	public void onPlayerHurt(EntityDamageEvent e) {
		if (e.getEntity() instanceof Player) {
			Player p = (Player) e.getEntity();
			if (!Teams.isGamer(p)) {
				e.setCancelled(true);
			} else if (MainHG.Game == Gamestatus.GracePeriod) {
				e.setCancelled(true);
			}
		}
	}



	@EventHandler
	public void onDamageEvent(EntityDamageByEntityEvent e) throws SQLException {
		if (e.getEntity() instanceof Player && e.getDamager() instanceof Player) {
			Player def = (Player) e.getEntity();
			Player dam = (Player) e.getDamager();

			if (Teams.isGamer(dam) && Teams.isGamer(def)) {
				// We do teams here...
				if (MainHG.TeamDefend && MainHG.EnableTeams) {
					if (StatsHG.isInTeam(dam, def.getName())) {
						if (StatsHG.isInTeam(def, dam.getName()) && MainHG.Gamers.size() >= 3) {
							// They both have each other in their teams... Such friends! :D
							Message.P(dam, Message.Replacer(MessageHG.OpponentIsFriend, def.getName(), "%def"), true);
							Message.P(def, Message.Replacer(MessageHG.FriendMayBeFoe, dam.getName(), "%dam"), true);
							e.setDamage((e.getDamage()/3) * 2);
						}
					}
				}
			} else {
				e.setCancelled(true);
			}
		}
	}
}
