package kz.pvp.PvPGames.Events;

import java.util.logging.Logger;

import kz.PvP.PkzAPI.utilities.Message;
import kz.pvp.PvPGames.Main.MainHG;
import kz.pvp.PvPGames.Methods.Generation;
import kz.pvp.PvPGames.Utilities.MessageHG;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockBurnEvent;
import org.bukkit.event.block.BlockPistonExtendEvent;
import org.bukkit.event.block.BlockPistonRetractEvent;

public class BlockModificationEvents implements Listener{
	public static MainHG plugin;
	static Logger log = Bukkit.getLogger();
	
	public BlockModificationEvents (MainHG mainclass){
		plugin = mainclass;
		mainclass.getServer().getPluginManager().registerEvents(this, mainclass);
	}
	@EventHandler
	public void OnBlockBreak (BlockBreakEvent e){
		
		if (Generation.isCornucopiaBlock(e.getBlock())){
			Message.P(e.getPlayer(), MessageHG.CannotDoAtThisTime, true);
			e.setCancelled(true);
		}
		Material bT = e.getBlock().getType();
		if (bT == Material.DIAMOND_BLOCK || bT == Material.IRON_BLOCK || bT == Material.GOLD_BLOCK){
			Message.P(e.getPlayer(), MessageHG.CannotDoAtThisTime, true);
			e.setCancelled(true);
		}
		
	}
	@EventHandler
	public void OnBlockBreak (BlockBurnEvent e){
		
		if (Generation.isCornucopiaBlock(e.getBlock())){
			e.setCancelled(true);
		}
		
	}
	@EventHandler
	public void OnBlockBreak (BlockPistonExtendEvent e){
		for (Block b : e.getBlocks()){
			if (Generation.isCornucopiaBlock(b)){
				e.setCancelled(true);
			}
		}
		
	}
	@EventHandler
	public void OnBlockBreak (BlockPistonRetractEvent e){
		if (Generation.isCornucopiaBlock(e.getRetractLocation().getBlock())){
			e.setCancelled(true);
		}
		
	}
}
