package kz.pvp.PvPGames.Events;

import kz.PvP.PkzAPI.methods.ConvertTimings;
import kz.PvP.PkzAPI.utilities.Message;
import kz.PvP.PkzAPI.utilities.Stats;
import kz.pvp.PvPGames.Enums.Gamestatus;
import kz.pvp.PvPGames.Main.MainHG;
import kz.pvp.PvPGames.Methods.Disguises;
import kz.pvp.PvPGames.Methods.Teams;
import kz.pvp.PvPGames.Utilities.Abilities;
import kz.pvp.PvPGames.Utilities.KitsHG;
import kz.pvp.PvPGames.Utilities.MessageHG;
import kz.pvp.PvPGames.Utilities.StatsHG;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.*;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.EntityTargetEvent.TargetReason;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.projectiles.ProjectileSource;
import org.bukkit.util.Vector;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class Abilities_Fighting implements Listener {
	public static MainHG plugin;

	public Abilities_Fighting (MainHG mainclass){
		plugin = mainclass;
		mainclass.getServer().getPluginManager().registerEvents(this, mainclass);
	}

	@EventHandler
	public void OnDamageEntity(EntityDamageEvent e) {
		if (e.getEntity() instanceof Player){
			Player def = (Player) e.getEntity();
			if (e.getCause() == DamageCause.FALL && (KitsHG.hasAbility(def, Abilities.AbilitySuperJump) || MainHG.MaxGameLength - 10 <= MainHG.TimeLeft)) {
				e.setCancelled(true);
			}

			if (e.getCause() == DamageCause.FALL && KitsHG.hasAbility(def, Abilities.AbilityBreakFall)) {
				List<Player> players = Abilities.getPlayersAround(def.getLocation(), 10);
				for (Player pl : players){
					pl.damage(e.getDamage());
				}
				e.setCancelled(true);
			}

		}
	}


	@EventHandler
	public void OnProjectileLand(ProjectileHitEvent e) {
		if (e.getEntity() instanceof Arrow && e.getEntity().getShooter() instanceof Player) {
			// If the projectile is an arrow
			Arrow arrow = (Arrow) e.getEntity();
			Player shooter = (Player) arrow.getShooter();

			if (KitsHG.hasAbility(shooter, Abilities.AbilityExplosiveArrow)) {
				arrow.getLocation().getWorld().createExplosion(arrow.getLocation(), 1.5F);
			}
		} else if (e.getEntity() instanceof Snowball) {
			// If the projectile is a snowball
			Snowball snowball = (Snowball) e.getEntity();
			final Location loc = snowball.getLocation();
			ProjectileSource ps = snowball.getShooter();
			if (ps instanceof Player){
				Player shooter = (Player)ps;
				if (KitsHG.hasAbility(shooter, Abilities.AbilitySnowballConfuse)) {
					snowball.getWorld().createExplosion(snowball.getLocation(), 0.0f, false);

					List<Player> playersaround = Abilities.getPlayersAround(snowball.getLocation(), Abilities.SnowballConfusionRadius);
					for (Player players : playersaround) {
						players.addPotionEffect(new PotionEffect(PotionEffectType.CONFUSION, Abilities.SnowballConfusionLength * 20, 0));
					}

					snowball.remove();
				}

				if (KitsHG.hasAbility(shooter, Abilities.AbilityFrost)) {
					ArrayList<Block> Blockslist = Abilities.getBlocksAround(snowball.getLocation(), 5, 0);
					for (Block block : Blockslist) {
						if (block.getType() == Material.WATER || block.getType() == Material.STATIONARY_WATER) {
							Abilities.setBlock(block, Abilities.FrostGroundWater);
						} else if (block.getType() == Material.LAVA || block.getType() == Material.STATIONARY_LAVA) {
							Abilities.setBlock(block, Abilities.FrostGroundLava);
						} else if (block.getType() != Material.CROPS && block.getType() != Material.LONG_GRASS && block.getType() != Material.ICE){
							Abilities.setBlock(block, Abilities.FrostGround);
						}
					}
				}

				if (KitsHG.hasAbility(shooter, Abilities.AbilitySnowSpawner)) {
					for (int x = loc.getBlockX() - 3; x <= loc.getBlockX() + 3; x++) {
						for (int z = loc.getBlockZ() - 3; z <= loc.getBlockZ() + 3; z++) {
							loc.getWorld().getBlockAt(x, loc.getBlockY(), z).setType(Material.ICE);
							plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
								@Override
								public void run(){
									for (int x = loc.getBlockX() - 3; x <= loc.getBlockX() + 3; x++) {
										for (int z = loc.getBlockZ() - 3; z <= loc.getBlockZ() + 3; z++) {
											loc.getWorld().getBlockAt(x, loc.getBlockY() + 1, z).setType(Material.SNOW);
										}
									}
								}
							}, 10);
						}
					}
				}
			}
		} else if (e.getEntity() instanceof EnderPearl) {
			// If the projectile is a enderpearl
			EnderPearl enderpearl = (EnderPearl) e.getEntity();
			ProjectileSource ps = enderpearl.getShooter();
			if (ps instanceof Player) {
				Player shooter = (Player) ps;

				if (KitsHG.hasAbility(shooter, Abilities.AbilityEnderPearlRoom) && Abilities.isCooled(shooter, Abilities.AbilityEnderPearlRoom)) {
					Abilities.generateEnderPearlRoom(enderpearl.getLocation(), shooter);

					Abilities.cooldown(shooter, Abilities.AbilityEnderPearlRoom, Abilities.EnderPearlRoomCooldown);// cooldown of 20 seconds...
					enderpearl.remove();
				}
			}
		}

	}


	@EventHandler
	public void OnPlayerDeath(PlayerDeathEvent e) {
		Player def = e.getEntity();
		if (KitsHG.hasAbility(def, Abilities.AbilityExplodeDeath)){
			Abilities.causeExplosion(def.getLocation(), 3.0f);
		}
		if (def.getKiller() instanceof Player){
			Player dam = def.getKiller();


			if (KitsHG.hasAbility(dam, Abilities.AbilityRefillFoodKill)) {
				dam.setFoodLevel(20);
			}


			if (KitsHG.hasAbility(dam, Abilities.AbilityXPKiller)) {
				dam.giveExpLevels((int)Math.ceil(Stats.getKills(dam.getName()) * 1.15));
			}
		}
		if (def.getLastDamageCause() instanceof EntityDamageByEntityEvent) {
			EntityDamageByEntityEvent ede = (EntityDamageByEntityEvent) def.getLastDamageCause();
			if (ede.getDamager() instanceof Arrow) {
				Arrow arrow = (Arrow) ede.getDamager();
				if (arrow.getShooter() instanceof Player) {
					Player shooter = (Player) arrow.getShooter();
					Stats.earnedAKill(shooter);

					if (KitsHG.hasAbility(shooter, Abilities.AbilityArcherUpgrade)) {
						if (!Abilities.ArcherUpgrade.containsKey(shooter.getName())) {
							Abilities.ArcherUpgrade.put(shooter.getName(), 0);
						}

						int previousKills = Abilities.ArcherUpgrade.get(shooter.getName());

						if (previousKills <= 2) {
							// Upgrade Bow to Level 1
							Abilities.upgradeBow(shooter, 1);
						} else if (previousKills <= 5) {
							// Upgrade Bow to Level 2
							Abilities.upgradeBow(shooter, 2);
						} else if (previousKills <= 9) {
							// Upgrade Bow to level 3
							Abilities.upgradeBow(shooter, 3);
						}
					}
				}
			}
		}
	}

	@EventHandler
	public void OnEntityTarget(EntityTargetEvent e) {
		Entity ent = e.getEntity();
		if (ent instanceof Monster) {
			Monster monster = (Monster) ent;

			if (e.getTarget() instanceof Player) {
				Player p = (Player) e.getTarget();
				if ((KitsHG.hasAbility(p, Abilities.AbilityHideFromMobs) || MainHG.Game != Gamestatus.Game) && (e.getReason() == TargetReason.CLOSEST_PLAYER || e.getReason() == TargetReason.RANDOM_TARGET)) {
					monster.setTarget(null);
				}
			}
		}
	}

	@EventHandler
	public void OnEntityDeath (EntityDeathEvent e){
		Entity ent = e.getEntity();
		if (ent instanceof Animals){
			Animals animal = (Animals) ent;
			if (animal.getKiller() instanceof Player){
				Player dam = (Player) animal.getKiller();
				if (KitsHG.hasAbility(dam, Abilities.AbilityPigHunter)) {
					if (e.getEntity() instanceof Pig){
						e.getDrops().add(new ItemStack(Material.PORK));
					}
				}
			}
		}
	}

	@EventHandler
	public void OnDamageEntityDamage(final EntityDamageByEntityEvent e) throws SQLException {
		if (e.getEntity() instanceof Player) {
			final Player def = (Player) e.getEntity();
			if (e.getDamager() instanceof Player) {
				Player dam = (Player) e.getDamager();



				if (KitsHG.hasAbility(dam, Abilities.AbilityHealWithHit)) {
					if (dam.getHealth() <= 19)
						dam.setHealth(dam.getHealth() + 1);
				}


				if (KitsHG.hasAbility(dam, Abilities.AbilityStoneDmg)) {
					if (dam.getItemInHand().getType() == Material.AIR) {
						e.setDamage(6.0);
					}
				}

				if (KitsHG.hasAbility(dam, Abilities.AbilityPoisonHit)){
					if (ConvertTimings.randomInt(0, 100) <= Abilities.PoisonHitChance){
						def.addPotionEffect(new PotionEffect(PotionEffectType.POISON, 20 * 5, 0));
					}
				}

				if (KitsHG.hasAbility(def, Abilities.AbilityCrouchBlock)) {
					if (def.isSneaking()) {
						e.setDamage((e.getDamage()/3) * 2);// 33% of the DMG is removed.
					} else if (dam.isSneaking()) {
						e.setDamage((e.getDamage()/3));
						Message.P(dam, MessageHG.YouAreCrouching, false);
					}
				}

				if (KitsHG.hasAbility(dam, Abilities.AbilityInstantBackstab)) {
					if (Math.abs(dam.getLocation().getDirection().angle(def.getLocation().getDirection())) < 1) {
						// Wow, the person has real skills! :P
						// We want to limit the player to a specific weapon, else the ability would be unfair.
						if (dam.getItemInHand().getType() == Abilities.InstantBackstabWeapon) {
							e.setDamage(e.getDamage() + Abilities.InstantBackstabHP);
							Message.P(dam, Message.Replacer(MessageHG.Backstabbed, def.getName(), "%def"), false);
							Message.P(def, Message.Replacer(MessageHG.BackstabbedBy, dam.getName(), "%dam"), false);
						}
					}
				}

				if (KitsHG.hasAbility(def, Abilities.AbilityCharmeleon) && MainHG.DisguiseCraftEnable && Disguises.isDisguised(dam)) {
					Disguises.unDisguise(def);
					Message.P(def, Message.Replacer(MessageHG.Undisguised, dam.getName(), "%dam"), false);
				}

				if (!e.isCancelled()) {
					StatsHG.addToAssist(dam, def);
				}
			} else if (e.getDamager() instanceof Arrow) {
				Arrow arrow = (Arrow) e.getDamager();
				if (arrow.getShooter() instanceof Player) {
					Player dam = (Player) arrow.getShooter();
					if (Teams.isGamer(dam) && Teams.isSpectator(def)) {
						arrow.setBounce(false);

						def.teleport(def.getLocation().add(0, 5, 0));
						def.setFlying(true);
						Vector velo = arrow.getVelocity();
						Location loc = arrow.getLocation();
						Arrow newArrow = dam.getWorld().spawn(loc, Arrow.class);
						newArrow.setShooter(dam);
						newArrow.setVelocity(velo);
						newArrow.setBounce(false);
						e.setCancelled(true);


						/*
						def.teleport(def.getLocation().add(0, 10, 0));
						if (!DeathListener.SpecWarnings.containsKey(def.getName()))
						DeathListener.SpecWarnings.put(def.getName(), 1);
						else
							DeathListener.SpecWarnings.put(def.getName(), DeathListener.SpecWarnings.get(def.getName()) + 1);

						if (DeathListener.SpecWarnings.get(def.getName()) <= 2){
							def.teleport(Main.World.getSpawnLocation());
						}
						else if (DeathListener.SpecWarnings.get(def.getName()) >=3){
							def.kickPlayer("Don't block the attackers...");
						}
						 */
					} else {
						if(KitsHG.hasAbility(dam, Abilities.AbilityInstaBowKill)) {
							if (dam.getLocation().distance(def.getLocation()) >= Abilities.ArrowInstaKillDistance) {
								if (def.getInventory().getHelmet() == null) {
									Teams.eliminateUser(def, dam, false);
								} else {
									Message.P(def, MessageHG.HelmetSavedYourLife, false);
								}
							}
						}

						if (KitsHG.hasAbility(dam, Abilities.AbilityArcherUpgrade)) {
							if (!Abilities.ArcherUpgrade.containsKey(dam.getName())) {
								Abilities.ArcherUpgrade.put(dam.getName(), 0);
							}

							int previousKills = Abilities.ArcherUpgrade.get(dam.getName());

							if (previousKills <= 2) {
								e.setDamage(e.getDamage());
							} else if (previousKills <= 5) {
								e.setDamage(e.getDamage() + 3.0);
							} else if (previousKills <= 9) {
								e.setDamage(e.getDamage() + 5.0);
							}
						}

						if (!e.isCancelled()){
							StatsHG.addToAssist(dam, def);
							//Stats.addDamage(dam, e.getDamage());
						}
					}
				}
			} else if (e.getCause() == DamageCause.LIGHTNING) {
				if (KitsHG.hasAbility(def, Abilities.AbilityThor)) {
					e.setCancelled(true);
				}
			}
		} else if (e.getDamager() instanceof Player) {
			Player dam = (Player) e.getDamager();
			if (e.getEntity() instanceof Monster || e.getEntity() instanceof Animals) {



				if (KitsHG.hasAbility(dam, Abilities.AbilityCharmeleon)) {
					if (MainHG.DisguiseCraftEnable) {
						Message.P(dam, Message.Replacer(MessageHG.DisguiseAs, Message.CleanCapitalize(e.getEntity().getType().getName()), "%disguise"), false);
						Disguises.disguiseAs(dam, e.getEntity().getType());
					}
				}
			}
		} else if (e.getDamager() instanceof Egg) {
			// If the projectile is a enderpearl
			Egg egg = (Egg) e.getDamager();
			ProjectileSource ps = egg.getShooter();
			if (ps instanceof Player) {
				Player shooter = (Player) ps;
				if (KitsHG.hasAbility(shooter, Abilities.AbilityPokemonHunt))
					if(e.getEntity() instanceof Animals || e.getEntity() instanceof Monster){
						ItemStack item = new ItemStack(Material.MONSTER_EGG, 1);
						item.setDurability((short) e.getEntity().getEntityId());
						shooter.getInventory().addItem(item);
						e.getEntity().remove();
					}
			}
		} else if (e.getDamager() instanceof Snowball){
			Snowball snowball = (Snowball) e.getDamager();
			ProjectileSource ps = snowball.getShooter();
			if (ps instanceof Player){
				Player shooter = (Player) ps;
				if (KitsHG.hasAbility(shooter, Abilities.AbilitySnowballDmg)){
					e.setDamage(Abilities.SnowballDamage);
				}
			}
		}
	}





	/*This method will return the player's remaining health after the specified amount of damage was applied to him/her.*/
	/*
	public double absoluteDamage(Player player, double d, boolean ignoreArmor) {
	  CraftPlayer cp = (CraftPlayer)player;
	  EntityPlayer ep = cp.getHandle();
	  if((ep.noDamageTicks > ep.maxNoDamageTicks / 2.0F) && (d <= ep.lastDamage)) {
	    return 0;
	  }
	  double aS = 0;
	  boolean invulnerable = false;
	  try {
		  if (EntityLiving.class.getDeclaredField("aS") != null){
		    java.lang.reflect.Field faS = EntityLiving.class.getDeclaredField("aS");
		    java.lang.reflect.Field fI = EntityLiving.class.getDeclaredField("invulnerable");
		    faS.setAccessible(true);
		    fI.setAccessible(true);
		    invulnerable = fI.getBoolean(ep);
		  }
	  }
	  catch(Exception e) {
	    e.printStackTrace();
	  }
	  if(invulnerable) {
	    return 0;
	  }
	  if (!ignoreArmor) {
	    int j = 15;
	    double k = d * j + aS;
	    d = k / 25;
	    aS = k % 25;
	  }
	  if(ep.hasEffect(MobEffectList.RESISTANCE)) {
	    int j = (ep.getEffect(MobEffectList.RESISTANCE).getAmplifier() + 1) * 5;
	    int k = 25 - j;
	    double l = d * k + aS;
	    d = l / 25;
	  }
	  return d;
	}
	 */
}
