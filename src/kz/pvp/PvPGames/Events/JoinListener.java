package kz.pvp.PvPGames.Events;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BookMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.ScoreboardManager;












import kz.PvP.PkzAPI.Main;
import kz.PvP.PkzAPI.methods.PlayersInfo;
import kz.PvP.PkzAPI.utilities.Message;
import kz.PvP.PkzAPI.utilities.Mysql;
import kz.PvP.PkzAPI.utilities.Stats;
import kz.pvp.PvPGames.Enums.Gamestatus;
import kz.pvp.PvPGames.Enums.Team;
import kz.pvp.PvPGames.Main.MainHG;
import kz.pvp.PvPGames.Methods.Kits;
import kz.pvp.PvPGames.Methods.Teams;
import kz.pvp.PvPGames.Methods.TimedCommand;
import kz.pvp.PvPGames.Utilities.MessageHG;
import kz.pvp.PvPGames.Utilities.StatsHG;

public class JoinListener implements Listener{
	public static MainHG plugin;
	static Logger log = Bukkit.getLogger();
	public static HashMap<String, Integer> DelayedDeaths = new HashMap<String, Integer>();// Gamers in the server / Hashmap incase we want to convert to multiple arenas



	public JoinListener (MainHG mainclass){
		plugin = mainclass;
		mainclass.getServer().getPluginManager().registerEvents(this, mainclass);
	}




	// This event is run, once a user joins the server
	@EventHandler
	public void OnPlayerJoin (PlayerJoinEvent e) throws SQLException{
		Player p = e.getPlayer();
		e.setJoinMessage(null);
		JoinedServer(p);
	}

	public static void JoinedServer(Player p) throws SQLException {		
		if (MainHG.Game == Gamestatus.Pregame){
			Teams.addPlayer(p);

			for (PotionEffect pe : p.getActivePotionEffects())
				p.removePotionEffect(pe.getType());
			PlayersInfo.ClearFully(p);

			if (p.hasPermission(MessageHG.PermGamemaker)){
				Message.P(p, MessageHG.JoinedAsPlayerSwitchToGM, true);
				TimedCommand.TimedCommands(p, MainHG.GamemakerTime, "gamemaker");
			}
			if (MainHG.GiveStartBook)
				giveBook(p);
			if (MainHG.KitMenu)
				Teams.giveTools(p, Team.Player);

		}
		else if (MainHG.Game == Gamestatus.GracePeriod || MainHG.Game == Gamestatus.Game){

			if (DelayedDeaths.containsKey(p.getName())){
				plugin.getServer().getScheduler().cancelTask(DelayedDeaths.get(p.getName()));
				DelayedDeaths.remove(p.getName());
				Message.P(p, MessageHG.YouHaveRejoinedTheGame, true);
			}
			else if (p.hasPermission(MessageHG.PermGamemaker))
				Teams.addGamemakers(p);
			else
				Teams.addSpectators(p);
		}
	}



	private static void giveBook(Player p) {
		ItemStack book = new ItemStack(Material.WRITTEN_BOOK);
		BookMeta meta = (BookMeta) book.getItemMeta();
		meta.setTitle(ChatColor.GRAY + "" + ChatColor.BOLD + "The Information Booklet");
		meta.setAuthor(ChatColor.DARK_PURPLE + "PkzHG");
		meta.setPages(MainHG.content);
		book.setItemMeta(meta);
		p.getInventory().addItem(book);
	}




	@EventHandler
	public void OnPlayerJoin (PlayerLoginEvent e){
		Player p = e.getPlayer();
		if (MainHG.Game != Gamestatus.Pregame){
			// Player has joined the game, after it began.
			if (!Teams.isGamer(p) && MainHG.SpectatorEnable == false && !p.hasPermission(MessageHG.PermSpectator) && !p.hasPermission("pvpgames.gamemaker")){
				// User was not in the game and joined the server.
				e.disallow(PlayerLoginEvent.Result.KICK_OTHER, MessageHG.GameIsInProgress);
			}
			else if (MainHG.SpectatorEnable == false && (Teams.isGamer(p) || (p.hasPermission(MessageHG.PermGamemaker)))){
				// User was in game, and has rejoined the server
				e.allow();
			}
			else if (!Teams.isGamer(p)){
				// User was not in game
			}
		}
		else{
			if (plugin.getServer().getOnlinePlayers().length >= plugin.getServer().getMaxPlayers()){
				if (p.hasPermission(MessageHG.PermJoinFull))
					e.allow();
				else
					e.disallow(PlayerLoginEvent.Result.KICK_OTHER, MessageHG.GameIsFull);
			}
		}
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void OnQuit (PlayerKickEvent e) throws SQLException{
		final Player p = e.getPlayer();
		e.setLeaveMessage(null);
		leaveGame(p);

	}
	@EventHandler(priority = EventPriority.HIGHEST)
	public void OnQuit (PlayerQuitEvent e) throws SQLException{
		final Player p = e.getPlayer();
		leaveGame(p);
		e.setQuitMessage(null);

	}




	private void leaveGame(final Player p) throws SQLException {
		if (MainHG.Game == Gamestatus.GracePeriod || MainHG.Game == Gamestatus.Game){
			if (Teams.isGamer(p)){
				// We begin a timer to close his connection
				int DelayedDeath = plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
					public void run() {
						if (DelayedDeaths.containsKey(p.getName())){
							Message.G(Message.Replacer(MessageHG.TributeFellDC, p.getName(), "%user"), true);
							try {
								if (!PlayersInfo.recordedExtraStat.containsKey(p.getUniqueId()))
									PlayersInfo.recordedExtraStat.put(p.getUniqueId(), new HashMap<String, String>());
								PlayersInfo.recordedExtraStat.get(p.getUniqueId()).put("RoundID", "INT/"+MainHG.GameID);
								PlayersInfo.EliminatedUser(null, p);
								Teams.eliminateUser(p, null, true);
							} catch (SQLException e) {
								e.printStackTrace();
							}
						}
					}
				}, 20L * MainHG.PlayerTimeout);
				DelayedDeaths.put(p.getName(), DelayedDeath);
			}
			// Update Fame...
			if (Stats.getScore(p.getName()) >= 0 && Main.UseMySQL == true){
				String ID = ""+Mysql.getUserID(p.getName());
				ResultSet rs = Mysql.PS.getSecureQuery("SELECT * FROM HungerGames_Fame WHERE User = ?", ID);

				if (rs.next()){
					Mysql.PS.getSecureQuery("UPDATE HungerGames_Fame SET Fame = ? WHERE User = ?", ""+ Stats.getHungerGamesFame(p.getName()), ID);
				}
				else{
					Mysql.PS.getSecureQuery("INSERT INTO HungerGames_Fame (User, Fame) VALUES (?,?)", ID, ""+Stats.getHungerGamesFame(p.getName()));
				}
			}
		}
		else if (MainHG.Game == Gamestatus.Pregame){
			Teams.deleteUser(p);
		}
	}

}
