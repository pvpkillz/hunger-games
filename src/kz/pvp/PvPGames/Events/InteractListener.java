package kz.pvp.PvPGames.Events;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.logging.Logger;

import kz.PvP.PkzAPI.methods.ConvertTimings;
import kz.PvP.PkzAPI.utilities.Message;
import kz.PvP.PkzAPI.utilities.statHooks.HungerGames;
import kz.pvp.PvPGames.Commands.PlayerCommands;
import kz.pvp.PvPGames.Enums.GameType;
import kz.pvp.PvPGames.Enums.Gamestatus;
import kz.pvp.PvPGames.Enums.Team;
import kz.pvp.PvPGames.Enums.mapInfo;
import kz.pvp.PvPGames.Main.MainHG;
import kz.pvp.PvPGames.Methods.Generation;
import kz.pvp.PvPGames.Methods.Maps;
import kz.pvp.PvPGames.Methods.Teams;
import kz.pvp.PvPGames.Utilities.Abilities;
import kz.pvp.PvPGames.Utilities.KitsHG;
import kz.pvp.PvPGames.Utilities.MessageHG;
import kz.pvp.PvPGames.Utilities.StatsHG;
import me.confuserr.banmanager.BmAPI;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.craftbukkit.v1_7_R3.inventory.CraftInventoryCustom;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.Event.Result;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityChangeBlockEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;

import ru.tehkode.permissions.PermissionUser;
import ru.tehkode.permissions.bukkit.PermissionsEx;



public class InteractListener implements Listener{
	public static MainHG plugin;
	static Logger log = Bukkit.getLogger();

	public InteractListener (MainHG mainclass){
		plugin = mainclass;
		mainclass.getServer().getPluginManager().registerEvents(this, mainclass);
	}

	@EventHandler
	public void blockUpdate(EntityChangeBlockEvent e){
		if (Maps.RandomizeLocs.contains(e.getBlock().getLocation())){
			e.getBlock().setTypeIdAndData(33, (byte) 7, false);
		}
        if ((e.getEntityType() == EntityType.FALLING_BLOCK)) {
            Location loc = e.getEntity().getLocation();
            e.getEntity().remove();
            loc.getBlock().setType(Material.CHEST);
            Maps.chestSetup((Chest) loc.getBlock().getState());
        }
	}

	@SuppressWarnings("deprecation")
	@EventHandler
	public void OnRightClickTool (PlayerInteractEvent e) throws SQLException{
		final Player p = e.getPlayer();
		Action a = e.getAction();
		if (a == Action.RIGHT_CLICK_AIR || a == Action.RIGHT_CLICK_BLOCK || a == Action.LEFT_CLICK_AIR || a == Action.LEFT_CLICK_BLOCK){

			if (p.getItemInHand() != null){// User is holding an item of interest maybe?
				if (p.getItemInHand().hasItemMeta()){// User is getting interesting :P
					if (p.getItemInHand().getItemMeta().hasDisplayName()){// This person wont quit teasing us!
						String iih = ChatColor.stripColor(p.getItemInHand().getItemMeta().getDisplayName());// Item In Hand | Abbreviation
						if (iih.equalsIgnoreCase(ChatColor.stripColor(Message.Colorize(MessageHG.LocateCompassName)))){
							getNearestPlayer(p, true);
						}
						else if (iih.equalsIgnoreCase(ChatColor.stripColor(MessageHG.KitSelectorName))){
							if (MainHG.Game == Gamestatus.Pregame)
								PlayerCommands.OpenKitMenu(p);
							else
								Message.P(e.getPlayer(), MessageHG.CannotDoAtThisTime, true);
						}
						else if (iih.equalsIgnoreCase(ChatColor.stripColor(MessageHG.SpectateCompassName))){
							if (Teams.isSpectator(p) || Teams.isGamemaker(p)){
								try {
									PlayerCommands.OpenPlayersMenu(p);
								} catch (SQLException e1) {
									e1.printStackTrace();
								}
							}
							else{
								Message.P(p, MessageHG.CannotDoAtThisTime, true);
							}


						}
						else if (iih.equalsIgnoreCase(ChatColor.stripColor(MessageHG.GamemakerPanelName))){
							GamemakerPanelView(p);
						}
						else if (iih.equalsIgnoreCase(ChatColor.stripColor(MessageHG.BandaidName)) && MainHG.Gamemode != GameType.HardCore){
							if (p.getHealth() == p.getMaxHealth())
								Message.P(p, MessageHG.AtFullHealth, false);
							else if (p.getHealth() < p.getMaxHealth()){
								p.setHealth(p.getMaxHealth());
								if (p.getItemInHand().getAmount() > 1)
									p.getItemInHand().setAmount(p.getItemInHand().getAmount() - 1);
								else
									p.setItemInHand(new ItemStack(Material.AIR));
								p.updateInventory();
								Message.P(p, MessageHG.HealedToFull, false);
								p.setFireTicks(0);
								for (PotionEffect effect : p.getActivePotionEffects()){
									if (effect.getDuration() <= 20 * 60 * 10)
										p.removePotionEffect(effect.getType());
								}
							}
						}
					}
				}
			}
			if (a == Action.RIGHT_CLICK_BLOCK){
				final Block b = e.getClickedBlock();
				if (Maps.chestBlocks.containsKey(b.getLocation())){
					b.setTypeIdAndData(33, (byte) 7, false);
					e.setUseInteractedBlock(Result.DENY);
					e.setCancelled(true);
					plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
						public void run() {
							p.openInventory(Maps.chestBlocks.get(b.getLocation()));
						}
					}, 1L);




				}
			}
		}
		if (e.getItem() != null && e.getItem().getType() != null && e.getItem().getType() == Material.AIR && (a == Action.RIGHT_CLICK_AIR || a == Action.RIGHT_CLICK_BLOCK)){
			if (KitsHG.hasAbility(p, Abilities.AbilityExplosiveArrow)){
				if (!Abilities.isShootingSpecial(p)){
					Message.P(p, MessageHG.YouAreShootingSpecial, true);
					Abilities.ShootingSpecial.add(p.getName());
				}
			}
		}


	}


	private void GamemakerPanelView(Player p) {
		// I need sleep! PCE
	}


	static Player getNearestPlayer (Player p, boolean Messages){
		if (MainHG.gameMap != null && p.getWorld().getName() == MainHG.gameWorld.getName()){

			Player enemy = null;
			Boolean found = false;
			for (int i = 0; i < 300; i++) {
				List<Entity> entities = p.getNearbyEntities(i, 64.0D, i);
				for (Entity e : entities) {
					if ((!e.getType().equals(EntityType.PLAYER))|| Teams.isSpectator((Player) e) || Teams.isGamemaker((Player) e) || StatsHG.isInTeam(p, ((Player) e).getName()))
						continue;
					p.setCompassTarget(e.getLocation());
					enemy = (Player)e;
					if (Messages){
						//Double distance = p.getLocation().distance( e.getLocation());
						//DecimalFormat df = new DecimalFormat("#.#");
						Message.P(p, Message.Replacer(MessageHG.UserLocated, "" + enemy.getName(), "%user"), false);
					}
					found = true;
					break;
				}

				if (found){
					break;
				}
			}
			if (!found) {
				if (Messages)
					Message.P(p, MessageHG.NoCompassResult, false);
				p.setCompassTarget(MainHG.gameWorld.getSpawnLocation());
			}
			return enemy;
		}
		return null;
	}

	/*
	private void LocateCompass(Player p) {

			Boolean found = false;
			for (int i = 0; i < 300; i++) {
				List<Entity> entities = p.getNearbyEntities(i, 64.0D, i);
				for (Entity e : entities) {
					if ((!e.getType().equals(EntityType.PLAYER))|| StatusSwitch.isSpectator((Player) e) || StatusSwitch.isGamemaker((Player) e))
						continue;
					p.setCompassTarget(e.getLocation());
					Player enemy = (Player)e;
					Double distance = p.getLocation().distance( e.getLocation());
					DecimalFormat df = new DecimalFormat("#.#");
					Message.P(p, Message.replacer(Message.replacer(Message.UserLocated, "" + df.format(distance), "<distance>"), "" + enemy.getName(), "<user>"), false);
					found = true;
					break;
				}

				if (found){
					break;
				}
			}
			if (!found) {
				Message.P(p, Message.NoCompassResult, false);
				p.setCompassTarget(Main.World.getSpawnLocation());
			}
	}
	 */

	@EventHandler
	public void Chat (AsyncPlayerChatEvent e){
		Player p = e.getPlayer();

		if (!e.isCancelled() || !BmAPI.isMuted(e.getPlayer().getName())){
			PermissionUser user = PermissionsEx.getUser(e.getPlayer());
			String prefx = user.getPrefix();

			String msg = "";

			if (e.getPlayer().hasPermission("chat.color"))
				msg = ChatColor.translateAlternateColorCodes('&', e.getMessage().substring(0,1).toUpperCase() + e.getMessage().substring(1));
			else
				msg = e.getMessage().substring(0,1).toUpperCase() + e.getMessage().substring(1);


			String prefix = ChatColor.translateAlternateColorCodes('&', prefx + " ");
			String Rank = HungerGames.getRankHG(p, true);


			if (MainHG.Game != Gamestatus.Pregame){
				if (Teams.isSpectator(p)){
					msg = prefix + e.getPlayer().getDisplayName() + ": " + ChatColor.GRAY + msg;
					MessageHG.g(msg, Team.Spectator, false);
					MessageHG.g(msg, Team.Gamemaker, false);
					e.setCancelled(true);
				}
				else if (Teams.isGamemaker(p) || Teams.isGamer(p)){
					e.setFormat(Rank + prefix + p.getDisplayName() + ChatColor.GRAY + ": " + msg);		
				}
			}
			else
				e.setFormat(Rank + prefix + ChatColor.GRAY + e.getPlayer().getDisplayName() + ChatColor.GRAY + ": " + msg);		
		}
	}










}
