package kz.pvp.PvPGames.Events;

import java.sql.SQLException;
import java.util.logging.Logger;

import kz.PvP.PkzAPI.utilities.Message;
import kz.pvp.PvPGames.Enums.Gamestatus;
import kz.pvp.PvPGames.Main.MainHG;
import kz.pvp.PvPGames.Methods.Teams;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Monster;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.entity.EntityTargetEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.event.player.PlayerBedEnterEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerFishEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.server.ServerListPingEvent;
import org.spigotmc.event.entity.EntityMountEvent;


public class SpectatorListener implements Listener {
	public static MainHG plugin;

	public SpectatorListener (MainHG mainclass){
		plugin = mainclass;
		mainclass.getServer().getPluginManager().registerEvents(this, mainclass);
	}


	@EventHandler
	public void ServerPing (ServerListPingEvent e){
		StringBuilder motd = new StringBuilder();		
		if (MainHG.Game == Gamestatus.Pregame)
			motd.append("&7() &aLobby&7 ()");
		else if (MainHG.Game == Gamestatus.GracePeriod ||  MainHG.Game == Gamestatus.SpawnedPre)
			motd.append("&7() &aSpawned&7 ()");
		else if (MainHG.Game == Gamestatus.Game)
			motd.append("&7() &aGaming&7 ()");

		if (MainHG.gameMap != null)
			motd.append("= " + MainHG.gameMap.getMapName());
		else
			motd.append("=  None");

		motd.append(" = " + MainHG.Gamers.size() + "/" + plugin.getServer().getMaxPlayers());
		e.setMotd(Message.Colorize(motd.toString()));
	}

	@EventHandler
	public void PlayerQuit (PlayerQuitEvent e){
		if (Teams.isSpectator(e.getPlayer())){
			e.setQuitMessage(null);
		}
	}
	@EventHandler
	public void InventoryUse (InventoryDragEvent e){
		if (e.getWhoClicked() instanceof Player){
			if (Teams.isSpectator((Player) e.getWhoClicked())){
				e.setCancelled(true);
			}
		}
	}
	@EventHandler
	public void InventoryUse (PlayerDropItemEvent e){
		if (MainHG.Game == Gamestatus.SpawnedPre || MainHG.Game == Gamestatus.Pregame || Teams.isGamemaker(e.getPlayer()) || Teams.isSpectator(e.getPlayer())){
			e.setCancelled(true);
		}
	}
	@EventHandler
	public void InventoryPickUp (PlayerPickupItemEvent e){
		if (Teams.isSpectator(e.getPlayer())){
			e.setCancelled(true);
		}
	}
	@EventHandler
	public void BlockBreak (BlockBreakEvent e){
		if (Teams.isSpectator(e.getPlayer()))
			e.setCancelled(true);
		if ((MainHG.Game == Gamestatus.Pregame || MainHG.Game == Gamestatus.SpawnedPre) && !e.getPlayer().hasPermission("pvpgames.gamemaker.edit"))
			e.setCancelled(true);
	}
	@EventHandler
	public void BlockPlace (BlockPlaceEvent e){
		if (Teams.isSpectator(e.getPlayer()))
			e.setCancelled(true);
		if ((MainHG.Game == Gamestatus.Pregame || MainHG.Game == Gamestatus.SpawnedPre) && !e.getPlayer().hasPermission("pvpgames.gamemaker.edit"))
			e.setCancelled(true);
	}
	@EventHandler
	public void BlockDeath (PlayerDeathEvent e){
		if (e.getEntity() instanceof Player){
			Player p = (Player) e.getEntity();
			if (MainHG.Game == Gamestatus.SpawnedPre || MainHG.Game == Gamestatus.Pregame || Teams.isSpectator(p)){
				e.setDeathMessage(null);
			}
		}
	}
	@EventHandler
	public void BlockFishing (PlayerFishEvent e){
		if (Teams.isSpectator(e.getPlayer()))
			e.setCancelled(true);
		if (MainHG.Game == Gamestatus.Pregame || MainHG.Game == Gamestatus.SpawnedPre)
			e.setCancelled(true);
	}
	@EventHandler
	public void BlockBedEntry (PlayerBedEnterEvent e){
		if (Teams.isSpectator(e.getPlayer()))
			e.setCancelled(true);
		if (MainHG.Game == Gamestatus.Pregame || MainHG.Game == Gamestatus.SpawnedPre)
			e.setCancelled(true);
	}
	@EventHandler
	public void BlockMount (EntityMountEvent e){
		if (e.getEntity() instanceof Player){
			Player p = (Player) e.getEntity();
			if (Teams.isSpectator(p)){
				e.setCancelled(true);
			}
			if (MainHG.Game == Gamestatus.Pregame || MainHG.Game == Gamestatus.SpawnedPre)
				e.setCancelled(true);
		}
	}
	@EventHandler
	public void BlockAttackingAnyDmg (EntityDamageEvent e){
		if (e.getEntity() instanceof Player){
			Player p = (Player) e.getEntity();
			if (Teams.isSpectator(p)){
				e.setCancelled(true);
			}
			else if (MainHG.Game == Gamestatus.Pregame || MainHG.Game == Gamestatus.SpawnedPre)
				e.setCancelled(true);
		}
	}
	@EventHandler
	public void BlockAttackingEntDmg (EntityDamageByEntityEvent e){
		if (e.getEntity() instanceof Player){
			Player p = (Player) e.getEntity();
			if (Teams.isSpectator(p)){
				e.setCancelled(true);
			}
			else if (MainHG.Game == Gamestatus.Pregame || MainHG.Game == Gamestatus.SpawnedPre)
				e.setCancelled(true);
		}
		else if (e.getDamager() instanceof Player){
			Player p = (Player) e.getDamager();
			if (Teams.isSpectator(p)){
				e.setCancelled(true);
			}
			else if (MainHG.Game == Gamestatus.Pregame || MainHG.Game == Gamestatus.SpawnedPre)
				e.setCancelled(true);
		}
	}


	@EventHandler
	public void Block (EntityShootBowEvent e){
		if (e.getEntity() instanceof Player){
			if (Teams.isSpectator((Player) e.getEntity())){
				e.setCancelled(true);
			}
		}
		else if (MainHG.Game == Gamestatus.Pregame || MainHG.Game == Gamestatus.SpawnedPre)
			e.setCancelled(true);
	}

	@EventHandler
	public void Block (CreatureSpawnEvent e){
		if (MainHG.Game == Gamestatus.Pregame || MainHG.Game == Gamestatus.SpawnedPre){
			e.setCancelled(true);
		}
	}


	@EventHandler
	public void OnPlayerShoot (ProjectileLaunchEvent e){
		if (e.getEntity() instanceof Player){
			if (Teams.isSpectator((Player)e.getEntity())){
				e.setCancelled(true);
			}
		}
		else if (MainHG.Game == Gamestatus.Pregame || MainHG.Game == Gamestatus.SpawnedPre)
			e.setCancelled(true);
	}

	@EventHandler
	public void OnItemInteractEvent (PlayerInteractEvent e) throws SQLException{
		Player p = e.getPlayer();
		if (Teams.isSpectator(p) || (MainHG.Game == Gamestatus.Pregame && !Teams.isGamemaker(p))){
			e.setCancelled(true);
		}
	}

	@EventHandler
	public void OnEntityTarget (EntityTargetEvent e){
		Entity ent = e.getEntity();
		if (ent instanceof Monster){
			Monster monster = (Monster) ent;

			if (e.getTarget() instanceof Player){
				Player p = (Player)e.getTarget();
				if (Teams.isGamemaker(p) || Teams.isSpectator(p)){
					monster.setTarget(null);
				}
			}
		}
	}





}
