package kz.pvp.PvPGames.Events;

import java.util.logging.Logger;

import kz.PvP.PkzAPI.utilities.Message;
import kz.pvp.PvPGames.Enums.Gamestatus;
import kz.pvp.PvPGames.Main.MainHG;
import kz.pvp.PvPGames.Methods.Maps;
import kz.pvp.PvPGames.Methods.Teams;
import kz.pvp.PvPGames.Utilities.MessageHG;

import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

public class MoveListener implements Listener{
	public static MainHG plugin;
	static Logger log = Bukkit.getLogger();

	public MoveListener (MainHG mainclass){
		plugin = mainclass;
		mainclass.getServer().getPluginManager().registerEvents(this, mainclass);
	}


	@EventHandler
	public void OnMoveEvent (PlayerMoveEvent e){

		// Just a nice small if statement that should really make the task less laggiez
		if ((e.getFrom().getBlockX() != e.getTo().getBlockX()) || (e.getFrom().getBlockZ() != e.getTo().getBlockZ())){
			Player p = e.getPlayer();
			if (!Maps.isLocInBorder(e.getTo())){
				Message.P(p, MessageHG.ReachedEndOfMap, true);
				p.setVelocity(p.getVelocity().add(p.getLocation().toVector().subtract(e.getTo().toVector()).normalize().multiply(2)));
				p.playSound(p.getLocation(), Sound.FIREWORK_TWINKLE, 2f, 2f);
			}
			else if (MainHG.Game == Gamestatus.SpawnedPre){
				p.playSound(p.getLocation(), Sound.FIREWORK_TWINKLE, 2f, 2f);
				e.setTo(e.getFrom());
			}

			if (Teams.isGamer(p)){
				InteractListener.getNearestPlayer(p, false);
			}




		}


	}





}
