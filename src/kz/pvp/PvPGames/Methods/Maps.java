package kz.pvp.PvPGames.Methods;

import kz.PvP.PkzAPI.methods.ConvertTimings;
import kz.PvP.PkzAPI.utilities.Message;
import kz.pvp.PvPGames.Main.MainHG;
import kz.pvp.PvPGames.Utilities.ChestSpawn;

import org.bukkit.*;
import org.bukkit.block.BlockState;
import org.bukkit.block.Chest;
import org.bukkit.block.DoubleChest;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryView;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.MaterialData;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Maps implements Listener {
	public static MainHG plugin;
	public static List<Location> RandomizeLocs = new ArrayList<Location>();
	public static List<String> RandomizedChunks = new ArrayList<String>();
	public static HashMap<Location, Inventory> chestBlocks = new HashMap<Location, Inventory>();
	
	public Maps(MainHG mainclass) {
		plugin = mainclass;
	}

	public static void unloadMap(String mapname) {
		if(plugin.getServer().unloadWorld(mapname, false)) {
			Message.C("Unloaded; " + mapname);
		} else {
			Message.C("Could not unload; " + mapname);
		}
	}


	//Loading maps (MUST BE CALLED AFTER UNLOAD MAPS TO FINISH THE ROLLBACK PROCESS)
	private static void copyDirectory(File sourceLocation, File targetLocation)
			throws IOException {
		if (sourceLocation.isDirectory()) {
			if (!targetLocation.exists()) {
				targetLocation.mkdir();
			}
			String[] children = sourceLocation.list();
			for (int i = 0; i < children.length; i++) {
				copyDirectory(new File(sourceLocation, children[i]), new File(
						targetLocation, children[i]));
			}
		} else {
			InputStream in = new FileInputStream(sourceLocation);
			OutputStream out = new FileOutputStream(targetLocation);
			byte[] buf = new byte[1024];
			int len;
			while ((len = in.read(buf)) > 0) {
				out.write(buf, 0, len);
			}
			in.close();
			out.close();
		}
	}
	public static void copy(InputStream in, File file) {
		try {
			OutputStream out = new FileOutputStream(file);
			byte[] buf = new byte[1024];
			int len;
			while ((len = in.read(buf)) > 0) {
				out.write(buf, 0, len);
			}
			out.close();
			in.close();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	public static boolean deleteDirectory(File path) {
		if( path.exists() ) {
			File files[] = path.listFiles();
			for (int i=0; i<files.length; i++) {
				if(files[i].isDirectory()) {
					deleteDirectory(files[i]);
				} else {
					files[i].delete();
				}
				//end else
			}
		}
		return( path.delete() );
	}
	public static World createWorld(final String worldName) {
		WorldCreator world = new WorldCreator(worldName).type(WorldType.NORMAL);
		World newWorld = world.createWorld();
		newWorld.setDifficulty(Difficulty.NORMAL);
		newWorld.setAutoSave(false);
		newWorld.setFullTime(0);
		newWorld.setKeepSpawnInMemory(true);
		newWorld.setPVP(true);
		newWorld.setStorm(false);
		newWorld.setThundering(false);
		Message.C("Created new world; " + newWorld.getName());
		return newWorld;
	}

	public static World loadMapFiles(final String mapname, String originalMap) {


		if (plugin.getServer().getWorld(originalMap) != null) {
			World world = plugin.getServer().getWorld(originalMap);

			if(plugin.getServer().unloadWorld(originalMap, false)) {
				Message.C(originalMap + " Unloaded.");
				deleteDirectory(world.getWorldFolder());
				loadMapFiles(mapname, originalMap);
			} else
				Message.C("Failed to unload map '" + world.getName() + "'");
		} else {
			Message.C("Copying map '" + mapname + "'");
			try {
				copyDirectory(new File(plugin.getDataFolder(), mapname), new File(originalMap));
				Message.C("Copied map '" + mapname + "'");
				return createWorld(originalMap);
			} catch (IOException e) {
				Message.C("Error: " + e.toString());
			}
		}
		return null;
	}


	public static Location getRandomLocation(){
		if (MainHG.gameMap != null){
			Location startFrom = MainHG.gameWorld.getSpawnLocation();
			Location loc;
			do{
				loc = startFrom.clone();
				Integer border = MainHG.gameMap.getBorder();
				loc.add(ConvertTimings.randomInt(-1 * border, border),
						60,
						(ConvertTimings.randomInt(-1 * border, border)));
				int newY = MainHG.gameWorld.getHighestBlockYAt(loc);
				loc.setY(newY);
			} while(!isLocInBorder(loc) && loc.add(0, -1, 0).getBlock().getType() != Material.AIR);// We don't want floating spawned feasts, chests and sort of stuffz
			return loc;
		}
		return null;
	}

	public static void RandomizeChests() {
		if (MainHG.gameWorld != null)
			for (Chunk c : MainHG.gameWorld.getLoadedChunks()){
				RandomizeChunk(c);
			}
	}
	public static boolean isLocInBorder(Location loc){
		if (MainHG.gameMap != null && loc.getWorld().getName() != MainHG.gameWorld.getName()){
			Integer border = MainHG.gameMap.getBorder();
			Location spawn = MainHG.gameWorld.getSpawnLocation();
			if (spawn.getWorld() == loc.getWorld())
				return (spawn.distance(loc) < border);
			else
				return true;
		}
		else
			return true;
	}
	public static void RandomizeChunk(Chunk c) {
		if (MainHG.gameMap != null && MainHG.gameWorld != null){
			if (!RandomizedChunks.contains(c.getX() + "|" + c.getZ())){
				for (BlockState b : c.getTileEntities()){
					if (b instanceof Chest || b instanceof DoubleChest){
						if (!RandomizeLocs.contains(b.getBlock().getLocation())){
							chestSetup(b);
							RandomizeLocs.add(b.getLocation());
						}
					}
				}
				RandomizedChunks.add(c.getX() + "|" + c.getZ());
			}
		}
	}

	public static void chestSetup(BlockState b) {
		Inventory inventory = Generation.spawnItems((Chest)b);
		ItemStack[] items = inventory.getContents();
		Chest chest = (Chest) b;
		chest.getInventory().clear();
		b.getLocation().getBlock().setTypeIdAndData(33, (byte) 7, false);
		int size = 27;
		if (items.length > 27)
			size = 54;
		
		
		Inventory inv = Bukkit.getServer().createInventory(null, size, "Supply Crate");
		Message.C("Size " + size + " and needed" + items.length);
		inv.setContents(items.clone());
		chestBlocks.put(b.getLocation(), inv);
		inventory.clear();
	}
}