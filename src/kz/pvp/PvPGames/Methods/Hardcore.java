package kz.pvp.PvPGames.Methods;

import kz.PvP.PkzAPI.methods.ConvertTimings;
import kz.PvP.PkzAPI.utilities.Message;
import kz.pvp.PvPGames.Enums.GameType;
import kz.pvp.PvPGames.Main.MainHG;
import kz.pvp.PvPGames.Utilities.MessageHG;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityRegainHealthEvent;
import org.bukkit.event.entity.EntityRegainHealthEvent.RegainReason;

import java.util.HashMap;
import java.util.logging.Logger;

public class Hardcore implements Listener{
	public static MainHG plugin;
	static Logger log = Bukkit.getLogger();
	public static HashMap<String, Integer> Vote = new HashMap<String, Integer>();// Vote Collector | 0 = No | 1 = Yes

	public Hardcore (MainHG mainclass){
		plugin = mainclass;
		mainclass.getServer().getPluginManager().registerEvents(this, mainclass);
	}
	
	
	private static void hardcoreApply() {
		  Message.G(MessageHG.HardcoreModeBegan, true);
		for (String player : MainHG.Gamers){
            Player p = Bukkit.getPlayer(player);
			p.setHealth(20.0);
			p.setGameMode(GameMode.SURVIVAL);
			p.setFoodLevel(20);
			MainHG.Gamemode = GameType.HardCore;
		}
	}
	
	
	
	
	
	
	
	
	
	@EventHandler
	public void OnPlayerRegenerate (EntityRegainHealthEvent e){
		if (e.getEntity() instanceof Player){
			Player p = (Player) e.getEntity();
			if (MainHG.Gamemode == GameType.HardCore){
				if (Teams.isGamer(p) && e.getRegainReason() == RegainReason.EATING){
					e.setCancelled(true);
				}
				else
					e.setCancelled(true);
			}
		}
	}
	
	
}
