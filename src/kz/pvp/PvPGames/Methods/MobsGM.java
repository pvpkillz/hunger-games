package kz.pvp.PvPGames.Methods;

import kz.PvP.PkzAPI.methods.ConvertTimings;
import kz.pvp.PvPGames.Main.MainHG;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.CreatureType;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashMap;
import java.util.Random;
import java.util.logging.Logger;

public class MobsGM{
	public static MainHG plugin;
	static Logger log = Bukkit.getLogger();
	public static HashMap<String, Boolean> MobEnable = new HashMap<String, Boolean>();// Gamers in the server / Hashmap incase we want to convert to multiple arenas
	public static HashMap<String, Integer> MobTasks = new HashMap<String, Integer>();// Gamers in the server / Hashmap incase we want to convert to multiple arenas

	public MobsGM (MainHG mainclass){
		plugin = mainclass;
	}
	
	
	
	public static boolean ToggleMobsAroundPlayers (final Player p){
		if (p != null){
			if (!MobEnable.containsKey(p.getName()))
				MobEnable.put(p.getName(), false);
		}
		
		
		if (p != null){// Only 1 player will get the 'Mob-Effect' toggled
			
			
			if (MobEnable.get(p.getName())){
				MobEnable.put(p.getName(), false);
				Bukkit.getScheduler().cancelTask(MobTasks.get(p.getName()));
			}
			else{
				Random r = new Random();
				int Low = 30;// Min 30 seconds....
				int High = MainHG.MaxMobEffectLength;
				final int ranSeconds = r.nextInt(High-Low) + Low;
				
				
				int MobAttacks = Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(plugin, new Runnable() {
				    @SuppressWarnings("deprecation")
					@Override  
				    public void run() {
						double random = Math.random();// Random double between 0 and 1
				    	if (random <= 0.5){
				    		// Spawn a mob nearby
				    		Location loc = p.getLocation();
				    		
				    		Location loc1 = loc.add(ConvertTimings.randomInt(-10, 10), 2, ConvertTimings.randomInt(-10, 10));
				    		Location loc2 = loc.add(ConvertTimings.randomInt(-10, 10), 2, ConvertTimings.randomInt(-10, 10));

				    		int Random = ConvertTimings.randomInt(0, 5);
				    		CreatureType CR = null;
							if (Random == 1)
					    	CR = CreatureType.ZOMBIE;
							else if (Random == 2)
					    	CR = CreatureType.SKELETON;
							else if (Random == 3)
				    		CR = CreatureType.CREEPER;
							else if (Random == 4)
					    	CR = CreatureType.SPIDER;
							else if (Random == 5)
					    	CR = CreatureType.CAVE_SPIDER;
							
				    		int Random1 = ConvertTimings.randomInt(0, 5);
				    		CreatureType CR1 = null;
							if (Random1 == 1)
					    	CR1 = CreatureType.ZOMBIE;
							else if (Random1 == 2)
					    	CR1 = CreatureType.SKELETON;
							else if (Random1 == 3)
				    		CR1 = CreatureType.CREEPER;
							else if (Random == 4)
					    	CR1 = CreatureType.SPIDER;
							else if (Random1 == 5)
					    	CR1 = CreatureType.CAVE_SPIDER;
							
							
							
				    		p.getWorld().spawnCreature(loc1, CR1);
				    		p.getWorld().spawnCreature(loc2, CR);
				    	}
				    	else if (random <= 1){
				    		// Nothing
				    	}
				    	
				    }
				}, ranSeconds * 20L, ranSeconds * 20L);
				MobTasks.put(p.getName(), MobAttacks);
			}
			
			
			
			
		}
		else{// All players get the 'Mob-Effect' toggled
			for (String gamer : MainHG.Gamers){
				ToggleMobsAroundPlayers(Bukkit.getPlayer(gamer));// We do a recursive method, but run it for each player...
			}
		}
		
		
		
		
		
		
		
		
		
		
		return false;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
