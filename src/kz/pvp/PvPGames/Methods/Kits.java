package kz.pvp.PvPGames.Methods;

import kz.PvP.PkzAPI.methods.ConvertTimings;
import kz.PvP.PkzAPI.methods.Scoreboards;
import kz.PvP.PkzAPI.utilities.Stats;
import kz.pvp.PvPGames.Enums.Gamestatus;
import kz.pvp.PvPGames.Enums.mapInfo;
import kz.pvp.PvPGames.Main.MainHG;
import kz.pvp.PvPGames.Utilities.FilesHG;
import kz.pvp.PvPGames.Utilities.KitsHG;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Logger;

public class Kits implements Listener{
	public static MainHG plugin;
	static Logger log = Bukkit.getLogger();

	public Kits (MainHG mainclass){
		plugin = mainclass;
		mainclass.getServer().getPluginManager().registerEvents(this, mainclass);
	}


	private static void randomKits(Player p) throws SQLException {
		String KitName = KitsHG.kitList.get(ConvertTimings.randomInt(0, KitsHG.kitList.size() - 1)).getKitName();
		while (!KitsHG.KitChoice.containsKey(p.getName())){
			KitsHG.choose(p, KitName.toLowerCase(), true);
			break;
		}
	}


	public static void ScoreBoard(Player pl) {

		if (MainHG.Game == Gamestatus.Pregame){
			ArrayList<String> mapList = new ArrayList<String>();
			if (FilesHG.worldconf.getCustomConfig().contains("RandomMap") && FilesHG.worldconf.getCustomConfig().getBoolean("RandomMap") == true) {

				mapList.add("&aRandom Terrain//0");
			} else
				for (mapInfo map : MainHG.maps)
					mapList.add(map.getMapName() + "//" + map.getIndex());
			if (Scoreboards.scoreboardsList.containsKey(pl.getName()))
				Scoreboards.updateScoreboardEntries(pl, mapList, "&6Vote for a map with &b&l/map #");
		}
		else{

			String timeLeft = ConvertTimings.getTime(MainHG.TimeLeft);
			String status = "";
			if (MainHG.Game == Gamestatus.Game){
				timeLeft = ConvertTimings.getTime(MainHG.TimeLeft - (MainHG.MaxGameLength - MainHG.FinalBattleTime));
				status = "&c&lDeathmatch in";
			}
			else if (MainHG.Game == Gamestatus.GracePeriod)
				status = "&b&lGrace Period | ";
			else if (MainHG.Game == Gamestatus.SpawnedPre)
				status = "&b&lCountdown | ";
			else if (MainHG.Game == Gamestatus.Deathmatch)
				status = "&c&lDeathmatch |";

				String TopTitle = "&7" + status + "&7 &b" + timeLeft;
			if (TopTitle.length() >= 32){
				TopTitle += " | ";
			}


			ArrayList<String> scoreboardList = new ArrayList<String>();
			scoreboardList.add("Tributes//"+ Teams.getGamers().size());
			scoreboardList.add("Spectators//" + Teams.getSpectators().size());
			scoreboardList.add("&aKills//"+Stats.getKillstreak(pl.getName()));
			scoreboardList.add("&m----------//-1");
			scoreboardList.add("&aPvP.Kz//-2");
			if (Scoreboards.scoreboardsList.containsKey(pl.getName()))
				Scoreboards.updateScoreboardEntries(pl, scoreboardList, TopTitle);
		}
	}
}
