package kz.pvp.PvPGames.Methods;

import kz.PvP.PkzAPI.methods.PlayersInfo;
import kz.PvP.PkzAPI.methods.itemModifications;
import kz.PvP.PkzAPI.utilities.IconMenu;
import kz.PvP.PkzAPI.utilities.Message;
import kz.PvP.PkzAPI.utilities.Mysql;
import kz.pvp.PvPGames.Commands.PlayerCommands;
import kz.pvp.PvPGames.Enums.Gamestatus;
import kz.pvp.PvPGames.Main.MainHG;
import kz.pvp.PvPGames.Utilities.FilesHG;
import kz.pvp.PvPGames.Utilities.KitsHG;
import kz.pvp.PvPGames.Utilities.MessageHG;
import kz.pvp.PvPGames.Utilities.StatsHG;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class Sponsor {
    public static MainHG plugin;

    public Sponsor (MainHG mainclass){
        plugin = mainclass;
    }

    public static void purchaseItemForGamer(Player sponsor, Player gamer, String name) throws SQLException {
        String[] nameOfItem = ChatColor.stripColor(name).replace(" | Cost: ", "-").split("-");// We clean out the dang thing.. :P
        int price = Integer.parseInt(nameOfItem[1]);
        String nameItemCode = nameOfItem[0];
        String ItemName = FilesHG.sponsorconf.getCustomConfig().getString(getCurrentTier() + "." + nameItemCode + ".Item");

        ItemStack is = itemModifications.getItemInfo(ItemName, null);
        if (Mysql.modifyTokens(sponsor.getName(), -price, true)){
            gamer.getInventory().addItem(is);
            Message.P(gamer, Message.Replacer(MessageHG.SponsoredBy, sponsor.getName(), "%sponsor"), false);
            Message.P(sponsor, Message.Replacer(MessageHG.SponsoredGamer, gamer.getName(), "%gamer"), false);
        }
    }



    public static void OpenSponsorMenuPlayers(final Player p) throws SQLException {
        Integer invsize = 54;
        if (PlayerCommands.menus.containsKey(p.getName())){
            PlayerCommands.menus.get(p.getName()).destroy();
            PlayerCommands.menus.remove(p.getName());
        }
        IconMenu menu = new IconMenu(ChatColor.GOLD + "Which tribute to sponsor?", p.getName(), invsize, new IconMenu.OptionClickEventHandler() {
            public void onOptionClick(final IconMenu.OptionClickEvent event) {

                plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
                    public void run() {
                        OpenSponsorItemsList(p, PlayersInfo.getPlayer(ChatColor.stripColor(event.getName())));
                    }
                }, 2L);

                event.setWillClose(true);
                event.setWillDestroy(false);
            }

        }, plugin);

        menu = PlayerCommands.getPlayerContainer(menu);
        menu.open(p);
        PlayerCommands.menus.put(p.getName(), menu);
    }


    private static void OpenSponsorItemsList(final Player p, final Player playerExact) {
        Integer invsize = 54;
        if (PlayerCommands.menus.containsKey(p.getName())){
            PlayerCommands.menus.get(p.getName()).destroy();
            PlayerCommands.menus.remove(p.getName());
        }
        String tier = getCurrentTier();
        if (tier != null){



            IconMenu menu = new IconMenu(ChatColor.GOLD + "Sponsoring " + playerExact.getName(), p.getName(), invsize, new IconMenu.OptionClickEventHandler() {
                public void onOptionClick(IconMenu.OptionClickEvent event) throws SQLException {
                    Sponsor.purchaseItemForGamer(p, playerExact, ChatColor.stripColor(event.getName()));
                    event.setWillClose(true);
                    event.setWillDestroy(false);
                }
            }, plugin);
            int pos = 0;
            int maxpos = 54;




            for (String itemCode : FilesHG.sponsorconf.getCustomConfig().getConfigurationSection(tier).getKeys(false)){
                if (pos == maxpos)
                    break;
                ArrayList<String> container = new ArrayList<String>();
                ItemStack is = itemModifications.getItemInfo(FilesHG.sponsorconf.getCustomConfig().getString(tier + "." + itemCode + ".Item"), null);

                int price = FilesHG.sponsorconf.getCustomConfig().getInt(tier + "." + itemCode + ".Price");

                List<String> itemLore = FilesHG.sponsorconf.getCustomConfig().getStringList(tier + "." + itemCode + ".Description");
                for (String lore : itemLore){
                    container.add(ChatColor.translateAlternateColorCodes('&', lore));
                }


                String[] info = new String[container.size()];
                info = container.toArray(info);

                menu.setOption(pos, is, ChatColor.RED + "" + itemCode + " | Cost: " + price, info);
                pos++;
                container.clear();
            }
            menu.open(p);
            PlayerCommands.menus.put(p.getName(), menu);
        } else {
            System.out.println(tier);
        }
    }

    private static String getCurrentTier() {
        String tier = null;
        if (MainHG.Game == Gamestatus.Game) {
            if (MainHG.TimeLeft >= ((MainHG.MaxGameLength/4) * 3)) {
                tier = "Tier4";
            } else if (MainHG.TimeLeft >= ((MainHG.MaxGameLength/4) * 2)) {
                tier = "Tier3";
            } else if (MainHG.TimeLeft >= ((MainHG.MaxGameLength/4))) {
                tier = "Tier2";
            } else if (MainHG.TimeLeft >= ((MainHG.MaxGameLength/4))) {
                tier = "Tier1";
            }
        }
        return tier;
    }
}
