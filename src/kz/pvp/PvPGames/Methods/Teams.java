package kz.pvp.PvPGames.Methods;

import kz.PvP.PkzAPI.Main;
import kz.PvP.PkzAPI.methods.ConvertTimings;
import kz.PvP.PkzAPI.methods.PlayersInfo;
import kz.PvP.PkzAPI.methods.itemModifications;
import kz.PvP.PkzAPI.utilities.Message;
import kz.PvP.PkzAPI.utilities.Mysql;
import kz.PvP.PkzAPI.utilities.Stats;
import kz.pvp.PvPGames.Enums.Gamestatus;
import kz.pvp.PvPGames.Enums.Team;
import kz.pvp.PvPGames.Main.MainHG;
import kz.pvp.PvPGames.Utilities.KitsHG;
import kz.pvp.PvPGames.Utilities.MessageHG;
import kz.pvp.PvPGames.Utilities.StatsHG;

import org.bukkit.*;
import org.bukkit.entity.Player;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;



public class Teams {


	public static MainHG plugin;

	public Teams (MainHG mainclass){
		plugin = mainclass;
	}


	public static void addPlayer (Player p) throws SQLException{
		String Name = p.getName();
		if (!isGamer(p)){
			p.setGameMode(GameMode.SURVIVAL);
			if (Name.length() >= 15)
				Name = Name.substring(0, 14);


			if (StatsHG.lastRoundWin(p))
				p.setPlayerListName(ChatColor.BLUE + Name);
			else
				p.setPlayerListName(Name);

			if (isGamemaker(p))
				MainHG.GameMakers.remove(p.getName());
			else if (isSpectator(p))
				MainHG.Spectators.remove(p.getName());

			MainHG.Gamers.add(p.getName());
			p.setHealth(20.0);
			p.setFoodLevel(20);
			clearFully(p);
			updateVanish(p);
			adjustName(p);

			String kitName = KitsHG.getDefaultKit(p);
			if (kitName != null && KitsHG.canUserAccess(kitName, p))
				KitsHG.choose(p, kitName, false);
			else if (kitName != null && !KitsHG.canUserAccess(kitName, p))
				Message.P(p, Message.Replacer(MessageHG.LostAccessToDefaultKit, kitName, "%kit"), true);

			Location loc = MainHG.lobbyWorld.getSpawnLocation();
			p.teleport(loc);
		}
	}
	private static void setNewTime(int i) {
		Message.G(Message.Replacer(Message.Replacer(MessageHG.EarlyStartPlayers, "" + MainHG.Gamers.size(), "%amount"), "" + 60, "%time"), true);
		MainHG.TimeLeft = i;
	}


	public static void addGamemakers (Player p){
		String Name = p.getName();
		if (Name.length() >= 15)
			Name = Name.substring(0, 14);
		p.setPlayerListName(ChatColor.GOLD + Name);


		if (!isGamemaker(p)){
			if (isGamer(p))
				MainHG.Gamers.remove(Name);
			else if (isSpectator(p))
				MainHG.Spectators.remove(Name);

			Message.P(p, MessageHG.YourAreGamemaking, true);
			MainHG.GameMakers.add(Name);
			p.setGameMode(GameMode.CREATIVE);
			p.setHealth(20.0);
			p.setFoodLevel(20);
			clearFully(p);
			updateVanish(p);
			adjustName(p);
			giveTools(p, Team.Gamemaker);
			Location loc = MainHG.lobbyWorld.getSpawnLocation();
			p.teleport(loc);
		}
		else
			Message.P(p, Message.Replacer(MessageHG.AlreadyAType, "Gamemaker", "%type"), true);
	}
	public static void addSpectators (Player p){
		if (!isSpectator(p)){
			if (isGamer(p))
				MainHG.Gamers.remove(p.getName());
			else if (isGamemaker(p))
				MainHG.GameMakers.remove(p.getName());



			MainHG.Spectators.add(p.getName());
			String Name = p.getName();
			if (Name.length() >= 15)
				Name = Name.substring(0, 14);
			p.setHealth(20.0);
			p.setFoodLevel(20);
			p.setPlayerListName(ChatColor.GRAY + Name);
			updateVanish(p);
			p.setAllowFlight(true);
			p.setFlying(true);
			Location loc = MainHG.gameWorld.getSpawnLocation();
			int AddX = ConvertTimings.randomInt(-8, 8);
			int AddZ = ConvertTimings.randomInt(-8, 8);
			loc.add(AddX, 6, AddZ);
			p.teleport(loc);
			Message.P(p, MessageHG.YourAreSpectating, true);
			clearFully(p);
			adjustName(p);
			giveTools(p, Team.Spectator);
		}
	}


	public static Set<Player> getGamers(){
		Set<Player> players = new CopyOnWriteArraySet<Player>();
		for (String player : MainHG.Gamers) {
			players.add(Bukkit.getPlayer(player));
		}

		return players;
	}
	public static Set<Player> getGamemakers(){
		Set<Player> players = new CopyOnWriteArraySet<Player>();
		for (String player : MainHG.GameMakers) {
			players.add(Bukkit.getPlayer(player));
		}

		return players;
	}
	public static Set<Player> getSpectators(){
		Set<Player> players = new CopyOnWriteArraySet<Player>();
		for (String player : MainHG.Spectators) {
			players.add(Bukkit.getPlayer(player));
		}

		return players;
	}


	private static void updateVanish(Player p) {
		for (Player pl : Bukkit.getOnlinePlayers()){
			if (isSpectator(p) || isGamemaker(p))
				pl.hidePlayer(p);
			else
				pl.showPlayer(p);
		}
	}


	private static void clearFully(Player p) {
		p.getInventory().clear();
		p.getInventory().setHelmet(null);
		p.getInventory().setChestplate(null);
		p.getInventory().setLeggings(null);
		p.getInventory().setBoots(null);
	}


	public static void adjustName(Player p) {
		String name = "";
		if (isGamer(p))
			name = ChatColor.GRAY + "";
		else if (isGamemaker(p))
			name = ChatColor.GOLD + "*GM* ";
		else if (isSpectator(p))
			name = ChatColor.DARK_GRAY + "*Spec* ";

		if (MainHG.KitPrefix && isGamer(p) && KitsHG.getPlayerKit(p.getName()) != null)
			name = name + ChatColor.DARK_GRAY + "[" + Message.CleanCapitalize(KitsHG.getPlayerKit(p.getName()).getKitName()) + "]" + ChatColor.GRAY + " ";

		name = name + p.getName();
		p.setDisplayName(name);
	}





	public static void giveTools(Player p, Team team) {
		if (team == Team.Spectator){
			itemModifications.giveItem(p, MessageHG.SpectateCompassName, Material.COMPASS, 0, MessageHG.SpectateCompassDesc, true);
		}
		else if (team == Team.Gamemaker){
			itemModifications.giveItem(p, MessageHG.SpectateCompassName, Material.COMPASS, 1000, MessageHG.SpectateCompassDesc, false);
			itemModifications.giveItem(p, MessageHG.GamemakerPanelName, Material.COOKIE, 1000, MessageHG.GamemakerPanelDesc, false);
		}
		else if (team == Team.Player){
			if (MainHG.Game != Gamestatus.Pregame)
				itemModifications.giveItem(p, MessageHG.LocateCompassName, Material.COMPASS, 1000, MessageHG.LocateCompassDesc, true);
			else
				itemModifications.giveItem(p, MessageHG.KitSelectorName, Material.PAPER, 1000, MessageHG.KitSelectorDesc, true);
		}
	}


	public static boolean isSpectator(Player p) {
		return MainHG.Spectators.contains(p.getName());
	}
	public static boolean isGamemaker(Player p) {
		return MainHG.GameMakers.contains(p.getName());
	}
	public static boolean isGamer(Player p) {
		return MainHG.Gamers.contains(p.getName());
	}


	public static void eliminateUser(Player p, Player dam, boolean Quit) throws SQLException {
		if (isGamer(p))
			MainHG.Gamers.remove(p.getName());


		if (MainHG.Gamers.size() == 1){
			// We have a winner - Lets end dis thang :D
			
			Winner(Teams.getGamers().iterator().next());
		}
		else if (MainHG.Gamers.size() == 0){
			// No winner?? - Safeguard Derpity derp derp
			// Lets just shutdown the server... To avoid issues.
			plugin.getServer().shutdown();
		}
		else if (MainHG.Gamers.size() >= 2 && Quit == false){
			// Not a winner, but they tried. Gotta give them some sort of recognition.. :P
			Message.G(Message.Replacer(MessageHG.TributesRemain, "" + Teams.getGamers().size(), "%remain"), false);
		}
		Location loc = p.getLocation().add(0, 10, 0);
		loc.getWorld().strikeLightningEffect(loc);

		if (dam != null){
			if (MainHG.SpectatorKickEnable){
				p.kickPlayer(Message.Replacer(MessageHG.EliminatedReason, dam.getName(), "%killer"));
			}
			else{
				Message.P(p, Message.Replacer(MessageHG.EliminatedReason, dam.getName(), "%killer"), false);
				addSpectators(p);
			}
		}
		else{
			if (MainHG.SpectatorKickEnable){
			}
			else{
				addSpectators(p);
			}
		}
	}


	private static void Winner(Player roundWinner) throws SQLException {
		if (MainHG.Game != Gamestatus.Finished){
			MainHG.Game = Gamestatus.Finished;
			if (roundWinner != null){
				Message.G(Message.Replacer(MessageHG.WehaveaWinner, roundWinner.getName(), "%winner"), true);
				Message.P(roundWinner, MessageHG.YouHaveWonThisRound, true);
				//StatsHG.modifyUserCns(dam.getName(), MainHG.PointsPerWin);

				roundWinner.setAllowFlight(true);
				roundWinner.setFlying(true);


				if (Main.UseMySQL){
					Long Time = System.currentTimeMillis()/1000;
					Mysql.PS.getSecureQuery("UPDATE HungerGames_Rounds SET End = ?, Winner = ? WHERE ID = ?", ""+Time, ""+Mysql.getUserID(roundWinner.getName()), ""+MainHG.GameID);
				}




				final Location loc = roundWinner.getLocation();
				Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(plugin, new Runnable() {
					@Override  
					public void run() {
						Locators.spawnFirework(loc.add(ConvertTimings.randomInt(-10, 10), 0, ConvertTimings.randomInt(-10, 10)), true);
						Locators.spawnFirework(loc.add(ConvertTimings.randomInt(-10, 10), 0, ConvertTimings.randomInt(-10, 10)), true);
						Locators.spawnFirework(loc.add(ConvertTimings.randomInt(-10, 10), 0, ConvertTimings.randomInt(-10, 10)), true);
					}
				}, 20L, 20L);



			}
			else{



			}
			Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
				public void run() {
					Bukkit.shutdown();
				}
			}, 20L * 10);
		}
	}




	public static void checkStatus() throws SQLException {
		if (MainHG.Game != Gamestatus.Pregame){
			if (MainHG.Gamers.size() == 0){
				// No winner?? - Safeguard Derpity derp derp
				// Lets just shutdown the server... To avoid issues.
				Bukkit.shutdown();
			}
		}
	}
	public static Player getPlayerFromSome(String partname) {
		List<Player> matches = Bukkit.matchPlayer(partname);
		if (matches.size() == 0)
			return null;
		else 
			return matches.get(0);
	}

	public static void deleteUser(Player p) {
		// Delete the user like they never existed... Shh, they did.
		if (MainHG.Gamers.contains(p.getName()))
			MainHG.Gamers.remove(p.getName());
		if (MainHG.GameMakers.contains(p.getName()))
			MainHG.GameMakers.remove(p.getName());
		if (MainHG.Spectators.contains(p.getName()))
			MainHG.Spectators.remove(p.getName());
		
		if (StatsHG.wonLastRound.containsKey(p.getUniqueId()))
			StatsHG.wonLastRound.remove(p.getUniqueId());
		if (KitsHG.KitChoice.containsKey(p))
			KitsHG.KitChoice.remove(p);

		if (Hardcore.Vote.containsKey(p.getName()))
			Hardcore.Vote.remove(p.getName());

		if (StatsHG.Assists.containsKey(p.getName()))
			StatsHG.Assists.remove(p.getName());

		if (StatsHG.teams.containsKey(p.getName()))
			StatsHG.teams.remove(p.getName());
	}
}
