package kz.pvp.PvPGames.Methods;

import java.util.logging.Logger;

import kz.pvp.PvPGames.Main.MainHG;

import org.bukkit.Bukkit;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;

import pgDev.bukkit.DisguiseCraft.DisguiseCraft;
import pgDev.bukkit.DisguiseCraft.api.DisguiseCraftAPI;
import pgDev.bukkit.DisguiseCraft.disguise.Disguise;
import pgDev.bukkit.DisguiseCraft.disguise.DisguiseType;

public class Disguises {
    public static MainHG plugin;

    public Disguises (MainHG mainclass){
        plugin = mainclass;
        setupDisguiseCraft();
    }

    public static DisguiseCraftAPI dcAPI;

    public void setupDisguiseCraft() {
        dcAPI = DisguiseCraft.getAPI();
    }

    public static void disguiseAs(Player p, EntityType entity) {
        if (MainHG.DisguiseCraftEnable){
            Disguise disguise = new Disguise(dcAPI.newEntityID(), getDisguiseType(entity));

            if (!dcAPI.isDisguised(p))
                dcAPI.disguisePlayer(p, disguise);
            else
                dcAPI.changePlayerDisguise(p, disguise);
        }
    }

    public static void unDisguise(Player p) {
        if (MainHG.DisguiseCraftEnable) {
            if (dcAPI.isDisguised(p))
                dcAPI.undisguisePlayer(p);
        }
    }

    public static DisguiseType getDisguiseType(EntityType entity) {

        if (entity == EntityType.BLAZE) {
            return DisguiseType.Blaze;
        }
        else if (entity == EntityType.CAVE_SPIDER) {
            return DisguiseType.CaveSpider;
        }
        else if (entity == EntityType.CHICKEN) {
            return DisguiseType.Chicken;
        }
        else if (entity == EntityType.COW) {
            return DisguiseType.Cow;
        }
        else if (entity == EntityType.CREEPER) {
            return DisguiseType.Creeper;
        }
        else if (entity == EntityType.ENDER_DRAGON) {
            return DisguiseType.EnderDragon;
        }
        else if (entity == EntityType.ENDERMAN) {
            return DisguiseType.Enderman;
        }
        else if (entity == EntityType.GHAST) {
            return DisguiseType.Ghast;
        }
        else if (entity == EntityType.GIANT) {
            return DisguiseType.Giant;
        }
        else if (entity == EntityType.IRON_GOLEM) {
            return DisguiseType.IronGolem;
        }
        else if (entity == EntityType.MAGMA_CUBE) {
            return DisguiseType.MagmaCube;
        }
        else if (entity == EntityType.MUSHROOM_COW) {
            return DisguiseType.MushroomCow;
        }
        else if (entity == EntityType.OCELOT) {
            return DisguiseType.Ocelot;
        }
        else if (entity == EntityType.PIG) {
            return DisguiseType.Pig;
        }
        else if (entity == EntityType.PIG_ZOMBIE) {
            return DisguiseType.PigZombie;
        }
        else if (entity == EntityType.SHEEP) {
            return DisguiseType.Sheep;
        }
        else if (entity == EntityType.SILVERFISH) {
            return DisguiseType.Silverfish;
        }
        else if (entity == EntityType.SKELETON) {
            return DisguiseType.Skeleton;
        }
        else if (entity == EntityType.SLIME) {
            return DisguiseType.Slime;
        }
        else if (entity == EntityType.SNOWMAN) {
            return DisguiseType.Snowman;
        }
        else if (entity == EntityType.SPIDER) {
            return DisguiseType.Spider;
        }
        else if (entity == EntityType.SQUID) {
            return DisguiseType.Squid;
        }
        else if (entity == EntityType.VILLAGER) {
            return DisguiseType.Villager;
        }
        else if (entity == EntityType.WOLF) {
            return DisguiseType.Wolf;
        }
        else if (entity == EntityType.ZOMBIE) {
            return DisguiseType.Zombie;
        }
        else if (entity == EntityType.WITCH) {
            return DisguiseType.Witch;
        }
        else if (entity == EntityType.BAT) {
            return DisguiseType.Bat;
        }
        else if (entity == EntityType.WITHER) {
            return DisguiseType.Wither;
        }
        return null;
    }


    public static boolean isDisguised(Player p) {
        return dcAPI.isDisguised(p);
    }
}
