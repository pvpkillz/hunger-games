package kz.pvp.PvPGames.Methods;

import kz.PvP.PkzAPI.utilities.Message;
import kz.pvp.PvPGames.Main.MainHG;
import kz.pvp.PvPGames.Utilities.MessageHG;
import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.FireworkEffect.Type;
import org.bukkit.Location;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Firework;
import org.bukkit.inventory.meta.FireworkMeta;

import java.util.Random;
import java.util.logging.Logger;

public class Locators {
	public static MainHG plugin;
	static Logger log = Bukkit.getLogger();
	public static int LightningTimer = 0;
	public static int FireworksTimer = 0;
	public Locators (MainHG mainclass){
		plugin = mainclass;
	}
	
	
	
	public static void beginLightningLocator(){
		Message.G(MessageHG.LightningLocatorActive, true);
		LightningTimer = Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(plugin, new Runnable() {
		    @Override  
		    public void run() {
				for (String p : MainHG.Gamers){
					Location strikeLoc = Bukkit.getPlayer(p).getLocation().add(0, 10, 0);
					Bukkit.getPlayer(p).getWorld().strikeLightningEffect(strikeLoc);
				}
		    }
		}, 0L, 20L * 10);
	}
	
	
	
	public static void beginFireworksLocator(){
		Message.G(MessageHG.FireworksLocatorActive, true);
		FireworksTimer = Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(plugin, new Runnable() {
		    @Override  
		    public void run() {
				for (String p : MainHG.Gamers){
					Location fireworksLoc = Bukkit.getPlayer(p).getLocation().add(0, 10, 0);
					spawnFirework(fireworksLoc, false);
				}
		    }
		}, 0L, 20L * 10);
	}
	
	
	public static void spawnFirework(Location loc, boolean Randomize) {
        Firework fw = (Firework) loc.getWorld().spawnEntity(loc, EntityType.FIREWORK);
        FireworkMeta fwm = fw.getFireworkMeta();
        Random r = new Random();
        
        Type type = Type.STAR;
        
        if (Randomize){
        	double ran = Math.random();
        	if (ran <= 0.2)
        	type = Type.BALL;
        	else if (ran <= 0.4)
            type = Type.BALL_LARGE;
        	else if (ran <= 0.6)
            type = Type.BURST;
        	else if (ran <= 0.8)
            type = Type.CREEPER;
        	else if (ran <= 1)
            type = Type.STAR;
        }
        
        
        
        Color c1 = Color.RED;
        Color c2 = Color.YELLOW;
        Color c3 = Color.ORANGE;
       
        FireworkEffect effect = FireworkEffect.builder().flicker(r.nextBoolean()).withColor(c1).withColor(c2).withFade(c3).with(type).trail(r.nextBoolean()).build();
        fwm.addEffect(effect);
       
        int rp = r.nextInt(2) + 1;
        fwm.setPower(rp);
        
        fw.setFireworkMeta(fwm);           
	}
}
