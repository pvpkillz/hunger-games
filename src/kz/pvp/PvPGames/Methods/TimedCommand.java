package kz.pvp.PvPGames.Methods;

import kz.PvP.PkzAPI.methods.ConvertTimings;
import kz.PvP.PkzAPI.utilities.Message;
import kz.pvp.PvPGames.Main.MainHG;
import kz.pvp.PvPGames.Utilities.MessageHG;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.HashMap;

public class TimedCommand {
    public static MainHG plugin;
    public static HashMap<String, ArrayList<String>> TimedCommands = new HashMap<String, ArrayList<String>>();// Team list


    public TimedCommand (MainHG mainclass){
        plugin = mainclass;
    }

    public static void TimedCommands(final Player p, int timeInSeconds, final String command){
        if (!TimedCommands.containsKey(p.getName())) {
            TimedCommands.put(p.getName(), new ArrayList<String>());
        }

        TimedCommands.get(p.getName()).add(command);

        if (timeInSeconds > 0){
            Message.P(p, Message.Replacer(MessageHG.HaveTimeLeft, ConvertTimings.convertTime(timeInSeconds), "%time"), false);

            Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
                public void run() {
                    TimedCommands.get(p.getName()).remove(command);
                }
            }, 20L * timeInSeconds);
        }
    }
}
