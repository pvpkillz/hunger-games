package kz.pvp.PvPGames.Methods;

import kz.PvP.PkzAPI.methods.ConvertTimings;
import kz.PvP.PkzAPI.methods.itemModifications;
import kz.PvP.PkzAPI.utilities.Message;
import kz.PvP.PkzAPI.utilities.schematics.Schem;
import kz.PvP.PkzAPI.utilities.schematics.Schematic;
import kz.pvp.PvPGames.Enums.CType;
import kz.pvp.PvPGames.Main.MainHG;
import kz.pvp.PvPGames.Utilities.FilesHG;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Generation {
	public static MainHG plugin;
	private static int radius = 8;
	private static Block mainBlock;
	public static ArrayList<Location> cblocks = new ArrayList<Location>();
	static File cornucopia = null;
	static File feast = null;
	static File minifeast = null;
	public Generation (MainHG mainclass){
		plugin = mainclass;
		cornucopia = new File(plugin.getDataFolder() + File.separator + "Schematics" + File.separator + "Cornucopia.schematic");
		feast = new File(plugin.getDataFolder() + File.separator + "Schematics" + File.separator + "Feast.schematic");
		minifeast = new File(plugin.getDataFolder() + File.separator + "Schematics" + File.separator + "Minifeast.schematic");
	}

	public static void createCorn(Location loc) {
		if (MainHG.EnableCornucopia){
			mainBlock = getCornSpawnBlock(loc);
			removeAbove(mainBlock);
			createCornucopia(loc, CType.Cornucopia);
		}
	}
	public static void createFeast(Location loc) {
		removeAbove(mainBlock);
		createCornucopia(loc, CType.Cornucopia);
	}
	private static Block getCornSpawnBlock(Location loc) {
		Block b = loc.getWorld().getBlockAt(loc.subtract(0,1,0));
		Integer sub = 0;
		while (b.getType() == Material.LOG || b.getType() == Material.LEAVES || b.getType() == Material.AIR) {
			sub++;
			loc = MainHG.gameWorld.getSpawnLocation().subtract(0, 1 + sub, 0);
			b = loc.getWorld().getBlockAt(loc);
		}
		return b;
	}

	private static void createCornucopia(Location loc, CType type) {
		File schematic = null;

		if (type == CType.Cornucopia)
			schematic = cornucopia;
		else if (type == CType.Feast)
			schematic = feast;
		else if (type == CType.Minifeast)
			schematic = minifeast;



		loc.add(-3, 1, -3);
		boolean generateCornucopia = false;

		//-2: new layer; -1: new row; 0: air; 1: block;
		// 2: chest; 3: enchanting table; 4: fence; 5 : no change
		// 6: diamond_block; 7: beacon

		try {
			Schematic schem = Schem.loadSchematic(schematic);
			radius = (int)Math.ceil(schem.getLenght()/2);
			for (int x = -radius; x <= radius; x++){
				for (int z = -radius; z <= radius; z++){
					Location center = loc.clone().subtract(0, 1, 0);
					Location subCenter = center.add(x, 0, z);
					if (!subCenter.getBlock().getType().isBlock()){
						if (subCenter.distance(center) <= radius){
							subCenter.getBlock().setType(Material.GRASS);
						}
					}
				}
			}
			
			
			final ArrayList<Block> blocks = Schem.pasteSchematic(loc.getWorld(), loc, schem);
			plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
				public void run() {
					for (Block b : blocks){
						if (b.getType() == Material.CHEST){
							((Chest)b.getState()).getBlockInventory().clear();
							Maps.chestSetup(((Chest)b.getState()));
						}
						cblocks.add(b.getLocation());
					}
				}
			}, 20L);
		} catch (IOException e) {
			Message.C("Failed to grab the Schematic... Defaulting to generated mode.");
			generateCornucopia = true;
		}


		if (generateCornucopia){
			for (int x = -radius; x <= radius; x++){
				for (int z = -radius; z <= radius; z++){
					Location center = loc.clone().subtract(0, 1, 0);
					Location subCenter = center.add(x, 0, z);
					if (!subCenter.getBlock().getType().isBlock()){
						if (subCenter.distance(center) <= radius){
							subCenter.getBlock().setType(Material.GRASS);
						}
					}
				}
			}
			Integer[] co = {
					0, 0, 0, 0, 0, 0, 0, -1,
					0, 4, 2, 1, 2, 4, 0, -1,
					0, 2, 1, 1, 1, 2, 0, -1,
					0, 1, 1, 1, 1, 1, 0, -1,
					0, 2, 1, 1, 1, 2, 0, -1,
					0, 4, 2, 1, 2, 4, 0, -1,
					0, 0, 0, 0, 0, 0, 0, -2,

					0, 0, 0, 0, 0, 0, 0, -1,
					0, 4, 0, 0, 0, 4, 0, -1,
					0, 0, 0, 0, 0, 0, 0, -1,
					0, 0, 0, 3, 0, 0, 0, -1,
					0, 0, 0, 0, 0, 0, 0, -1,
					0, 4, 0, 0, 0, 4, 0, -1,
					0, 0, 0, 0, 0, 0, 0, -2,

					0, 0, 0, 0, 0, 0, 0, -1,
					0, 4, 0, 0, 0, 4, 0, -1,
					0, 0, 0, 0, 0, 0, 0, -1,
					0, 0, 0, 0, 0, 0, 0, -1,
					0, 0, 0, 0, 0, 0, 0, -1,
					0, 4, 0, 0, 0, 4, 0, -1,
					0, 0, 0, 0, 0, 0, 0, -2,

					0, 0, 1, 1, 1, 0, 0, -1,
					0, 1, 1, 1, 1, 1, 0, -1,
					1, 1, 0, 0, 0, 1, 1, -1,
					1, 1, 0, 0, 0, 1, 1, -1,
					1, 1, 0, 0, 0, 1, 1, -1,
					0, 1, 1, 1, 1, 1, 0, -1,
					0, 0, 1, 1, 1, 0, 0, -2,

					0, 0, 0, 0, 0, 0, 0, -1,
					0, 0, 0, 1, 0, 0, 0, -1,
					0, 0, 1, 1, 1, 0, 0, -1,
					0, 1, 1, 0, 1, 1, 0, -1,
					0, 0, 1, 1, 1, 0, 0, -1,
					0, 0, 0, 1, 0, 0, 0, -1,
					0, 0, 0, 0, 0, 0, 0, -2,

					0, 0, 0, 0, 0, 0, 0, -1,
					0, 0, 0, 0, 0, 0, 0, -1,
					0, 0, 0, 0, 0, 0, 0, -1,
					0, 0, 0, 1, 0, 0, 0, -1,
					0, 0, 0, 0, 0, 0, 0, -1,
					0, 0, 0, 0, 0, 0, 0, -1,
					0, 0, 0, 0, 0, 0, 0, -2};

			for(Integer i : co) {
				Material m = Material.AIR;
				switch (i) {
				case 0:
					m = Material.AIR;
					break;
				case 1:
					m = Material.IRON_BLOCK;
					break;
				case 2:
					m = Material.CHEST;
					break;
				case 3:
					m = Material.ENCHANTMENT_TABLE;
					break;
				case 4:
					m = Material.FENCE;
					break;
				case 5:
					break;
				case -1:
					break;
				case -2:
					break;
				default:
					plugin.getLogger().warning("Illegal integer found while creating cornucopia: " + i.toString());
					break;
				}

				if (i == -1) {
					loc.add(0, 0, 1);
					loc.subtract(7, 0, 0);
				} else if (i == -2) {
					loc.add(0, 1, 0);
					loc.subtract(7, 0, 6);
				} else if (i == 5){
					loc.add(1, 0, 0);
				} else {
					if (loc.getBlock().getType() == Material.CHEST) {
						loc.getBlock().setType(Material.AIR);
					}
					loc.getBlock().setType(m);
					if (m == Material.CHEST){
						Chest chest = (Chest) loc.getBlock().getState();
						spawnItems(chest);
					}
					if (i != 0) {
						cblocks.add(loc.getBlock().getLocation());
					}
					loc.add(1, 0, 0);
				}
			}
		}
	}

	public static Boolean isCornucopiaBlock(Block b) {
		return (MainHG.EnableCornucopia && cblocks.contains(b.getLocation()));
	}
	@Deprecated
	public static void removeAbove(Block block) {
		Location loc = block.getLocation();
		loc.setY(loc.getY()+1);
		Block newBlock = MainHG.gameWorld.getBlockAt(loc);
		while(loc.getY() < MainHG.gameWorld.getMaxHeight()) {
			newBlock.setType(Material.AIR);
			loc.setY(loc.getY()+1);
			newBlock = Bukkit.getServer().getWorlds().get(0).getBlockAt(loc);
		}
	}

	public static Inventory spawnItems(Chest chest) {
		List<String> items = FilesHG.cornconf.getCustomConfig().getStringList("Items");
		for (String itemString : items) {
			String[] itemDefinition = itemString.split(";");
			String itemStackString = itemDefinition[0];
			ItemStack itemStack = itemModifications.getItemInfo(itemStackString, null);
			Integer amount = 0;
			if (itemDefinition.length >= 2){
				Integer minAmount = 1;
				Integer maxAmount = 1;
				Boolean Common = true;
				String[] itemDefinitionCommon = itemDefinition[1].split(",");
				if (itemDefinitionCommon.length == 2){
					if (ConvertTimings.isInteger(itemDefinitionCommon[0]))
						minAmount = Integer.parseInt(itemDefinitionCommon[0]);
					if (ConvertTimings.isInteger(itemDefinitionCommon[1]))
						maxAmount = Integer.parseInt(itemDefinitionCommon[1]);
				}
				if (itemDefinitionCommon.length == 3)
					Common = Boolean.parseBoolean(itemDefinitionCommon[2]);

				Random r = new Random();

				if(Common){
					int randomInt = ConvertTimings.randomInt(0, 100);
					if (randomInt > 35)
						Common = false;
				}
				else{
					int randomInt = ConvertTimings.randomInt(0, 100);
					if (randomInt <= 10)
						Common = true;
				}

				if(!Common)
					continue;

				if (maxAmount == minAmount)
					amount = maxAmount;
				else
					amount = ConvertTimings.randomInt(minAmount, maxAmount);

				while (amount > 0) {
					Integer slot = r.nextInt(27);
					int maxtry = 0;
					while (chest.getInventory().getItem(slot) != null &&!chest.getInventory().getItem(slot).getType().equals(itemStack.getType()) && maxtry < 500) {
						maxtry++;
						slot = r.nextInt(27);
					}
					chest.getInventory().setItem(slot, itemStack);
					chest.update();
					amount--;
				}
			}
		}
		return chest.getInventory();
	}

	public static Block getMainBlock() {
		return mainBlock;
	}
}
