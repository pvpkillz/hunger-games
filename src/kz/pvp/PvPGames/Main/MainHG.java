package kz.pvp.PvPGames.Main;

import kz.PvP.PkzAPI.Main;
import kz.PvP.PkzAPI.utilities.Mysql;
import kz.pvp.PvPGames.Commands.GameCommands;
import kz.pvp.PvPGames.Commands.PlayerCommands;
import kz.pvp.PvPGames.Enums.GameType;
import kz.pvp.PvPGames.Enums.Gamestatus;
import kz.pvp.PvPGames.Enums.mapInfo;
import kz.pvp.PvPGames.Events.*;
import kz.pvp.PvPGames.Methods.*;
import kz.pvp.PvPGames.Timers.waitingTimer;
import kz.pvp.PvPGames.Timers.winnerAnnouncer;
import kz.pvp.PvPGames.Utilities.*;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;



public class MainHG extends JavaPlugin{
	public static boolean CornucopiaChests = false;// Should the Cornucopia chests be enabled?
	public static final int VoteTimeout = 60;// Length of voting period.
	public static final int HCDelay = 15;// Delay in seconds after a successful vote of Hardcore
	public static final int PlayerTimeout = 30;// Timeout for players if they leave the game
	public static final int MaxMobEffectLength = 240;// Max the mob effect lasts for... In Seconds
	public static boolean SpectatorKickEnable = true;//Kick players when they die? 
	public static boolean EnableTeams = true;//Enable teams?
	public static boolean TeamDefend = false;// are Teams Defend enabled?
	public static boolean FinalBattleSpawned = false;// Has the final battle began?
	public static boolean ProtocolLibEnable = false;// Protocol Lib installed?
	public static boolean DisguiseCraftEnable = false;// DisguiseCraft enabled?
	public static long FeastSpawnTime = 10;// Feast spawn time...
	public static long LastGame = 0;// LastGame
	public static int AtleastNeededPlayers = 1;// The least players needed for a round. | Set to 1 for DEBUG
	public static boolean GiveStartBook = false;// Should the players be given a start-out book?
	// We initialize the variables, but don't set their real values.
	public static Gamestatus Game = Gamestatus.Pregame;// We set gamemode to PreGame
	public static int TimeLeft = 0;// Time which is used across the whole code for pregame timer & invincibility
	public static int GamemakerTime = 0;
	public static String LastWinner = null;
	public static String WebsiteForKits = null;// The website for more kits
	public static int PointsPerKill = 0;// Per kill is not initialized yet.
	public static int PointsPerWin = 0;// Per win is not initialized yet.
	public static boolean EnableCompass = false;// Enable compass is not initialized yet.
	public static int MaxGameLength = 0;// The length of each game / Minutes
	public static boolean RandomDefaultKit = false;
	public static String DefaultKit = "";
	public static int FinalBattleTime = 0;// The time at which players should be sent to special room.
	public static int GracePeriod = 0;// Time for invincibility after game begins.
	public static int PregameTime = 0;// Time for waiting before game begins.
	public static boolean KitPrefix = false;// Should we place the kit name before real name in chat?
	public static boolean EnableCornucopia = false;// Do we enable a cornucopia?
	public static boolean EnableFeast = false;// Do we enable a feast?
	public static boolean DefaultKitEnable = false;// Do players who did not choose a kit, be given a default one?
	public static boolean KitMenu = false;// Do we use chat menu or inventory menu?
	public static boolean DeathSign = false;// Do we place Death Signs when a person dies?
	public static boolean SpectatorEnable = false;// Can people spectate the game or not?
	public static String MainLanguage = "en";// Main language for the plugin to use
	public static boolean simpleRewards = false;// Enable simple rewards = Give all kits on win.
	List<String> pages = null;
	public static List<String> content = new ArrayList<String>();
	List<String> page = new ArrayList<String>();
	public static mapInfo gameMap;
	
	
	// Some variables used across the whole code!!!
	public static boolean LocatorsActive = false;// Are the locators active?
	public static boolean DisableKitChoosing = false;// Should kit selection be disabled? | For the random kits

	// We initialize the Teams or Sets of people which will be in the games.
	public static ArrayList<String> Gamers = new ArrayList<String>();// Gamers in the server / Hashmap incase we want to convert to multiple arenas
	public static ArrayList<String> GameMakers = new ArrayList<String>();// GameMakers in the server / Hashmap incase we want to convert to multiple arenas
	public static ArrayList<String> Spectators = new ArrayList<String>();// Spectators in the server / Hashmap incase we want to convert to multiple arenas
	public static ArrayList<mapInfo> maps = new ArrayList<mapInfo>();
	public static ArrayList<UUID> voters = new ArrayList<UUID>();

	
	public static HashMap<mapInfo, ArrayList<String>> mapVotes = new HashMap<mapInfo, ArrayList<String>>();

	public static MainHG plugin;
	public static Connection con;
	public static Plugin plug;
	public static GameType Gamemode = GameType.Normal;
	public static int GameID = 0;
	public static Location feastLoc = null;
	public static String feastLocation = null;
	public static World lobbyWorld;
	public static World gameWorld;//The world being used for the current game.

	
	
	public void onLoad()
	{
		new FilesHG(this);
		new Maps(this);
		FileConfiguration conf = FilesHG.config.getCustomConfig();

		// We set variables depending on the config file.
		String Option = "";

		Option = "Token_Rewards.Kill";
		if (!conf.contains(Option))
			FilesHG.config.getCustomConfig().set(Option, "1");
		PointsPerKill = conf.getInt(Option);// Amount of points to reward per kill.

		Option = "Token_Rewards.Win";
		if (!conf.contains(Option))
			FilesHG.config.getCustomConfig().set(Option, "1");
		PointsPerWin = conf.getInt(Option);// Amount of points to reward per win.

		Option = "EnableCompass";
		if (!conf.contains(Option))
			FilesHG.config.getCustomConfig().set(Option, "true");
		EnableCompass = conf.getBoolean(Option);// Enable compass

		Option = "Time.GameLength";
		if (!conf.contains(Option))
			FilesHG.config.getCustomConfig().set(Option, "25");
		MaxGameLength = conf.getInt(Option) * 60;// Convert to seconds

		Option = "Time.FinalBattle";
		if (!conf.contains(Option))
			FilesHG.config.getCustomConfig().set(Option, "20");
		FinalBattleTime = conf.getInt(Option) * 60;// Convert to seconds

		Option = "Time.Invincibility";
		if (!conf.contains(Option))
			FilesHG.config.getCustomConfig().set(Option, "120");
		GracePeriod = conf.getInt(Option);// Keep Seconds

		Option = "Time.Pregame";
		if (!conf.contains(Option))
			FilesHG.config.getCustomConfig().set(Option, "300");
		PregameTime = conf.getInt(Option);// Keep Seconds


		Option = "Game.NeededPlayers";
		if (!conf.contains(Option))
			FilesHG.config.getCustomConfig().set(Option, "3");
		AtleastNeededPlayers = conf.getInt(Option);// Needed players per game start

		Option = "Game.KitPrefix";
		if (!conf.contains(Option))
			FilesHG.config.getCustomConfig().set(Option, "false");
		KitPrefix = conf.getBoolean(Option);// Show Kit prefix on chat throughout game

		Option = "Game.Cornucopia";
		if (!conf.contains(Option))
			FilesHG.config.getCustomConfig().set(Option, "true");
		EnableCornucopia = conf.getBoolean(Option);// Enable Cornucopia



		Option = "Game.DefaultKitEnable";
		if (!conf.contains(Option))
			FilesHG.config.getCustomConfig().set(Option, "true");
		DefaultKitEnable = conf.getBoolean(Option);// Keep a default kit used, when no kit is chosen

		Option = "Game.KitMenu";
		if (!conf.contains(Option))
			FilesHG.config.getCustomConfig().set(Option, "false");
		KitMenu = conf.getBoolean(Option);// Use an inventory menu for kit choosing or chat


		Option = "Game.Spectators";
		if (!conf.contains(Option))
			FilesHG.config.getCustomConfig().set(Option, "true");
		SpectatorEnable = conf.getBoolean(Option);// Enable Spectators throughout the game.

		Option = "Game.GiveBook";
		if (!conf.contains(Option))
			FilesHG.config.getCustomConfig().set(Option, "true");
		GiveStartBook = conf.getBoolean(Option);// Enable Spectators throughout the game.


		Option = "Game.MainLanguage";
		if (!conf.contains(Option))
			FilesHG.config.getCustomConfig().set(Option, "en");
		MainLanguage = conf.getString(Option);// Control the Main language that the plugin uses

		Option = "Game.KitWebsite";
		if (!conf.contains(Option))
			FilesHG.config.getCustomConfig().set(Option, "http://www.PvP.kz/Kits");
		WebsiteForKits = conf.getString(Option);// The website used to purchase kits.

		Option = "Game.Simple_Reward";
		if (!conf.contains(Option))
			FilesHG.config.getCustomConfig().set(Option, "true");
		simpleRewards = conf.getBoolean(Option);// Give the last winner a reward

		Option = "Gamemakers.JoinTimeout";
		if (!conf.contains(Option))
			FilesHG.config.getCustomConfig().set(Option, "120");
		GamemakerTime = conf.getInt(Option);// Join Timeout for switching to a Gamemaker

		Option = "Game.FeastSpawn";
		if (!conf.contains(Option))
			FilesHG.config.getCustomConfig().set(Option, "120");
		FeastSpawnTime = conf.getInt(Option) * 60;// Generate a Feast in the round...

		Option = "Game.DefaultKitEnable";
		if (!conf.contains(Option))
			FilesHG.config.getCustomConfig().set(Option, "true");
		DefaultKitEnable = conf.getBoolean(Option);// Enable default kits?


		Option = "Game.RandomDefaultKit";
		if (!conf.contains(Option))
			FilesHG.config.getCustomConfig().set(Option, "true");
		RandomDefaultKit = conf.getBoolean(Option);// Randomzie the default kit?

		Option = "Game.DefaultKit";
		DefaultKit = conf.getString(Option);// Enable default kit?

		Option = "Game.DefendTeam";
		if (!conf.contains(Option))
			FilesHG.config.getCustomConfig().set(Option, "true");
		TeamDefend = conf.getBoolean(Option);// Defence of team players

		Option = "Game.SpectatorKick";
		if (!conf.contains(Option))
			FilesHG.config.getCustomConfig().set(Option, "true");
		SpectatorKickEnable = conf.getBoolean(Option);// Enable Teams

		Option = "Game.EnableTeams";
		if (!conf.contains(Option))
			FilesHG.config.getCustomConfig().set(Option, "true");
		EnableTeams = conf.getBoolean(Option);// Enable Teams
	}


	public void onEnable()
	{
		new JoinListener(this);
		new PlayerCommands(this);
		new GameCommands(this);
		new TimedCommand(this);
		new MobsGM(this);
		new MoveListener(this);
		new Hardcore(this);
		new Locators(this);	    
		new SpectatorListener(this);
		new Kits(this);
		new GameFunctions(this);
		new Generation(this);
		new DeathListener(this);
		new Teams(this);
		new InteractListener(this);
		new KitsHG(this);
		new Abilities(this);
		new MessageHG(this);
		new Sponsor(this);
		new StatsHG(this);
		new BlockModificationEvents(this);
		new Abilities_Building(this);
		new Abilities_Fighting(this);
		new Abilities_Interaction(this);

		for (Entity e : this.getServer().getWorlds().get(0).getEntities())
			e.remove();
		
		
		Scoreboard();
		RegisterCommands();
		AssignRequiredData();




		if (!Main.UseMySQL)
			simpleRewards = true;

		for (Player p : getServer().getOnlinePlayers())
			try {
				JoinListener.JoinedServer(p);
			} catch (SQLException e1) {e1.printStackTrace();}


		ResultSet rs;
		try {
			rs = Mysql.PS.getSecureQuery("SELECT * FROM HungerGames_Rounds ORDER BY End DESC LIMIT 1");
			if (rs.next())
				MainHG.LastGame = rs.getLong("End");
		} catch (SQLException e1) {e1.printStackTrace();}


		if (winnerAnnouncer.task != null){
			if (this.getServer().getScheduler().isCurrentlyRunning(winnerAnnouncer.task.getTaskId()))
				this.getServer().getScheduler().cancelTask(winnerAnnouncer.task.getTaskId());
		}
		winnerAnnouncer.task = new winnerAnnouncer().runTaskTimer(this, 1 * 20L, 1 * 20L);





		if (waitingTimer.task != null){
			if (this.getServer().getScheduler().isCurrentlyRunning(waitingTimer.task.getTaskId()))
				this.getServer().getScheduler().cancelTask(waitingTimer.task.getTaskId());
		}
		waitingTimer.task = new waitingTimer().runTaskTimer(this, 1 * 20L, 1 * 20L);
		
		
		
		Plugin disguiseCraft = this.getServer().getPluginManager().getPlugin("DisguiseCraft");

		if (disguiseCraft == null || !Main.ProtocolLibEnable)
			MainHG.DisguiseCraftEnable = false;
		else{
			MainHG.DisguiseCraftEnable = true;
			new Disguises(this);
		}
		
		MainHG.lobbyWorld = this.getServer().getWorlds().get(0);
		
		
		List<String> worldsinfo = FilesHG.worldconf.getCustomConfig().getStringList("Worlds");

		// Use preset map
		Integer count = 1;
		for (String mapString : worldsinfo){
			Integer WorldBorder;// World border of the map
			String MapAuthor;// Map author
			String MapName;// Map name
			
			String[] WorldInfoSet = mapString.split(",");
			MapName = WorldInfoSet[0];
			WorldBorder = Integer.parseInt(WorldInfoSet[1]);
			MapAuthor = WorldInfoSet[2];
			mapInfo map = new mapInfo();
			map.setMapName(MapName);
			map.setMapBorder(WorldBorder);
			map.setMapAuthor(MapAuthor);
			map.setIndex(count);
			maps.add(map);
			mapVotes.put(map, new ArrayList<String>());
			count++;
		}
	}
	private void AssignRequiredData() {
		pages = FilesHG.bookconf.getCustomConfig().getStringList("content");
		for(String line : pages)  {
			line = line.replace("<newline>", ChatColor.RESET + "\n");
			line = ChatColor.translateAlternateColorCodes('&', line);
			if(!line.contains("<newpage>")) {
				page.add(line + "\n");
			} else {
				StringBuilder sb = new StringBuilder();
				for (String l : page) {
					sb.append(l);
				}

				content.add(sb.toString());
				page.clear();
			}
		}

		StringBuilder sb = new StringBuilder();
		for (String l : page) {
			sb.append(l);
		}

		content.add(sb.toString());
	}


	private void RegisterCommands() {
		getCommand("kit").setExecutor(new PlayerCommands(this));
		getCommand("sponsor").setExecutor(new PlayerCommands(this));
		getCommand("team").setExecutor(new PlayerCommands(this));
		getCommand("cp").setExecutor(new GameCommands(this));
		getCommand("gamemaker").setExecutor(new GameCommands(this));
		getCommand("map").setExecutor(new PlayerCommands(this));
	}




	// The scoreboard method / Runs the scoreboard updater every 1 second.
	private void Scoreboard() {
		this.getServer().getScheduler().runTaskTimerAsynchronously(this, new Runnable() {
			@Override  
			public void run() {
				for (Player pl : getServer().getOnlinePlayers()){
					Kits.ScoreBoard(pl);
				}
			}
		}, 10L, 10L);

	}




	public void onDisable(){
		Bukkit.getServer().getScheduler().cancelAllTasks();
	}
}
