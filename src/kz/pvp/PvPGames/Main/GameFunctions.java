package kz.pvp.PvPGames.Main;

import java.io.File;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map.Entry;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Logger;

import kz.PvP.PkzAPI.Main;
import kz.PvP.PkzAPI.methods.ConvertTimings;
import kz.PvP.PkzAPI.methods.Scoreboards;
import kz.PvP.PkzAPI.utilities.Message;
import kz.PvP.PkzAPI.utilities.Mysql;
import kz.PvP.PkzAPI.utilities.schematics.Schem;
import kz.PvP.PkzAPI.utilities.schematics.Schematic;
import kz.pvp.PvPGames.Enums.Gamestatus;
import kz.pvp.PvPGames.Enums.Team;
import kz.pvp.PvPGames.Enums.mapInfo;
import kz.pvp.PvPGames.Methods.Generation;
import kz.pvp.PvPGames.Methods.Maps;
import kz.pvp.PvPGames.Methods.Teams;
import kz.pvp.PvPGames.Timers.countdownTimer;
import kz.pvp.PvPGames.Timers.gracePeriodTimer;
import kz.pvp.PvPGames.Timers.mapLoadTimer;
import kz.pvp.PvPGames.Timers.roundTimer;
import kz.pvp.PvPGames.Utilities.Abilities;
import kz.pvp.PvPGames.Utilities.KitsHG;
import kz.pvp.PvPGames.Utilities.MessageHG;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.potion.PotionEffect;

public class GameFunctions implements Listener{
	public static MainHG plugin;
	static Logger log = Bukkit.getLogger();
	public static ArrayList<Location> spawns = new ArrayList<Location>();
	public static ArrayList<Location> originalSpawns = new ArrayList<Location>();
	public static File deathmatchArena;

	public GameFunctions (MainHG mainclass){
		plugin = mainclass;
	}

	public static void gamePreparation() throws SQLException{
		/*
		 * Calculate Votes...
		 */
		mapInfo chosenMap = null;
		Integer voteCount = 0;
		for (Entry<mapInfo, ArrayList<String>> mapVoteInfo : MainHG.mapVotes.entrySet()){
			if (mapVoteInfo.getValue().size() >= voteCount){
				voteCount = mapVoteInfo.getValue().size();
				chosenMap = mapVoteInfo.getKey();
			}
		}
		MainHG.gameMap = chosenMap;
		Message.G(Message.Replacer(Message.Replacer(MessageHG.GameBegins, ConvertTimings.convertTime(5), "%time"), MainHG.gameMap.getMapName(), "%map"), true);
		MainHG.gameWorld = Maps.loadMapFiles(MainHG.gameMap.getMapName(), "Hunger-Games");
		
		mapLoadTimer.task = new mapLoadTimer().runTaskTimer(plugin, 0 * 20L, 1 * 20L);
		ArrayList<Block> blocks = Abilities.getBlocksAround(MainHG.gameWorld.getSpawnLocation(), 30, 2);
		for (Block block : blocks){
			if (block.getType() == Material.WOOD_PLATE){
				GameFunctions.spawns.add(block.getLocation().add(0.5, 0, 0.5));// The center of the block eh!
				Generation.cblocks.add(block.getLocation());
			}
		}
		originalSpawns.addAll(spawns);
	}

	public static void gameStart () throws SQLException{
		boolean setSpawns = false;
		if (spawns.size() >= 1 && spawns.size() >= Bukkit.getOnlinePlayers().length){
			setSpawns = true;
			MainHG.Game = Gamestatus.SpawnedPre;
		}
		Scoreboards.scoreboardsList.clear();
		Scoreboards.locationScoreboard.clear();

		for (Player p : Teams.getGamers()){


			if (spawns.size() >= 1 && spawns.size() >= Bukkit.getOnlinePlayers().length){
				int index = ConvertTimings.randomInt(0, spawns.size());
				p.teleport(spawns.get(index));
				spawns.remove(index);
			}
			else{
				int x = ConvertTimings.randomInt(-10, -7);
				if (ConvertTimings.randomInt(0, 1) == 1)
					x = ConvertTimings.randomInt(7, 10);


				int z = ConvertTimings.randomInt(-10, -7);
				if (ConvertTimings.randomInt(0, 1) == 1)
					z = ConvertTimings.randomInt(7, 10);
				p.teleport(MainHG.gameWorld.getSpawnLocation().add(x, 5, z));
			}
		}
		if (!setSpawns)
			startGracePeriod();
		else
			startCountdown();
	}

	private static void startCountdown() {
		if (countdownTimer.task != null){
			if (plugin.getServer().getScheduler().isCurrentlyRunning(countdownTimer.task.getTaskId()))
				plugin.getServer().getScheduler().cancelTask(countdownTimer.task.getTaskId());
		}
		countdownTimer.task = new countdownTimer().runTaskTimer(plugin, 1 * 20L, 1 * 20L);
	}
	private static boolean dontReteleport = false;

	public static void startGracePeriod() throws SQLException{
		Maps.RandomizeChests();
		MainHG.gameWorld.playSound(MainHG.gameWorld.getSpawnLocation(), Sound.ANVIL_LAND, 10f, 1f);


		if (MainHG.Game == Gamestatus.SpawnedPre)
			dontReteleport = true;
		if (MainHG.GracePeriod == 0){
			OnGameStarted();// We don't wait! :D
		}
		else{// We got to wait...

			MainHG.Game = Gamestatus.GracePeriod;

			if (gracePeriodTimer.task != null){
				if (plugin.getServer().getScheduler().isCurrentlyRunning(gracePeriodTimer.task.getTaskId()))
					plugin.getServer().getScheduler().cancelTask(gracePeriodTimer.task.getTaskId());
			}
			gracePeriodTimer.task = new gracePeriodTimer().runTaskTimer(plugin, 1 * 20L, 1 * 20L);


			Message.G(Message.Replacer(MessageHG.GracePeriodStarted, ConvertTimings.convertTime(MainHG.TimeLeft), "%time"), false);
		}

		//**** Invincibility has started ****//
		if (!MainHG.gameWorld.getChunkAt(MainHG.gameWorld.getSpawnLocation()).isLoaded())
			MainHG.gameWorld.loadChunk(MainHG.gameWorld.getChunkAt(MainHG.gameWorld.getSpawnLocation()));

		MainHG.gameWorld.setTime(0);

		// We insert into DB
		if (Main.UseMySQL){
			Long Time = System.currentTimeMillis()/1000;
			StringBuilder competitorsString = new StringBuilder();
			for (String playerName : MainHG.Gamers){
				competitorsString.append(Mysql.getUserID(playerName) + ":" + KitsHG.getPlayerKit(playerName).getKitName());
				if (MainHG.Gamers.indexOf(playerName) < MainHG.Gamers.size() - 1)
					competitorsString.append(",");
			}
			
			String competitors = competitorsString.toString();
			
			
			ResultSet rs = Mysql.PS.getSecureQuery("INSERT INTO HungerGames_Rounds (Start, End, Winner, Contestants) VALUES (?, NULL, NULL,? )", ""+Time, ""+competitors);
			if (rs.next())
				MainHG.GameID = rs.getInt(1);
		}
		
		for (final String playerName : MainHG.Gamers){
			Scoreboards.getBoard(playerName);
			Runnable teleport = new Runnable() {
				public void run() {
					try {
						Player pl = Bukkit.getPlayer(playerName);
						pl.setFlying(false);
						pl.setAllowFlight(false);
						if (!dontReteleport){
							int x = ConvertTimings.randomInt(-10, -7);
							if (ConvertTimings.randomInt(0, 1) == 1)
								x = ConvertTimings.randomInt(7, 10);


							int z = ConvertTimings.randomInt(-10, -7);
							if (ConvertTimings.randomInt(0, 1) == 1)
								z = ConvertTimings.randomInt(7, 10);
							pl.teleport(MainHG.gameWorld.getSpawnLocation().add(x, 5, z));
						}

						if(pl.isInsideVehicle())
							pl.getVehicle().eject();

						pl.setHealth(20.0);
						pl.setFoodLevel(20);
						pl.setExhaustion(20);
						pl.getEnderChest().clear();
						pl.setGameMode(GameMode.SURVIVAL);
						pl.setFireTicks(0);
						pl.closeInventory();
						pl.setFlying(false);
						pl.setAllowFlight(false);
						for(PotionEffect e : pl.getActivePotionEffects())
							pl.removePotionEffect(e.getType());

						if(pl.getOpenInventory() != null)
							pl.getOpenInventory().close();

						KitsHG.giveKit(pl);
						Teams.giveTools(pl, Team.Player);

						Teams.adjustName(pl);

					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			};
			ExecutorService executor = Executors.newCachedThreadPool();
			executor.submit(teleport);
		}
	}
	public static void OnGameStarted () throws SQLException{
		MainHG.Game = Gamestatus.Game;
		Message.G(MessageHG.BattleActive, false);
		Message.G(MessageHG.MayTheOdds, false);

		Teams.checkStatus();


		if (roundTimer.task != null){
			if (plugin.getServer().getScheduler().isCurrentlyRunning(roundTimer.task.getTaskId()))
				plugin.getServer().getScheduler().cancelTask(roundTimer.task.getTaskId());
		}
		roundTimer.task = new roundTimer().runTaskTimer(plugin, 1 * 20L, 1 * 20L);
	}


	public static void endGame() throws SQLException {
		if (Teams.getGamers().size() == 0){

			if (Main.UseMySQL){
				Long Time = System.currentTimeMillis()/1000;
				Mysql.PS.getSecureQuery("UPDATE HungerGames_Rounds SET End = ?, Winner = ? WHERE ID = ?", Time+"", null, ""+MainHG.GameID);
			}
			plugin.getServer().shutdown();
		}
		else{
			Player chosenPlayer = null;
			while (Teams.getGamers().size() > 1){
				for (Player p : Teams.getGamers()){
					if (chosenPlayer == null)
						chosenPlayer = p;
					else if (chosenPlayer.getHealth() > p.getHealth())
						chosenPlayer = p;
				}
				Teams.eliminateUser(chosenPlayer, null, false);
			}
		}
	}


	public static void generateFinalBattle() {
		MainHG.FinalBattleSpawned = true;
		MainHG.Game = Gamestatus.Deathmatch;


		deathmatchArena = new File(plugin.getDataFolder() + File.separator + "Schematics" + File.separator + "Arena-Deathmatch.schematic");

		Location battleRoom = Maps.getRandomLocation().add(0, 40, 0);

		try {
			Schematic schem = Schem.loadSchematic(deathmatchArena);
			ArrayList<Block> blocks = Schem.pasteSchematic(battleRoom.getWorld(), battleRoom, schem);
			for (Block block : blocks){
				if (block.getType() == Material.STAINED_CLAY && block.getData() == 14){
					for (Player p : Teams.getGamers()){
						p.teleport(block.getLocation().add(0,2,0));
					}
				}
				Generation.cblocks.add(block.getLocation());
			}

		} catch (IOException e) {
			Message.C("Failed to load deathmatch arena... Generating it...");
			battleRoom.getBlock().setType(Material.GLOWSTONE);
			Abilities.generateRoom(battleRoom, 10, 7, Material.COBBLESTONE);
			for (Player p : Teams.getGamers()){
				p.teleport(battleRoom.add(0,3,0));
			}
		}




	}
}
