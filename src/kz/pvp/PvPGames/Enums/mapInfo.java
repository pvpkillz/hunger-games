package kz.pvp.PvPGames.Enums;

public class mapInfo {
	
	private String mapName;
	private Integer mapBorder;
	private String mapAuthor;
	private Integer mapIndex;
	public mapInfo() {}

	/*
	 * Setting methods
	 */
	public void setMapName(String name){mapName = name;}
	public void setMapBorder(Integer radius){mapBorder = radius;}
	public void setMapAuthor(String author){mapAuthor = author;}
	public void setIndex(Integer index){mapIndex = index;}
	
	/*
	 * Getting methods
	 */
	public String getMapName(){return mapName;}
	public Integer getBorder(){return mapBorder;}
	public String getAuthor(){return mapAuthor;}
	public Integer getIndex(){return mapIndex;}
}
