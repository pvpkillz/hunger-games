package kz.pvp.PvPGames.Enums;

public enum Gamestatus {
    Pregame,
    SpawnedPre,
    GracePeriod,
    Game,
    Deathmatch,
    Finished;
}