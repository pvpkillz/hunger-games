package kz.pvp.PvPGames.Enums;

public enum ModType {
    Add,
    Remove,
    Clear;
}