package kz.pvp.PvPGames.Enums;

import java.util.ArrayList;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;

public class KitInfo {
	String kitName = "Unknown";
	int tokenPrice = 0;
	ArrayList<ItemStack> kitItems = new ArrayList<ItemStack>();
	ArrayList<Integer> abilities = new ArrayList<Integer>();
	ArrayList<PotionEffect> potionEffects = new ArrayList<PotionEffect>();
	ItemStack iconMenu = new ItemStack(Material.PAPER);

	public KitInfo() {}

	/*
	 * Setting methods
	 */
	public void setIconMenu(ItemStack icon){iconMenu = icon;}
	public void setKitName(String name){kitName = name;}
	public void setPrice(Integer cost){tokenPrice = cost;}

	/*
	 * Adding methods
	 */
	public void addPotionEffect(PotionEffect pe){potionEffects.add(pe);}
	public void addKitItem(ItemStack is){kitItems.add(is);}
	public void addAbility(Integer abilityID){abilities.add(abilityID);}
	/*
	 * Removing methods
	 */
	public void removePotionEffect(PotionEffect pe){potionEffects.remove(pe);}
	public void removeKitItem(ItemStack is){kitItems.remove(is);}


	/*
	 * Getting methods
	 */
	public ArrayList<ItemStack> getKitItems (){return kitItems;}
	public ArrayList<PotionEffect> getPotionEffects (){return potionEffects;}
	public ArrayList<Integer> getAbilities (){return abilities;}
	public ItemStack getIconMenu (){return iconMenu;}
	public String getKitName (){return kitName;}
	public int getPrice() {return tokenPrice;}

}
