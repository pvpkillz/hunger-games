package kz.pvp.PvPGames.Enums;

public enum Team {
    Spectator,
    Gamemaker,
    Player;
}